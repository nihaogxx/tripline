package com.christy.tripline.model;

public class TransitItemModel {

    private String itemType;
    private String placeId;
    private String placeName;
    private String transitNum;
    private String date;
    private String time;


    public TransitItemModel() {
       // required
    }

    public TransitItemModel(String itemType, String placeId, String placeName, String transitNum, String date, String time) {
        this.itemType = itemType;
        this.placeId = placeId;
        this.placeName = placeName;
        this.transitNum = transitNum;
        this.date = date;
        this.time = time;
    }

    public String getPlaceId() {
        return placeId;
    }

    public String getPlaceName() {
        return placeName;
    }

    public String getTransitNum() {
        return transitNum;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getItemType() {
        return itemType;
    }

}

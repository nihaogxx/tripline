package com.christy.tripline.model;

public class LodgingItemModel {

    private String itemType;
    private String placeId;
    private String placeName;
    private String dates;

    public LodgingItemModel() {
        // Required empty public constructor
    }

    public LodgingItemModel(String itemType, String placeId, String placeName, String dates) {
        this.itemType = itemType;
        this.placeId = placeId;
        this.placeName = placeName;
        this.dates = dates;
    }

    public String getPlaceId() {
        return placeId;
    }

    public String getPlaceName() {
        return placeName;
    }

    public String getDates() {
        return dates;
    }

    public String getItemType() {
        return itemType;
    }
}

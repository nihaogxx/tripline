package com.christy.tripline.model;

public class NoteItemModel {

    private String itemType;
    private String noteTitle;
    private String noteBody;


    public NoteItemModel() {
        //requird
    }

    public String getItemType() {
        return itemType;
    }

    public NoteItemModel(String itemType, String noteTitle, String noteBody) {
        this.itemType = itemType;
        this.noteTitle = noteTitle;
        this.noteBody = noteBody;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public String getNoteBody() {
        return noteBody;
    }

}

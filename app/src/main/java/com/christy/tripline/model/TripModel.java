package com.christy.tripline.model;

public class TripModel {
    private String destination;
    private String background;
    private String timeStamp;
    private String duration;
    private String startDate;
    private String countryCode;
    private String coordinates;
    private String privacy;

    public TripModel() {
        // Required empty public constructor
    }

    public TripModel(String destination, String background, String timeStamp, String days, String startDate, String countryCode, String
                     coordinates, String privacy) {
        this.destination = destination;
        this.background = background;
        this.timeStamp = timeStamp;
        this.duration = days;
        this.startDate = startDate;
        this.countryCode = countryCode;
        this.coordinates = coordinates;
        this.privacy = privacy;
    }

    public String getDestination() {
        return destination;
    }

    public String getBackground() {
        return background;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public String getDuration() {
        return duration;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public String getPrivacy() {
        return privacy;
    }
}



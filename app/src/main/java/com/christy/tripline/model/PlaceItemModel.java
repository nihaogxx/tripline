package com.christy.tripline.model;

public class PlaceItemModel {

    private String itemType;
    private String placeId;
    private String placeName;
    private String placeType;

    public PlaceItemModel() {
        // Required empty public constructor
    }
    public PlaceItemModel(String itemType, String placeId, String placeName, String placeType) {
        this.itemType = itemType;
        this.placeId = placeId;
        this.placeName = placeName;
        this.placeType = placeType;
    }

    // must have setters and getter, or else get "Firebase No properties to serialize found on class"

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getPlaceName() {
        return placeName;
    }

    public String getPlaceType() {
        return placeType;
    }

    public String getItemType() {
        return itemType;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }



}

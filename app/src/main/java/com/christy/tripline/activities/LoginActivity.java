package com.christy.tripline.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.christy.tripline.R;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.christy.tripline.Tasks.UploadImageToStorageTask;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class LoginActivity extends AppCompatActivity {

    // Constants
    private static final int RC_GOOGLE_SIGN_IN = 1;

    // UI
    private SignInButton mGoogleButton;
    //private Button mFacebookButton;
    private FrameLayout mEmailTextInputLayout;
    private TextInputLayout mPasswordInputLayout;
    private AutoCompleteTextView mEmailView;
    private TextInputEditText mPasswordView;
    private Button mEmailSignInButton;
    private Button mTriplineSignInButton;
    private Button mRegisterButton;
    private Button mClearButton;
    ProgressDialog loadingBar;


    // Google api
    private GoogleApiClient mGoogleApiClient;
    // declare_auth
    private FirebaseAuth mAuth;
    // declare_database

    private StorageReference mUserImageRef;
    private DatabaseReference mDatabaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // find views
        mGoogleButton = findViewById(R.id.google_sign_in_button);
        mEmailTextInputLayout = findViewById(R.id.email_text_input_layout);
        mPasswordInputLayout = findViewById(R.id.password_text_input_layout);
        mEmailView = findViewById(R.id.email_input);
        mPasswordView = findViewById(R.id.password_input);
        mEmailSignInButton = findViewById(R.id.email_sign_in_button);
        mTriplineSignInButton = findViewById(R.id.tripline_sign_in_button);
        mRegisterButton = findViewById(R.id.email_register_button);
        mClearButton = findViewById(R.id.clear_email_login);
        loadingBar = new ProgressDialog(this);

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)//to request users' ID and basic profile information
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail() //To request users' email addresses
                .build();


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(LoginActivity.this, "Connection to Google Sign-in failed...", Toast.LENGTH_SHORT).show();

                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        mAuth = FirebaseAuth.getInstance();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mUserImageRef = FirebaseStorage.getInstance().getReference().child("Profile Images");

        // set listeners
        mGoogleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInWithGoogle();
            }
        });

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override //when the "enter" button is pressed
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == R.integer.login || actionId == EditorInfo.IME_ACTION_DONE) {
                    attemptEmailLogin();
                    return true;
                }
                return false;
            }
        });

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEmailView.getText().clear();
            }
        });

        mEmailView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mEmailView.getText().toString().length() > 0) {
                    mClearButton.setVisibility(View.VISIBLE);
                } else mClearButton.setVisibility(View.INVISIBLE);

            }
        });
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptEmailLogin();
            }
        });

        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerNewUser();
            }
        });

        mTriplineSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInputFields();
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();

        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            sendUserToHomeActivity();
        }
    }


    private void signInWithGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_GOOGLE_SIGN_IN); //https://stackoverflow.com/questions/62671106/onactivityresult-method-is-deprecated-what-is-the-alternative
    }


    @Override
    // try to get account
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_GOOGLE_SIGN_IN) {

            loadingBar.setTitle("Google Sign In");
            loadingBar.setMessage("Please wait, we are signing you in with your Google account...");
            loadingBar.setCanceledOnTouchOutside(true);
            loadingBar.show();

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            if (result.isSuccess()) {


                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
                //Toast.makeText(this, "Signing in", Toast.LENGTH_SHORT).show();
            }


        } else {
            Toast.makeText(this, "Failed to get google account", Toast.LENGTH_SHORT).show();
            loadingBar.dismiss();
        }
    }


    // sign in with account
    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {

        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            checkForNewUser();

                        } else {
                            // If sign in fails, display a message to the user.
                            sendUserToLoginActivity();
                            // toast is better here cuz it's a system message. R.id.welcome_layout: The view to find a parent from.
                            Snackbar.make(findViewById(R.id.welcome_layout), "Failed to sign in", Snackbar.LENGTH_SHORT).show();
                            loadingBar.dismiss();
                        }

                    }
                });

    }


    private void checkForNewUser() {

        // check if it is a new user
        final FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            mDatabaseReference.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.child(user.getUid()).getValue() == null) { // new user. OR: if !dataSnapshot.child(user.getUid()).exists
                        saveNameToDatabase();

                    } else {
                        sendUserToHomeActivity();
                        loadingBar.dismiss();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }


    private void saveNameToDatabase() {

        final FirebaseUser user = mAuth.getCurrentUser();
        // save username to database
        mDatabaseReference.child("Users").child(user.getUid()).child("name").setValue(user.getDisplayName())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Uri userPhotoUrl = user.getPhotoUrl();
                            if (userPhotoUrl != null) {
                                String photo = userPhotoUrl.toString();
                                savePhotoToStorage(photo);
                                savePhotoToDatabase(photo);
                            }
                        } else {
                            loadingBar.dismiss();
                            sendUserToLoginActivity();
                        }
                    }
                });
    }

    private void savePhotoToStorage(String photo) {

        FirebaseUser user = mAuth.getCurrentUser();

        StorageReference filePath = mUserImageRef.child(user.getUid() + ".jpg");

        new UploadImageToStorageTask(filePath).execute(photo);// pass the photo url
    }


    private void savePhotoToDatabase(String photo) {

        final FirebaseUser user = mAuth.getCurrentUser();

        // save user photo download url to database
        mDatabaseReference.child("Users").child(user.getUid()).child("profileImage").setValue(photo)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            sendUserToHomeActivity();
                            loadingBar.dismiss(); // dismiss loadingBar when sign-in fails or succeeds

                        } else {
                            loadingBar.dismiss();// dismiss loadingBar when sign-in fails or succeeds
                            sendUserToLoginActivity();
                        }
                    }
                });
    }

    private void sendUserToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    private void sendUserToHomeActivity() {
        Intent intent = new Intent(this, HomeActivity.class);
        // Intent.FLAG_ACTIVITY_NEW_TASK: this activity will become the start of a new task on this history stack. so later when press back button on home activity, it will not go to login activity
        // Intent.FLAG_ACTIVITY_CLEAR_TASK: causes any existing task to be cleared before the activity is started. That is, the activity becomes the new root of an otherwise empty task, and any old activities are finished. This can only be used in conjunction with FLAG_ACTIVITY_NEW_TASK
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);


    }


    private void attemptEmailLogin() {
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        if (email.equals("") || password.equals("")) {
            Toast.makeText(this, "Email and password are required", Toast.LENGTH_SHORT).show();
            return;
        }

        // Use FirebaseAuth to sign in with email & password
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()) {
                    showErrorDialog("There was a problem signing in, please try again");
                } else {
                    sendUserToHomeActivity();
                }
            }
        });
    }

    public void registerNewUser() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    private void showErrorDialog(String message) {
        new AlertDialog.Builder(this)
                .setTitle("Oops")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .setIcon(android.R.drawable.ic_dialog_alert) // VS (com.christy.tripline.R.drawable...
                .show();

    }

    public void showInputFields() {

        mEmailTextInputLayout.setVisibility(View.VISIBLE);
        mPasswordInputLayout.setVisibility(View.VISIBLE);
        mEmailSignInButton.setVisibility(View.VISIBLE);
        mRegisterButton.setVisibility(View.VISIBLE);
    }
}

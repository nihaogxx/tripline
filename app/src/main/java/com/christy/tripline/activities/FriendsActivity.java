package com.christy.tripline.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.christy.tripline.R;
import com.christy.tripline.adapters.FriendsRecyclerAdapter;
import com.christy.tripline.adapters.PeopleRecyclerAdapter;
import com.christy.tripline.adapters.RequestRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.LinkedList;

public class FriendsActivity extends AppCompatActivity {

    private RecyclerView friendsRecyclerView;
    private RecyclerView peopleRecyclerView;
    private RecyclerView requestRecyclerView;
    private FriendsRecyclerAdapter friendsAdapter;
    private PeopleRecyclerAdapter peopleAdapter;
    private RequestRecyclerAdapter requestAdapter;
    private TextView peopleHead;
    private TextView requestHead;
    private TextView friendHead;
    private EditText searchInput;
    private Button clearButton;

    private String uId;
    private DatabaseReference friendsRef;
    private DatabaseReference friendRequestsRef;
    private DatabaseReference usersRef;

    //Listeners
    private ChildEventListener friendRequestsListener;
    private ChildEventListener friendListListener;

    // for friends request
    private LinkedList<String> rUrlList = new LinkedList<>();
    private LinkedList<String> rNameList = new LinkedList<>();
    private LinkedList<String> rLocationList = new LinkedList<>();
    private LinkedList<String> rIdList = new LinkedList<>();
    // for friends
    private LinkedList<String> fUrlList;
    private LinkedList<String> fNameList;
    private LinkedList<String> fLocationList;
    private LinkedList<String> fIdList;
    // for other ppl
    private LinkedList<String> pUrlList = new LinkedList<>();
    private LinkedList<String> pNameList = new LinkedList<>();
    private LinkedList<String> pLocationList = new LinkedList<>();
    private LinkedList<String> pIdList = new LinkedList<>();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);

        // set action bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        // set title of the action bar
        TextView titleView = findViewById(R.id.app_bar_title);
        titleView.setText("Friends");

        //UI
        friendsRecyclerView = findViewById(R.id.friends_list);
        peopleRecyclerView = findViewById(R.id.people_list);
        requestRecyclerView = findViewById(R.id.request_list);
        searchInput = findViewById(R.id.search_input);
        friendsRecyclerView.requestFocus();
        clearButton = findViewById(R.id.search_input_clear_button);
        peopleHead = findViewById(R.id.people_head);
        requestHead = findViewById(R.id.request_head);
        friendHead = findViewById(R.id.friends_head);

        // References
        FirebaseAuth auth = FirebaseAuth.getInstance();
        uId = auth.getCurrentUser().getUid();
        friendRequestsRef = FirebaseDatabase.getInstance().getReference().child("FriendRequests");
        friendsRef = FirebaseDatabase.getInstance().getReference().child("Friends");
        usersRef = FirebaseDatabase.getInstance().getReference().child("Users");


        // clear button on click listener
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                searchInput.getText().clear();

            }
        });

        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {

                //clear old data
                clearOldData();
                // search in friends, if s == "", all friends will show
                searchThroughFriends(s);

                if (s.length() == 0) {
                    peopleHead.setVisibility(View.INVISIBLE);

                } else if (s.length() > 0) {

                    searchThroughUsers(s);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                // clear button
                if(searchInput.getText().toString().length() > 0) {
                    clearButton.setVisibility(View.VISIBLE);
                }else clearButton.setVisibility(View.INVISIBLE);

            }
        });
    }

    private void searchThroughUsers(final CharSequence s) {

        usersRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot userData : dataSnapshot.getChildren()) {

                    String name = userData.child("name").getValue().toString();
                    if (name.toLowerCase().startsWith(s.toString().toLowerCase())){
                        String id = userData.getKey();
                        String url = "";
                        if (userData.hasChild("profileImage")) {
                            url = userData.child("profileImage").getValue().toString();
                        }
                        String location = "";
                        if (userData.hasChild("location")) {
                            location = userData.child("location").getValue().toString();
                        }

                        pNameList.add(name);
                        pIdList.add(id);
                        pLocationList.add(location);
                        pUrlList.add(url);
                        peopleAdapter.notifyItemInserted(pNameList.size()-1);
                    }
                }
                if (pNameList.size() == 0) {
                    peopleHead.setVisibility(View.GONE);
                } else peopleHead.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void searchThroughFriends(final CharSequence s) {

        friendsRef.child(uId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot friendData : dataSnapshot.getChildren()) {

                    final String friendId  = friendData.getKey();
                    usersRef.child(friendId).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            String name = dataSnapshot.child("name").getValue().toString();

                            if (name.toLowerCase().startsWith(s.toString().toLowerCase())) {

                                String profileUrl = "";
                                if (dataSnapshot.hasChild("profileImage")) {
                                    profileUrl = dataSnapshot.child("profileImage").getValue().toString();
                                }
                                String location = "";
                                if (dataSnapshot.hasChild("location")){
                                    location = dataSnapshot.child("location").getValue().toString();
                                }

                                fIdList.add(friendId);
                                fNameList.add(name);
                                fLocationList.add(location);
                                fUrlList.add(profileUrl);
                                friendsAdapter.notifyItemInserted(fNameList.size()-1);
                            }

                            if (fIdList.size() == 0) {
                                friendHead.setVisibility(View.GONE);
                            } else friendHead.setVisibility(View.VISIBLE);
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        friendsAdapter.notifyItemRangeInserted(0, fNameList.size());
    }



    private void clearOldData() {
        // clear old data from friends lists
        int count = fIdList.size();
        fIdList.clear();
        fLocationList.clear();
        fNameList.clear();
        fUrlList.clear();
        friendsAdapter.notifyItemRangeRemoved(0, count);

        // clear old data from people lists
        // clear people lists & hide people view
        count = pIdList.size();
        pIdList.clear();
        pLocationList.clear();
        pNameList.clear();
        pUrlList.clear();
        peopleAdapter.notifyItemRangeRemoved(0, count);
        peopleHead.setVisibility(View.INVISIBLE);
    }

    private void setUpFriendsListListener() {

        friendListListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                final String friendId = dataSnapshot.getKey();
                fIdList.add(friendId);

                usersRef.child(friendId).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String name = dataSnapshot.child("name").getValue().toString();

                        String profileUrl = "";
                        if (dataSnapshot.hasChild("profileImage")) {
                            profileUrl = dataSnapshot.child("profileImage").getValue().toString();
                        }
                        String location = "";
                        if (dataSnapshot.hasChild("location")){
                            location = dataSnapshot.child("location").getValue().toString();
                        }

                        fNameList.add(name);
                        fLocationList.add(location);
                        fUrlList.add(profileUrl);
                        friendsAdapter.notifyItemInserted(fNameList.size()-1);

                        friendHead.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });



            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                String id = dataSnapshot.getKey();
                int index = fIdList.indexOf(id);
                fIdList.remove(index);
                fNameList.remove(index);
                fLocationList.remove(index);
                fUrlList.remove(index);

                friendsAdapter.notifyItemRemoved(index);

                if (fIdList.size() == 0) {

                    friendHead.setVisibility(View.GONE);
                } else friendHead.setVisibility(View.VISIBLE);

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };


    }

    private void setUpFriendRequestsListener() {
        // show a list of friend requests
        friendRequestsListener =  new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                String requestType = dataSnapshot.child("requestType").getValue().toString();
                if (requestType.equals("received")) { // received a request from this person
                    final String id = dataSnapshot.getKey();
                    rIdList.add(id);
                    requestHead.setVisibility(View.VISIBLE);

                    usersRef.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            String name = dataSnapshot.child("name").getValue().toString();

                            String profileUrl = "";
                            if (dataSnapshot.hasChild("profileImage")) {
                                profileUrl = dataSnapshot.child("profileImage").getValue().toString();
                            }
                            String location = "";
                            if (dataSnapshot.hasChild("location")) {
                                location = dataSnapshot.child("location").getValue().toString();
                            }

                            rNameList.add(name);

                            rLocationList.add(location);
                            rUrlList.add(profileUrl);
                            requestAdapter.notifyItemInserted(rNameList.size() - 1);

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                }

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {// called multiple times

                String requestType = dataSnapshot.child("requestType").getValue().toString();
                //if uId is the requester it wasn't added to requestList, so when cancel request it
                // cannot be found in the list
                if (requestType.equals("received")) {
                    String id = dataSnapshot.getKey();

                    int index = rIdList.indexOf(id);
                    rIdList.remove(index);
                    rNameList.remove(index);
                    rLocationList.remove(index);
                    rUrlList.remove(index);
                    requestAdapter.notifyItemRemoved(index);

                    if (rIdList.size() == 0) {
                        requestHead.setVisibility(View.GONE);
                    }
                }


            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

    }

    private void setUpAdapters() {
        // set up adapter to display a list of requests
        requestRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        requestAdapter = new RequestRecyclerAdapter(this, rUrlList,
                rNameList, rLocationList, rIdList);
        requestRecyclerView.setAdapter(requestAdapter);

        // set up adapter to display a list of friends
        friendsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        friendsAdapter = new FriendsRecyclerAdapter(this, fUrlList, fNameList, fLocationList, fIdList);
        friendsRecyclerView.setAdapter(friendsAdapter);

        // set up adapter to display other people
        peopleRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        peopleAdapter = new PeopleRecyclerAdapter(this, pUrlList, pNameList, pLocationList, pIdList);
        peopleRecyclerView.setAdapter(peopleAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();

        fUrlList = new LinkedList<>();
        fNameList = new LinkedList<>();
        fLocationList = new LinkedList<>();
        fIdList = new LinkedList<>();

        setUpAdapters();
        setUpFriendsListListener();
        setUpFriendRequestsListener();
        friendRequestsRef.child(uId).addChildEventListener(friendRequestsListener);
        friendsRef.child(uId).addChildEventListener(friendListListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        friendRequestsRef.child(uId).removeEventListener(friendRequestsListener);
        friendsRef.child(uId).removeEventListener(friendListListener);
    }


}

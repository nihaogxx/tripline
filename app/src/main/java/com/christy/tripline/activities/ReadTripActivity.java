package com.christy.tripline.activities;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.widget.TextView;
import com.christy.tripline.R;
import com.christy.tripline.adapters.ViewOnlyRecyclerAdapter;

public class ReadTripActivity extends AppCompatActivity {

    private TextView titleView;
    private String mTitle, mTripDuration, mStartDate, uId;
    private ViewOnlyRecyclerAdapter mRecyclerAdapter;
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_trip);


        // get info of the trip
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            mTitle = intent.getExtras().getString("title");
            mTripDuration = intent.getExtras().getString("duration");
            mStartDate = intent.getExtras().getString("startDate");
            uId = intent.getExtras().getString("uId");
            mRecyclerAdapter = new ViewOnlyRecyclerAdapter(this, mTitle, mTripDuration, mStartDate, uId);
        }

        // set action bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        // set title of the action bar
        titleView = findViewById(R.id.app_bar_title);
        titleView.setText(mTitle);


        //set up adapter
        mRecyclerView = findViewById(R.id.list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mRecyclerAdapter);

    }
}

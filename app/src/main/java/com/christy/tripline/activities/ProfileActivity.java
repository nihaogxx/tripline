package com.christy.tripline.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.christy.tripline.R;
import com.christy.tripline.Tasks.GetCountryCodeTask;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import java.util.Arrays;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    private static final int RC_GALLERY_PICK = 1;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 3;
    private String oldGender, newGender, oldIntroduction = "", newIntroduction, oldLocation, newLocation, oldName, newName;

    private LinearLayout mLinearLayout;
    private CircleImageView mImageView;
    private TextView mUsernameView;
    private ProgressDialog loadingBar;
    private Button submitButton;
    private RadioGroup genderGroup;
    private EditText intro;
    private TextView locationView;


    private StorageReference mUserImageRef;
    private DatabaseReference mDatabaseReference;
    private String uID;
    private FirebaseAuth mAuth;
    private ValueEventListener nameEventListener;
    private ValueEventListener picEventListener;
    private static String APP_API_KEY;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        APP_API_KEY = getString(R.string.app_api_key);

        mLinearLayout = findViewById(R.id.profile_layout);
        mImageView = findViewById(R.id.profile_image);
        mUsernameView = findViewById(R.id.profile_user_name);
        loadingBar = new ProgressDialog(this);
        submitButton = findViewById(R.id.submit_button);
        genderGroup = findViewById(R.id.gender_group);
        intro = findViewById(R.id.input_body);
        locationView = findViewById(R.id.input_location);


        mAuth = FirebaseAuth.getInstance();
        uID = mAuth.getCurrentUser().getUid();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mUserImageRef = FirebaseStorage.getInstance().getReference().child("Profile Images");

        mDatabaseReference.child("Users").child(uID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild("name")){
                    oldName = dataSnapshot.child("name").getValue().toString();
                }
                if (dataSnapshot.hasChild("gender")) {
                    oldGender = dataSnapshot.child("gender").getValue().toString();
                    // check audio button
                    if (oldGender.equals("female")) {
                        genderGroup.check(R.id.female_button);
                    } else genderGroup.check(R.id.male_button);
                } else genderGroup.check(R.id.female_button);

                if (dataSnapshot.hasChild("intro")) {
                    oldIntroduction = dataSnapshot.child("intro").getValue().toString();
                    intro.setText(oldIntroduction);

                } else oldIntroduction = "";
                newIntroduction = oldIntroduction;

                if (dataSnapshot.hasChild("location")) {
                    oldLocation = dataSnapshot.child("location").getValue().toString();
                    locationView.setText(oldLocation);
                } else oldLocation = "";
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // send user to gallery
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, RC_GALLERY_PICK);

            }
        });

        locationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchAutoComplete();
            }
        });

        // initialize Places
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), APP_API_KEY);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        mUsernameView.requestFocus();// to avoid black background of circle image view

        // name change listener
        nameEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    String name = dataSnapshot.getValue().toString();
                    mUsernameView.setText(name);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        mDatabaseReference.child("Users").child(uID).child("name").addValueEventListener(nameEventListener);

        // pic change listener
        picEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    String image = dataSnapshot.getValue().toString();
                    Picasso.with(ProfileActivity.this).load(image).placeholder(R.drawable.no_image_icon).into(mImageView);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        mDatabaseReference.child("Users").child(uID).child("profileImage").addValueEventListener(picEventListener);

        // Keyboard change username action: when press done button
        mUsernameView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == 300 || actionId == EditorInfo.IME_ACTION_DONE) {

                    // hide keyboard
                    View currentFocus = getCurrentFocus();
                    if (currentFocus != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
                        // OR findViewById.getWindowToken()
                    }

                    // update display name
                    checkNameDuplicate();

                    return true;
                }

                return false;
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // get latest checked radio button
                int id = genderGroup.getCheckedRadioButtonId();
                if (id == R.id.female_button) {
                    newGender = "female";
                } else {
                    newGender = "male";
                }
                // save gender
                mDatabaseReference.child("Users").child(uID).child("gender").setValue(newGender);

                // save intro
                newIntroduction = intro.getText().toString();
                if (!newIntroduction.equals(oldIntroduction)) {
                    mDatabaseReference.child("Users").child(uID).child("intro").setValue(newIntroduction);
                }

                // save location
                newLocation = locationView.getText().toString();
                if (!newLocation.equals(oldLocation)) {
                    mDatabaseReference.child("Users").child(uID).child("location").setValue(newLocation);
                }

                finish();
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        mDatabaseReference.child("Users").child(uID).child("name").removeEventListener(nameEventListener);
        mDatabaseReference.child("Users").child(uID).child("profileImage").removeEventListener(picEventListener);
        mUsernameView.setOnEditorActionListener(null);
    }

    private void launchAutoComplete() {

        List<Place.Field> fields = Arrays.asList(Place.Field.NAME, Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                .setTypeFilter(TypeFilter.CITIES)
                .build(ProfileActivity.this);
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }


    @SuppressWarnings("unused")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {  // for location

            if (resultCode == Activity.RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                String placeName = place.getName().toString();
                LatLng coordinates = place.getLatLng();

                // get country code and name in background
                new GetCountryCodeTask(this, placeName).execute(coordinates);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String location = GetCountryCodeTask.getDestination();
                        locationView.setText(location);
                    }
                }, 2000);

            }
        }

        if (requestCode == RC_GALLERY_PICK && resultCode == RESULT_OK
                && data != null) {// user picked a pic

            CropImage.activity() // enabling crop activity
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1, 1)
                    .start(this);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) { // user clicked on Crop button
                Uri resultUri = result.getUri();
                loadingBar.setTitle("Saving Information");
                loadingBar.setMessage("Please wait, we are updating your profile picture...");
                loadingBar.setCanceledOnTouchOutside(true);
                loadingBar.show();

                // store image to Firebase storage
                storeImageToStorage(resultUri);

            } else {
            }
        }

    }

    private void storeImageToStorage(Uri photoUrl) {
        final StorageReference filePath = mUserImageRef.child(uID + ".jpg");
        filePath.putFile(photoUrl) // upload a local file to storage
                .addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        if (task.isSuccessful()) {
                            // get download url
                            filePath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {

                                    String photoUrl = uri.toString();

                                    // store the download url to firebase database
                                    storeImageToDatabase(photoUrl);
                                }
                            });

                        } else {
                            Toast.makeText(ProfileActivity.this,
                                    "profile image failed to update", Toast.LENGTH_SHORT).show();
                            loadingBar.dismiss();
                        }
                    }
                });

    }

    private void storeImageToDatabase(String downloadUrl) {

        mDatabaseReference.child("Users").child(uID).child("profileImage")
                .setValue(downloadUrl).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(ProfileActivity.this,"profile image updated", Toast.LENGTH_SHORT).show();
                    loadingBar.dismiss();

                } else {
                    Toast.makeText(ProfileActivity.this,
                            "profile image failed to update", Toast.LENGTH_SHORT).show();
                    loadingBar.dismiss();
                }

            }
        });
    }

    private void checkNameDuplicate() {

        newName = mUsernameView.getText().toString();

        if (!newName.equals(oldName)){
            // check name duplicate
            FirebaseDatabase.getInstance().getReference().child("Users")
                    // called once and then immediately removed, at initial data or data change
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            boolean cancel = false;

                            // must store disPlayName here otherwise cannot resolve it
                            String disPlayName = mUsernameView.getText().toString();

                            if (TextUtils.isEmpty(disPlayName)) {
                                mUsernameView.setError("This field is required");
                                cancel = true;
                            } else {
                                // checking for existing username
                                for (DataSnapshot idSnapshot : dataSnapshot.getChildren()) {
                                    String s = (String) idSnapshot.child("name").getValue();// idSnapshot.getValue(User.class).getName
                                    if (s != null && s.equals(disPlayName)) {
                                        mUsernameView.setError("This name is taken");
                                        cancel = true;
                                        break;
                                    }
                                }
                            }

                            if (!cancel) {
                                updateName(newName);
                                mUsernameView.clearFocus(); // remove cursor and focus
                                mLinearLayout.requestFocus(); // not necessary
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

        }
    }

    private void updateName(String name) {

        // update in database
        mDatabaseReference.child("Users").child(uID).child("name").setValue(name)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(ProfileActivity.this, "Username updated", Toast.LENGTH_SHORT).show();
                            loadingBar.dismiss();
                        } else {
                            Toast.makeText(ProfileActivity.this, "Cannot update username, please try again",
                                    Toast.LENGTH_SHORT).show();
                            loadingBar.dismiss();
                        }
                    }
                });


    }


}

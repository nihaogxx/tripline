package com.christy.tripline.activities;

import com.christy.tripline.R;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.PhotoMetadata;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPhotoRequest;
import com.google.android.libraries.places.api.net.FetchPhotoResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import androidx.annotation.NonNull;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.christy.tripline.model.LodgingItemModel;
import com.christy.tripline.model.PlaceItemModel;
import com.christy.tripline.model.TransitItemModel;
import com.christy.tripline.fragments.DatePicker;
import com.christy.tripline.fragments.TimePicker;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import cz.msebera.android.httpclient.Header;

public class AddPlaceActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private MapView mMapView;
    private DatabaseReference tripRef;
    private TextView checkInView;
    private TextView checkOutView;
    private TextView departureView;
    private TextView arrivalView;
    private EditText transitNumView;
    private TextView dateView;
    private LinearLayout reviews;
    private ScrollView mScrollView;
    private String title, placeID, dayNumber, placeName, placeType, countryCode, tag;
    private static final String URL_DETAIL_REQUEST = "https://maps.googleapis.com/maps/api/place/details/json?";
    private static String APP_API_KEY;
    private PlacesClient mPlacesClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_place);

        APP_API_KEY = getString(R.string.app_api_key);

        // set up map view
        mMapView = findViewById(R.id.viewport_map);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);

        title = getIntent().getExtras().getString("title");
        dayNumber = getIntent().getExtras().getString("dayNumber");
        countryCode = getIntent().getExtras().getString("countryCode");
        tag = getIntent().getExtras().getString("tag");

        String uid = FirebaseAuth.getInstance().getUid();
        tripRef = FirebaseDatabase.getInstance().getReference().child("Users").child(uid)
                .child("Trips").child(title);


        // UI
        final TextView nameView = findViewById(R.id.place_name);
        final TextView typeView = findViewById(R.id.place_type);
        final TextView ratingView = findViewById(R.id.place_rating);
        final TextView addressView = findViewById(R.id.place_address);
        final TextView openView = findViewById(R.id.open_now);
        final TextView hoursView = findViewById(R.id.open_hours);
        final LinearLayout gallery = findViewById(R.id.place_gallery);
        final FloatingActionButton addPlaceButton = findViewById(R.id.add_place_button);
        final RatingBar ratingBar = findViewById(R.id.rating_bar);
        final TextView optional = findViewById(R.id.optional);
        final LinearLayout lodgingLayout = findViewById(R.id.lodging_dates);
        final LinearLayout transitLayout = findViewById(R.id.transit_info);
        final ImageView bookmark = findViewById(R.id.bookmark);
        final LinearLayout hoursLayout = findViewById(R.id.hours_layout);
        final ImageView callView = findViewById(R.id.call);
        final ImageView websiteView = findViewById(R.id.website);
        final View hoursDivider = findViewById(R.id.hours_divider);
        transitNumView = findViewById(R.id.transit_num);
        dateView = findViewById(R.id.input_date);
        checkInView = findViewById(R.id.check_in_date);
        checkOutView = findViewById(R.id.check_out_date);
        departureView = findViewById(R.id.input_departure_time);
        arrivalView = findViewById(R.id.input_arrival_time);
        reviews = findViewById(R.id.reviews);
        mScrollView = findViewById(R.id.info_scroller);


        CardView autoCompleteCard = findViewById(R.id.autocomplete_card);
        autoCompleteCard.requestFocus();

        //Initialize the places SDK client
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), APP_API_KEY);
        }

        // Create a new Places client instance.
        mPlacesClient = Places.createClient(this);

        // creates a reference to the fragment
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        // set country: the search scope
        autocompleteFragment.setCountry(countryCode);
        // set fields that we need
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.PHOTO_METADATAS,
                Place.Field.TYPES, Place.Field.ADDRESS, Place.Field.OPENING_HOURS, Place.Field.PHONE_NUMBER,
                Place.Field.LAT_LNG, Place.Field.RATING, Place.Field.VIEWPORT, Place.Field.WEBSITE_URI));


        // add a listener
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(final Place place) {

                mScrollView.setVisibility(View.VISIBLE);

                if (tag.equals("lodging")) {

                    optional.setVisibility(View.VISIBLE);
                    lodgingLayout.setVisibility(View.VISIBLE);

                } else if (tag.equals("transit")) {
                    optional.setVisibility(View.VISIBLE);
                    transitLayout.setVisibility(View.VISIBLE);

                }

                // clear gallery for new item
                gallery.removeAllViews();

                // get place ID
                placeID = place.getId();

                // set bookmark
                final String[] bookmarkId = {""};
                bookmark.setImageResource(R.drawable.ic_bookmark_add);
                bookmark.setSelected(false);

                tripRef.child("Bookmarks").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        for (DataSnapshot data : dataSnapshot.getChildren()) {

                            // it is bookmarked
                            if (data.getValue().toString().equals(placeID)) {
                                bookmarkId[0] = data.getKey();
                                bookmark.setImageResource(R.drawable.ic_bookmark);
                                bookmark.setSelected(true);
                                break;
                            }

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                bookmark.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        v.setSelected(!v.isSelected());

                        if (v.isSelected()) {

                            bookmark.setImageResource(R.drawable.ic_bookmark);
                            // save in database
                            DatabaseReference bookmarkRef = tripRef.child("Bookmarks").push();
                            bookmarkRef.setValue(placeID);
                            bookmarkId[0] = bookmarkRef.getKey();


                        } else if (!v.isSelected()) {

                            bookmark.setImageResource(R.drawable.ic_bookmark_add);
                            // remove from database
                            tripRef.child("Bookmarks").child(bookmarkId[0]).removeValue();

                        }
                    }
                });

                // get photos
                List<PhotoMetadata> photos = place.getPhotoMetadatas();
                if (photos != null) {

                    for (int i = 0; i < photos.size(); i++) {

                        final View photoView = LayoutInflater.from(AddPlaceActivity.this).inflate(R.layout.single_photo_horizental,
                                gallery, false);

                        final ImageView imageView = photoView.findViewById(R.id.place_photo);
                        final TextView attriView = photoView.findViewById(R.id.photo_attributions);
                        // get photo data
                        final PhotoMetadata photo = photos.get(i);
                        // get attributions: <a href="https://maps.google.com/maps/contrib/100587037508060120583">Tony Hanjraa</a>
                        String attrOri = photo.getAttributions();
                        attrOri = attrOri.substring(3, attrOri.length()-4);
                        final String link = attrOri.substring(6, attrOri.indexOf('>')-1);
                        final String author = attrOri.substring(attrOri.indexOf('>')+1);
                        // Create a FetchPhotoRequest.
                        FetchPhotoRequest photoRequest = FetchPhotoRequest.builder(photo).build();
                        mPlacesClient.fetchPhoto(photoRequest).addOnSuccessListener(new OnSuccessListener<FetchPhotoResponse>() {
                            @Override
                            public void onSuccess(FetchPhotoResponse fetchPhotoResponse) {
                                Bitmap bitmap = fetchPhotoResponse.getBitmap();
                                imageView.setImageBitmap(bitmap);
                                attriView.setText("by " + author);
                                attriView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                                        startActivity(intent);
                                    }
                                });
                                gallery.addView(photoView);

                            }
                        });
                    }
                }

                // get name
                placeName = place.getName();
                nameView.setText(placeName);

                // get type
                if (place.getTypes() != null) {

                    Place.Type type = place.getTypes().get(0); // Emnu
                    placeType = type.toString().toLowerCase();
                    typeView.setText(placeType);
                } else typeView.setVisibility(View.GONE);


                // get rating for place/lodging
                if (tag.equals("place") || tag.equals("lodging")) {
                    if (place.getRating() != null){
                    float rating = place.getRating().floatValue(); // place.getRating() returns a Double, so cant simply put (float)place.getRating().
                        ratingView.setVisibility(View.VISIBLE);
                        ratingBar.setVisibility(View.VISIBLE);
                        DecimalFormat df = new DecimalFormat("#.#");
                        String placeRating = df.format(rating);
                        ratingView.setText(placeRating);
                        ratingBar.setRating(rating);
                    } else {
                        ratingView.setVisibility(View.GONE);
                        ratingBar.setVisibility(View.GONE);
                    }
                } else {
                    ratingView.setVisibility(View.GONE);
                    ratingBar.setVisibility(View.GONE); // hide rating for transit
                }



                // get Address
                String address = (String) place.getAddress();
                addressView.setText(address);


                // get phone#
                final String phoneNumber = place.getPhoneNumber();

                callView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (phoneNumber != null && !phoneNumber.isEmpty()) {

                            startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber)));

                        } else {

                            Toast.makeText(AddPlaceActivity.this, "Call is not available for this place",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });


                // get website link
                websiteView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (place.getWebsiteUri() != null) {
                            String link = place.getWebsiteUri().toString();

                            if (!link.startsWith("http://") && !link.startsWith("https://")) {

                                link = "https://" + link;
                            }
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                            startActivity(intent);

                        } else {

                            Toast.makeText(AddPlaceActivity.this, "Website is not available for this place",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                });


                // get location
                final LatLng location = place.getLatLng();

                // get viewport
                LatLngBounds viewport = place.getViewport();

                // move camera after getting location and viewport
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(viewport, 500, 400, 0);
                mMap.moveCamera(cu);
                mMap.addMarker(new MarkerOptions().position(location));
                addPlaceButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        checkPlaceDuplicate();
                    }
                });


                // get opening hours
                if(place.getOpeningHours() != null){
                    hoursLayout.setVisibility(View.VISIBLE);
                    hoursDivider.setVisibility(View.VISIBLE);
                    List<String> hours = place.getOpeningHours().getWeekdayText();

                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < hours.size(); i++) {

                         sb.append(hours.get(i)).append("\n").append("\n");

                    }
                    hoursView.setText(sb.toString());
                }

                // open now?
                String url = URL_DETAIL_REQUEST + "placeid=" + placeID + "&fields=opening_hours&key=" + APP_API_KEY;
                AsyncHttpClient client = new AsyncHttpClient();
                client.get(url, new JsonHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                        try {
                            JSONObject result = response.getJSONObject("result");
                            if (result.has("opening_hours")) {

                                if (result.getJSONObject("opening_hours").has("open_now")) {

                                    String open = "" + result.getJSONObject("opening_hours").getBoolean("open_now");
                                    if (open.equals("true")) {

                                        openView.setText("open now");

                                    } else if (open.equals("false")) {
                                        openView.setText("closed now");
                                    }

                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                    }
                });


                // get reviews via Place Details Requests
                String reviewUrl = URL_DETAIL_REQUEST + "placeid=" + placeID + "&fields=review&key=" + APP_API_KEY;

                AsyncHttpClient reviewClient = new AsyncHttpClient();
                reviewClient.get(reviewUrl, new JsonHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                        try {
                            reviews.removeAllViews();
                            if (response.getJSONObject("result").has("reviews")) {

                                for (int i = 0; i < response.getJSONObject("result").getJSONArray("reviews").length(); i++) {

                                    View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.single_review,
                                            reviews, false);

                                    ImageView profileView = view.findViewById(R.id.photo);
                                    TextView nameView = view.findViewById(R.id.author);
                                    RatingBar ratingBar = view.findViewById(R.id.rating_bar);
                                    TextView timeView = view.findViewById(R.id.time);
                                    TextView textView = view.findViewById(R.id.text);

                                    JSONObject review = response.getJSONObject("result").getJSONArray("reviews").getJSONObject(i);
                                    String author = review.getString("author_name");
                                    double rating = review.getDouble("rating");
                                    String time = review.getString("relative_time_description");
                                    String text = review.getString("text");
                                    String photoUrl = review.getString("profile_photo_url");

                                    Picasso.with(AddPlaceActivity.this).load(photoUrl).placeholder(R.drawable.no_image_icon).into(profileView);
                                    nameView.setText(author);
                                    ratingBar.setRating((float) rating);
                                    timeView.setText(time);
                                    textView.setText(text);
                                    reviews.addView(view);
                                }

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                    }
                });


            }

            @Override
            public void onError(Status status) {

            }
        });
    }

    private void checkPlaceDuplicate() {

        tripRef.child("Days").child(dayNumber).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                boolean cancel = false;
                for (DataSnapshot itemData : dataSnapshot.getChildren()) {
                    // note does not have child "placeId"
                    if (itemData.child("placeId").exists()) {
                        String id = itemData.child("placeId").getValue().toString();
                        if (placeID.equals(id)) {
                            Toast.makeText(getBaseContext(), "Item already exists in " + dayNumber, Toast.LENGTH_SHORT).show();
                            cancel = true;
                        }
                    }
                }

                if (!cancel) { //if no duplicate

                    // save to database
                    DatabaseReference ref = tripRef.child("Days").child(dayNumber).push();
                    switch (tag) {
                        case "place":
                            PlaceItemModel newPlace = new PlaceItemModel("place", placeID, placeName, placeType);
                            ref.setValue(newPlace);
                            Toast.makeText(getBaseContext(), "Adding place to " + dayNumber, Toast.LENGTH_SHORT).show();
                            break;

                        case "lodging":

                            String checkInDate = checkInView.getText().toString();
                            String checkOutDate = checkOutView.getText().toString();
                            String dates = checkInDate + " - " + checkOutDate;

                            LodgingItemModel newLodging = new LodgingItemModel("lodging", placeID, placeName, dates);
                            ref.setValue(newLodging);
                            Toast.makeText(getBaseContext(), "Adding Lodging to " + dayNumber, Toast.LENGTH_SHORT).show();
                            break;

                        case "transit":

                            String transitNum = transitNumView.getText().toString();
                            String date = dateView.getText().toString();
                            String departureTime = departureView.getText().toString();
                            String arrivalTime = arrivalView.getText().toString();
                            String time = departureTime + " - " + arrivalTime;

                            TransitItemModel newTransit = new TransitItemModel("transit", placeID, placeName,
                                    transitNum, date, time);
                            ref.setValue(newTransit);
                            Toast.makeText(getBaseContext(), "Adding Transit to " + dayNumber, Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void datePickerListener(View view) {

        DatePicker newFragment = new DatePicker();
        Bundle bundle = new Bundle();
        switch (view.getId()) {
            case R.id.check_in_date:
                bundle.putString("tag", "CheckIn");
                break;
            case R.id.check_out_date:
                bundle.putString("tag", "CheckOut");
                break;
            case R.id.input_date:
                bundle.putString("tag", "transit");
                break;
        }
        newFragment.setArguments(bundle);
        newFragment.show(getSupportFragmentManager(), "DatePicker");

    }

    public void getDatePickerResult(String date, String tag) {

        switch (tag) {
            case "CheckIn":

                checkInView.setText(date);

                break;
            case "CheckOut":
                checkOutView.setText(date);

                break;
            case "transit":
                dateView.setText(date);
                break;
        }

    }

    public void getTimePickerResult(String time, String tag) {

        if (tag.equals("departure")) {

            departureView.setText(time);

        } else if (tag.equals("arrival")) {
            arrivalView.setText(time);
        }

    }

    public void timePickerListener(View view) {
        TimePicker newFragment = new TimePicker();
        Bundle bundle = new Bundle();
        if (view.getId() == R.id.input_departure_time) {
            bundle.putString("tag", "departure");
        } else if (view.getId() == R.id.input_arrival_time) {
            bundle.putString("tag", "arrival");
        }
        newFragment.setArguments(bundle);
        newFragment.show(getSupportFragmentManager(), "TimePicker");


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

    }
    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }


    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}

package com.christy.tripline.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import androidx.annotation.NonNull;

import com.christy.tripline.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.PhotoMetadata;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPhotoRequest;
import com.google.android.libraries.places.api.net.FetchPhotoResponse;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.material.tabs.TabLayout;
import androidx.core.app.ActivityCompat;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.christy.tripline.fragments.ReadMoreAndEditFragment;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PointOfInterest;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;
import java.util.List;


public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnPoiClickListener
        , GoogleMap.InfoWindowAdapter {

    private GoogleMap mMap;
    private String mTitle, mCoord, mDuration;
    private static float ZOOM_LEVEL_CITY = 10;
    private static int REQUEST_CODE_LOCATION_PERMISSION = 18;
    private DatabaseReference tripRef;
    private CardView mapCard;
    private TabLayout dayButtons;
    private View infoWindowView;
    private Marker focusedMarker = null;
    private PlacesClient mPlacesClient;
    private static String APP_API_KEY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        APP_API_KEY = getString(R.string.app_api_key);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent intent = getIntent();
        mTitle = intent.getExtras().getString("title");
        mCoord = intent.getExtras().getString("coord");


        // set action bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        // set title of the action bar
        TextView titleView = findViewById(R.id.app_bar_title);
        titleView.setText(mTitle);

        // UI
        mapCard = findViewById(R.id.map_card);
        dayButtons = findViewById(R.id.day_buttons);
        dayButtons.addTab(dayButtons.newTab().setText("ALL"));


        //get database reference
        String uid = FirebaseAuth.getInstance().getUid();
        tripRef = FirebaseDatabase.getInstance().getReference().child("Users").child(uid)
                .child("Trips").child(mTitle);


        // get duration from database in case duration changes
        tripRef.child("duration").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mDuration = dataSnapshot.getValue().toString();

                for (int i = 0; i < Integer.parseInt(mDuration); i++) {

                    // create tabs for each day
                    dayButtons.addTab(dayButtons.newTab().setText("DAY " + (i + 1)));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        dayButtons.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                mMap.clear();

                if (tab.getPosition() == 0) {
                    // add markers for all plan items
                    // can use ClusterManager, but may interfere marker click listener
                    tripRef.child("Days").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            final LatLngBounds.Builder builder = new LatLngBounds.Builder();
                            // iterate days
                            int count = 0;
                            for (DataSnapshot dayData : dataSnapshot.getChildren()) {

                                // iterate items of each day
                                for (DataSnapshot itemData : dayData.getChildren()) {
                                    count++;
                                    addMarkerForPlan(itemData, builder);

                                }

                            }
                            if (count != 0) {
                                zoomCameraToIncludeAll(builder);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                } else {

                    tripRef.child("Days").child("DAY " + tab.getPosition()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            final LatLngBounds.Builder builder = new LatLngBounds.Builder();

                            if (dataSnapshot.getChildrenCount() != 0) {
                                for (DataSnapshot itemData : dataSnapshot.getChildren()) {
                                    addMarkerForPlan(itemData, builder);
                                }
                                zoomCameraToIncludeAll(builder);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        // initialize Places
        if (!Places.isInitialized()){
            Places.initialize(getApplicationContext(), APP_API_KEY);
        }
        mPlacesClient = Places.createClient(this);
    }

    private void zoomCameraToIncludeAll(final LatLngBounds.Builder builder) {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                LatLngBounds bounds = builder.build();
                int padding = 200; // offset from edges of the map in pixels
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                // zoom map to include all markers
                mMap.animateCamera(cu);
//                mMap.setMaxZoomPreference(ZOOM_LEVEL_MAX);

            }
        }, 1200);


    }

    private void addMarkerForPlan(DataSnapshot itemData, final LatLngBounds.Builder builder) {

        final String itemType = itemData.child("itemType").getValue().toString();
        if (!itemType.equals("note")) {
            // get placeId
            final String placeId = itemData.child("placeId").getValue().toString();
            final String placeName = itemData.child("placeName").getValue().toString();

            // get place
            List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG);
            FetchPlaceRequest placeRequest = FetchPlaceRequest.builder(placeId, fields).build();
            mPlacesClient.fetchPlace(placeRequest).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
                @Override
                public void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
                    Place place = fetchPlaceResponse.getPlace();

                    // get coordinates
                    LatLng coord = place.getLatLng();

                    Marker marker = null;

                    // add marker
                    switch (itemType) {

                        case "place":

                            marker = mMap.addMarker(new MarkerOptions().position(coord)
                                    .title(placeName).snippet(placeId)
                                    .icon(getBitmapDescriptor(R.drawable.ic_place_dark)));
                            break;

                        case "transit":

                            marker = mMap.addMarker(new MarkerOptions().position(coord)
                                    .title(placeName).snippet(placeId)
                                    .icon(getBitmapDescriptor(R.drawable.ic_transit_dark)));
                            break;

                        case "lodging":

                            marker = mMap.addMarker(new MarkerOptions().position(coord)
                                    .title(placeName).snippet(placeId)
                                    .icon(getBitmapDescriptor(R.drawable.ic_hotel_dark)));
                            break;
                    }
                    marker.setTag("fromPlan");
                    builder.include(marker.getPosition());
                }
            });
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        // get coordinates of destination
        String[] coord = mCoord.split(" ");
        Double lat = Double.parseDouble(coord[0]);
        Double lng = Double.parseDouble(coord[1]);
        final LatLng latLng = new LatLng(lat, lng);


        // enable My Location layer
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat.requestPermissions()
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCATION_PERMISSION);
            return;
        }
        mMap.setMyLocationEnabled(true);

        // set map type
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        // enable indoor level picker
        mMap.getUiSettings().setIndoorLevelPickerEnabled(true);

        // set info window adapter
        mMap.setInfoWindowAdapter(this);

        tripRef.child("Days").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                final LatLngBounds.Builder builder = new LatLngBounds.Builder();
                // iterate days
                int count = 0;
                for (DataSnapshot dayData : dataSnapshot.getChildren()) {

                    // iterate items of each day
                    for (DataSnapshot itemData : dayData.getChildren()) {
                        count++;
                        addMarkerForPlan(itemData, builder);

                    }
                }

                if (count != 0) {
                    zoomCameraToIncludeAll(builder);
                } else mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, ZOOM_LEVEL_CITY));

                //animateCamera(): Map moves to the given location (or list of locations) using an animation, including zooming out and then zooming in to the new location (if the two points are far from each other).
                //moveCamera(): Map changes focus to the given the given location (or list of locations) without animation, in a single frame change.

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        // set poi click listener
        mMap.setOnPoiClickListener(this);

        // on marker click listener
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(final Marker marker) {
                showAndRefreshInfo(marker);
                return false;
            }
        });

        // on map click
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                // remove focused marker if it is not a plan item
                if (focusedMarker != null && (focusedMarker.getTag() == null || !focusedMarker.getTag().equals("fromPlan"))) {
                    focusedMarker.remove();
                }
            }
        });

        // long click map to add a marker
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                // add a marker without title
                Marker marker = mMap.addMarker(new MarkerOptions().position(latLng));
                // hide info window
                marker.hideInfoWindow();
                // move camera
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            }
        });

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {

                ReadMoreAndEditFragment readMoreAndEditFragment = new ReadMoreAndEditFragment();
                Bundle args = new Bundle();
                args.putString("placeId", marker.getSnippet());
                args.putString("placeName", marker.getTitle());
                args.putString("title", mTitle);

                if (marker.getTag() == "fromPlan") {

                    args.putString("tag", "fromPlan");

                } else args.putString("tag", "fromPoi");

                readMoreAndEditFragment.setArguments(args);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(R.id.map_container, readMoreAndEditFragment, "ReadMore");
                ft.addToBackStack(null);
                ft.commit();

            }
        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_LOCATION_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // The grant results for the corresponding permissions which is either PERMISSION_GRANTED
                // or PERMISSION_DENIED. Never null.

            } else {
            }

        }
    }

    private BitmapDescriptor getBitmapDescriptor(int id) {
        Drawable vectorDrawable = this.getResources().getDrawable(id);
        int h = vectorDrawable.getIntrinsicHeight();
        int w = vectorDrawable.getIntrinsicWidth();
        vectorDrawable.setBounds(0, 0, w, h);
        Bitmap bm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bm);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bm);
    }

    private void showAndRefreshInfo(final Marker marker) {

        focusedMarker = marker;
        marker.showInfoWindow();

        // handle delay
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //refresh
                if (marker.isInfoWindowShown()) {

                    marker.hideInfoWindow();
                    marker.showInfoWindow();
                }
            }
        }, 1000);
    }

    @Override
    public void onPoiClick(PointOfInterest poi) {// contains latitude/longitude, place ID, and name of the POI.
        // OS sometimes confuse onPoiClick and onMarkerClick, so avoid clicking markers on a poi
        LatLng location = poi.latLng;
        String name = poi.name;
        String id = poi.placeId;
        // add marker (info window is always anchored to a marker)
        Marker marker = mMap.addMarker(new MarkerOptions().position(location).title(name).snippet(id));
        focusedMarker = marker;
        // move camera without zoom
        mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
        //set focused marker and show info
        showAndRefreshInfo(marker);

    }


    @Override
    public View getInfoWindow(Marker marker) {

        return null;
    }

    @Override
    public View getInfoContents(final Marker marker) {

        // marker is from long click
        if (marker.getTitle() == null) return null; // show default info window

        // Else, get placeId and title
        String placeId = marker.getSnippet();
        final String title = marker.getTitle();

        // get photo by placeId
        List<Place.Field> fields = Arrays.asList(Place.Field.PHOTO_METADATAS);
        FetchPlaceRequest placeRequest = FetchPlaceRequest.builder(placeId, fields).build();
        mPlacesClient.fetchPlace(placeRequest).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
            @Override
            public void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
                Place place = fetchPlaceResponse.getPlace();
                // get photo
                if (place.getPhotoMetadatas() != null) {
                    PhotoMetadata photo = place.getPhotoMetadatas().get(0);
                    String attrOri = photo.getAttributions();
                    attrOri = attrOri.substring(3, attrOri.length()-4);
                    final String author = attrOri.substring(attrOri.indexOf('>')+1);
                    FetchPhotoRequest photoRequest = FetchPhotoRequest.builder(photo).build();
                    mPlacesClient.fetchPhoto(photoRequest).addOnSuccessListener(new OnSuccessListener<FetchPhotoResponse>() {
                        @Override
                        public void onSuccess(FetchPhotoResponse fetchPhotoResponse) {
                            Bitmap bitmap = fetchPhotoResponse.getBitmap();
                            //inflate view
                            infoWindowView = LayoutInflater.from(getBaseContext()).inflate(R.layout.map_info_card, mapCard, false);
                            ImageView imageView = infoWindowView.findViewById(R.id.info_image);
                            TextView textView = infoWindowView.findViewById(R.id.info_title);
                            TextView attriView = infoWindowView.findViewById(R.id.attr);
                            // set title
                            textView.setText(title);
                            // set attribution
                            attriView.setText("by " + author);
                            //set imageView
                            imageView.setImageBitmap(bitmap);
                        }
                    });
                }
                else {
                    //inflate view
                    infoWindowView = LayoutInflater.from(getBaseContext()).inflate(R.layout.map_info_card, mapCard, false);
                    final ImageView imageView = infoWindowView.findViewById(R.id.info_image);
                    TextView textView = infoWindowView.findViewById(R.id.info_title);
                    // set title
                    textView.setText(title);
                    //set imageView
                    imageView.setImageResource(R.drawable.ic_no_photo);
                }

            }
        });
        return infoWindowView;
    }

}

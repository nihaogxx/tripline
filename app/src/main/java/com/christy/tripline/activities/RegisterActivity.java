package com.christy.tripline.activities;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.christy.tripline.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.SignInMethodQueryResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class RegisterActivity extends AppCompatActivity {

    // for checking validation
    private View focusView = null;
    private boolean cancel = false;


    // Firebase instance variables
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabaseReference;

    // UI
    private EditText mEmailView;
    private EditText mUsernameView;
    private EditText mPasswordView;
    private EditText mConfirmPasswordView;
    private Button mClearEmailButton;
    private Button mClearNameButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        mAuth = FirebaseAuth.getInstance();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();

        // find views
        mEmailView = findViewById(R.id.email_input_register);
        mUsernameView = findViewById(R.id.username_register);
        mPasswordView = findViewById(R.id.password_input_register);
        mConfirmPasswordView = findViewById(R.id.confirm_password_register);
        mClearEmailButton = findViewById(R.id.clear_email_register);
        mClearNameButton = findViewById(R.id.clear_name_register);

        // Keyboard sign up action
        mConfirmPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override // after user press return key
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.integer.register_form_finished || id == EditorInfo.IME_ACTION_DONE) {
                    attemptRegistration();
                    return true;
                }
                return false;
            }
        });

        // Add textChangedListener to auto hide and show clear buttons
        mUsernameView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (mUsernameView.getText().toString().length() > 0) {
                    mClearNameButton.setVisibility(View.VISIBLE);
                } else mClearNameButton.setVisibility(View.INVISIBLE);

            }
        });

        mEmailView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (mEmailView.getText().toString().length() > 0) {
                    mClearEmailButton.setVisibility(View.VISIBLE);
                } else mClearEmailButton.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void attemptRegistration() {
        // Reset errors displayed in the form.
        mEmailView.setError(null);
        mPasswordView.setError(null);
        mUsernameView.setError(null);
        // check each input one by one from bottom up cuz we want the focus on the first view which has an error
            // Check for a valid password
        String password = mPasswordView.getText().toString();

            // if (password.trim().length() = 0)
        if (TextUtils.isEmpty(password) || !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

            // Check for a valid email address.
        String email = mEmailView.getText().toString();

        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
            checkUsername();

        } else if (!email.contains("@")) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
            checkUsername();
        } else { // check if account already exists

            checkAccountExistence(email);

        }

    }


    private boolean isPasswordValid(String password) {
        String confirmedPassword = mConfirmPasswordView.getText().toString();

        boolean hasUppercase = !password.equals(password.toLowerCase());
        boolean hasLowercase = !password.equals(password.toUpperCase());

        if (!confirmedPassword.equals(password)) {
            return false;
        }
        if (password.trim().length() < 8) {
            return false;
        }

        // no digits
        // \d is a digit (a character in the range 0-9), and + means 1 or more times. So, \d+ is 1 or more digits.
        if (!password.matches("(.*)\\d+(.*)")) { //has special character: !password.matches("[A-Za-z0-9 ]*")
            return false;
        }
        // no lower case or no uppercase or no both
        return hasLowercase && hasUppercase;
    }

    public void checkAccountExistence(String email) {

        mAuth.fetchSignInMethodsForEmail(email)
                .addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
                    @Override
                    public void onComplete(@NonNull Task<SignInMethodQueryResult> task) {

                        if (task.isSuccessful()) {
                            SignInMethodQueryResult result = task.getResult();
                            //getSignInMethods() returns a list of signIn methods
                            // that can be used to sign in a given user
                            if (result != null && result.getSignInMethods() != null
                                    && result.getSignInMethods().size() > 0) {

                                mEmailView.setError(getString(R.string.error_existing_email));
                                focusView = mEmailView;
                                cancel = true;
                            }
                            // check for a valid username after email validation is completed
                            checkUsername();
                        }


                    }
                });

    }

    public void checkUsername() {

        final String disPlayName = mUsernameView.getText().toString();

        if (TextUtils.isEmpty(disPlayName)) {
            mUsernameView.setError("This field is required");
            focusView = mUsernameView;
            cancel = true;
        } else {
            FirebaseDatabase.getInstance().getReference().child("Users")
                    // called once and then immediately removed, at initial data or data change
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            // checking for existing username
                            for (DataSnapshot idSnapshot : dataSnapshot.getChildren()) {
                                String s = (String) idSnapshot.child("name").getValue();// idSnapshot.getValue(User.class).getName
                                if (s != null && s.equals(disPlayName)) {
                                    mUsernameView.setError("This name is taken");
                                    focusView = mUsernameView;
                                    cancel = true;
                                    break;
                                }
                            }
                            // check if there is an error
                            if (cancel) {
                                // There was an error; don't attempt login and focus on the first
                                // field with an error.
                                focusView.requestFocus();
                                cancel = false; // cancel is not local so must reset!!!
                            } else {
                                createFirebaseUser();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
        }
    }


    private void createFirebaseUser() {
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()) {
                    showErrorDialog("Registration attempt failed");
                } else {
                    FirebaseUser user = mAuth.getCurrentUser();
                    if (user != null) {
                        mDatabaseReference.child("Users").child(user.getUid()).child("name").setValue(mUsernameView.getText().toString())
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            sendEmailVerification();
                                            sendUserToHomeActivity();
                                        } else showErrorDialog("Registration attempt failed");
                                    }
                                });
                    }
                }
            }
        });


    }

    private void sendUserToHomeActivity() {

        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
//        finish(); wats the diff btw finish and intent flags?

    }


    public void signUp(View view) {
        attemptRegistration();
    }


    private void sendEmailVerification() {

        if (mAuth.getCurrentUser() != null) {

            mAuth.getCurrentUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful())
                        Toast.makeText(getBaseContext(), "Email Verfication Sent", Toast.LENGTH_LONG).show();
                    else {
                        if (task.getException() != null) {
                            task.getException().printStackTrace();
                        }
                    }

                }
            });
        }
    }

    private void showErrorDialog(String msg) {
        new AlertDialog.Builder(this)
                .setTitle("Error")
                .setMessage(msg)
                .setPositiveButton(android.R.string.ok, null)
                .setIcon(android.R.drawable.ic_dialog_alert);
    }


    public void clearText(View view) {
        switch (view.getId()) {
            case R.id.clear_email_register:
                mEmailView.getText().clear();
                break;

            case R.id.clear_name_register:
                mUsernameView.getText().clear();
        }
    }
}

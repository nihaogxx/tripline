package com.christy.tripline.activities;

import android.content.Intent;
import androidx.annotation.NonNull;
import com.christy.tripline.R;
import com.christy.tripline.model.TripModel;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.christy.tripline.adapters.TripPagerAdapter;
import com.christy.tripline.fragments.EditTripFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;


public class CurrentTripActivity extends AppCompatActivity implements EditTripFragment.EditTripListener {

    private TextView titleView;
    private TripPagerAdapter mTripPagerAdapter;

    private String mTitle, mTripDuration, mDestination, mStartDate, mCountryCode, mCoord, mPrivacy, mPublicId, uID;
    private Bundle args;
    static String CUSTOM_SEARCH_API_KEY;
    static String SEARCH_ENGINE_ID;

    private DatabaseReference mTripsDatabaseRef;
    private FragmentManager fm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_trip);

        SEARCH_ENGINE_ID = getString(R.string.search_engine_id);
        CUSTOM_SEARCH_API_KEY = getString(R.string.app_api_key);

        // get title of the trip
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            mTitle = intent.getExtras().getString("title");

        }

        // get FragmentManager
        fm = getSupportFragmentManager();

        // References
        FirebaseAuth auth = FirebaseAuth.getInstance();
        uID = auth.getCurrentUser().getUid();
        mTripsDatabaseRef = FirebaseDatabase.getInstance().getReference().child("Users").child(uID)
                .child("Trips");

        mTripsDatabaseRef.child(mTitle).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                TripModel tripModel = dataSnapshot.getValue(TripModel.class);
                mTripDuration = tripModel.getDuration();
                mDestination = tripModel.getDestination();
                mStartDate = tripModel.getStartDate();
                mCountryCode = tripModel.getCountryCode();
                mCoord = tripModel.getCoordinates();
                mPrivacy = tripModel.getPrivacy();
                // put public id if any
                if (dataSnapshot.hasChild("publicId")) {
                    mPublicId = dataSnapshot.child("publicId").getValue().toString();
                }

                // create bundle with info
                args = new Bundle();
                args.putString("duration", mTripDuration);
                args.putString("destination", mDestination);
                args.putString("startDate", mStartDate);
                args.putString("title", mTitle);
                args.putString("countryCode", mCountryCode);
                args.putString("coord", mCoord);


                // connect ViewPager to PagerAdapter
                ViewPager viewPager = findViewById(R.id.view_pager);
                // pass bundle to adapter so adapter can pass it to fragments
                mTripPagerAdapter = new TripPagerAdapter(fm, args);
                viewPager.setAdapter(mTripPagerAdapter);

                // connect TabLayout to ViewPager
                TabLayout tabLayout = findViewById(R.id.tabLayout);
                tabLayout.setupWithViewPager(viewPager, true);
                // set tab text color in xml or here

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        // set action bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        // set map button
        getSupportActionBar().setDisplayShowHomeEnabled(true);// show home button
        // to set the icon at the start of the toolbar
        toolbar.setNavigationIcon(R.drawable.ic_mode_map);
        // map mode click listener
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(CurrentTripActivity.this, MapsActivity.class);
                intent.putExtra("title", mTitle);
                intent.putExtra("coord", mCoord);
                startActivity(intent);
            }
        });

        // set title of the action bar
        titleView = findViewById(R.id.app_bar_title);
        titleView.setText(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.current_trip_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_edit) {
            showEditTripFragment();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void showEditTripFragment() {

        EditTripFragment editTripFragment = new EditTripFragment();
        Bundle bundle = new Bundle();// cannot make bundle global and use it everywhere, cuz if user edit the trip info, bundle will not get updated
        bundle.putString("title", mTitle);
        bundle.putString("destination", mDestination);
        bundle.putString("date", mStartDate);
        bundle.putString("countryCode", mCountryCode);
        bundle.putString("coord", mCoord);
        bundle.putString("privacy", mPrivacy);

        editTripFragment.setArguments(bundle);
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.current_trip_container, editTripFragment, "TripEdit");// not showing if parent is a linear layout
        ft.addToBackStack(null); // press back button to get back to activity
        ft.commit();

    }


    @Override
    public void onFinishEditTrip(final String title, String destination, String date, String countryCode,
                                 String coord, String privacy) {

        // update value in database
        // time stamp

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.ENGLISH);
        String timeStamp = dateFormat.format(new Date());

        mTripsDatabaseRef.child(mTitle).child("timeStamp").setValue(timeStamp);

        // des, coordinates & country code
        if (!mDestination.equals(destination)) {
            mDestination = destination;
            mCoord = coord;
            mCountryCode = countryCode;
            mTripsDatabaseRef.child(mTitle).child("destination").setValue(mDestination);
            mTripsDatabaseRef.child(mTitle).child("coordinates").setValue(mCoord);
            mTripsDatabaseRef.child(mTitle).child("countryCode").setValue(mCountryCode);

            // search for a background image and update database
            imageSearch(destination);

            // change data for adapter
            args.putString("destination", mDestination);
            args.putString("countryCode", mCountryCode);
            args.putString("coord", mCoord);
        }

        // start date
        if (!mStartDate.equals(date)) {
            mStartDate = date;
            // change data for adapter
            args.putString("startDate", mStartDate);
        }

        // privacy
        if (!privacy.equals(mPrivacy)) { // privacy is changed

            if (privacy.equals("public")) {
                // save to public trips
                DatabaseReference publicRef = FirebaseDatabase.getInstance().getReference().child("Public Trips").push();
                publicRef.child("uId").setValue(uID);
                publicRef.child("title").setValue(mTitle);
                publicRef.child("destination").setValue(mDestination);

                // save public id under trip
                mPublicId = publicRef.getKey();
                mTripsDatabaseRef.child(mTitle).child("publicId").setValue(mPublicId);

            } else if (privacy.equals("private")) { // publicId != null
                // remove from public trips
                FirebaseDatabase.getInstance().getReference().child("Public Trips").child(mPublicId).removeValue();

                // remove public id under trip
                mTripsDatabaseRef.child(mTitle).child("publicId").removeValue();
            }

            // set privacy in database
            mPrivacy = privacy;
            mTripsDatabaseRef.child(mTitle).child("privacy").setValue(mPrivacy);
        }

        if (!title.equals(mTitle)) { // title is changed

            // change title under public trips if it's public
            if (mPrivacy.equals("public")) {
                FirebaseDatabase.getInstance().getReference().child("Public Trips")
                        .child(mPublicId).child("title").setValue(title);
            }
            mTripsDatabaseRef.child(mTitle).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    // create a new node with new key and old value
                    mTripsDatabaseRef.child(title).setValue(dataSnapshot.getValue());

                    // change data for adapter
                    args.putString("title", title);
                    mTripPagerAdapter.notifyDataSetChanged();

                    // remove old node after notify adapter, or else adapter cant find removed node(null pointer)
                    mTripsDatabaseRef.child(mTitle).removeValue();

                    // update action bar
                    mTitle = title;
                    titleView.setText(mTitle);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        } else mTripPagerAdapter.notifyDataSetChanged();
        // how to refresh Explore only when destination is changed???

    }

    private void imageSearch(String keyword) {

        String urlString = "https://www.googleapis.com/customsearch/v1?q=" + keyword +
                "&key=" + CUSTOM_SEARCH_API_KEY + "&cx=" + SEARCH_ENGINE_ID + "&num=1" + "&searchType=image"
                + "&imgSize=medium" + "&imgType=photo" + "&alt=json";

        AsyncHttpClient client = new AsyncHttpClient();

        client.get(urlString, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // called when response HTTP status is "200 OK"
                String imgUrl;
                try {
                    imgUrl = response.getJSONArray("items").getJSONObject(0).
                            getString("link");

                } catch (JSONException e) {
                    imgUrl = "null";
                    e.printStackTrace();
                }

                // save bg in database
                mTripsDatabaseRef.child(mTitle).child("background").setValue(imgUrl);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
            }
        });
    }


}

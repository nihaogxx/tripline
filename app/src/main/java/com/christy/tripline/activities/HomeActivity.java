package com.christy.tripline.activities;

import android.content.Intent;
import android.os.Handler;
import androidx.annotation.NonNull;

import com.christy.tripline.R;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.navigation.NavigationView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.christy.tripline.Tasks.GetCountryCodeTask;
import com.christy.tripline.adapters.HomeRecyclerAdapter;
import com.christy.tripline.fragments.SearchResultFragment;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class HomeActivity extends AppCompatActivity {


    // Constants
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 9;

    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private HomeRecyclerAdapter mAdapter;
    private LinkedList<String> titleList = new LinkedList<>();
    private LinkedList<String> uIdList = new LinkedList<>();
    private LinkedList<String> desList = new LinkedList<>();
    private CircleImageView mNavCircleImageView;
    private TextView mUserNameTextView;

    private String uID;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabaseReference; // it represents a particular location in the cloud database
    private ValueEventListener mProfileEventListener;
    private ValueEventListener mSingleValueEventListener;
    private static String APP_API_KEY;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        APP_API_KEY = getString(R.string.app_api_key);

        // get the Firebase references
        mAuth = FirebaseAuth.getInstance();
        uID = mAuth.getCurrentUser().getUid();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();

        //must set up action bar before set up ActionBarDrawerToggle, otherwise it shows a back arrow instead of hamburger.
        Toolbar toolbar = findViewById(R.id.toolbar);
        // set the toolbar as this activity's action bar
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);// we wont show default title "Tripline"
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);// set "up" button
        TextView appBarTitle = findViewById(R.id.app_bar_title);
        appBarTitle.setText("Home");

        // set ActionBarDrawerToggle: tie together the functionality of DrawerLayout and the
        // framework ActionBar to implement the recommended design for navigation drawers.
        // can be used as DrawerLayout.DrawerListener
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(HomeActivity.this, drawerLayout,
                R.string.drawer_open, R.string.drawer_close); // toggle indicator(up button) will become a hamburger
        drawerLayout.addDrawerListener(mActionBarDrawerToggle);
        //Synchronize the state of the drawer indicator with the linked DrawerLayout
        mActionBarDrawerToggle.syncState();// or put this line in onPostCreate()

        // set up header
        final NavigationView navigationView = findViewById(R.id.nav_view);
            //navigationView.setItemIconTintList(null);// to solve the problem by removing tint effect: trip icon not showing
        View headerView = navigationView.inflateHeaderView(R.layout.navi_header_home);// set up header programmatically
        LinearLayout profileImgContainer = headerView.findViewById(R.id.profile_img_container);
            // set (the parent view Of CircleImageView).setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            // This disable hardware acceleration, to solve the issue:
            // if cannot wrap circleImageView with LinearLayout or FrameLayout in order to show the circle image
        profileImgContainer.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        mNavCircleImageView = headerView.findViewById(R.id.nav_profile_image); // must inflate root layout first
        mUserNameTextView = headerView.findViewById(R.id.nav_user_name);


        // set navigation item selected listener
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                UserMenuSelector(item);

                return false;
            }
        });

        // listen constantly, NOT addChildListener cuz it could be a profile update
        mProfileEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) { // user exists

                    if (dataSnapshot.child("profileImage").getValue() != null) {

                        String image = dataSnapshot.child("profileImage").getValue().toString();

                        Picasso.with(HomeActivity.this).load(image).placeholder(R.drawable.no_image_icon).into(mNavCircleImageView);
                    }

                    if (dataSnapshot.child("name").getValue() != null) { //dataSnapshot.hasChild("name")
                        String name = dataSnapshot.child("name").getValue().toString();
                        mUserNameTextView.setText(name);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        // set up layout manager and instantiate adapter
        RecyclerView desRecyclerView = findViewById(R.id.list);
        desRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new HomeRecyclerAdapter(this, titleList, uIdList, desList);
        desRecyclerView.setAdapter(mAdapter);

        mSingleValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot itemData : dataSnapshot.getChildren()) {

                    String uid = itemData.child("uId").getValue().toString();
                    String title = itemData.child("title").getValue().toString();
                    String des = itemData.child("destination").getValue().toString();
                    // ordered from new to old.
                    // we do a lot of adding at the front of the list, so we use linkedList over arrayList since linkedList takes O(1) time to remove/add first/end
                    titleList.addFirst(title);
                    uIdList.addFirst(uid);
                    desList.addFirst(des);
                }

                mAdapter.notifyItemRangeInserted(0, titleList.size());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        //Initialize Places
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), APP_API_KEY);
        }
    }


    @Override
    protected void onRestart() { // stopped -> onRestart -> started
        // users delete/add a trip and/or make it public/private all the time so the list is ever-changing
        // clear the lists when get back to this activity from another activity and repopulate them in onStart()
        super.onRestart();
        int index = uIdList.size();
        uIdList.clear();
        titleList.clear();
        desList.clear();
        mAdapter.notifyItemRangeRemoved(0, index);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // set profile listener to update header
        mDatabaseReference.child("Users").child(uID).addValueEventListener(mProfileEventListener);

        // set event listener to update recycler view
        mDatabaseReference.child("Public Trips").addListenerForSingleValueEvent(mSingleValueEventListener);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // inflate the xml file into the Menu via MenuInflater
        getMenuInflater().inflate(R.menu.trips_menu, menu);
        return true;
    }

    // the activity's onOptionsItemSelected()
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //click event will be consumed by the onOptionsItemSelect() call on mActionBarDrawerToggle
        // and won't fall through to other item click functions.

        if (mActionBarDrawerToggle.onOptionsItemSelected(item)) {// the drawer toggle is clicked

            return true;
        }

        if (item.getItemId() == R.id.action_search) {// search button is clicked

            launchAutoComplete();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void launchAutoComplete() {
        List<Place.Field> fields = Arrays.asList(Place.Field.NAME, Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                .setTypeFilter(TypeFilter.CITIES)
                .build(HomeActivity.this);
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE && resultCode == RESULT_OK) {

            Place place = Autocomplete.getPlaceFromIntent(data);
            String placeName = place.getName().toString();
            LatLng coordinates = place.getLatLng();

            // get country code and name in background
            new GetCountryCodeTask(HomeActivity.this, placeName).execute(coordinates);

            //runs outside of the activity
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    // this is a static context
                    String destination = GetCountryCodeTask.getDestination();

                    // launch result fragment
                    Bundle bundle = new Bundle();
                    bundle.putString("des", destination);
                    SearchResultFragment searchResultFragment = new SearchResultFragment();
                    searchResultFragment.setArguments(bundle);
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.add(R.id.app_bar_container, searchResultFragment, "HomeSearchResult");
                    //pop the previous result frag if any
                    getSupportFragmentManager().popBackStack();
                    // wont need to pop more than one back stack, so pass null, otherwise pass frag name
                    ft.addToBackStack(null); // save the cur transaction so that user can later reverse this transaction
                    ft.commit();


                }
            }, 2000);

        }

    }

    private void UserMenuSelector(MenuItem item) {

        switch (item.getItemId()) {// item.getTitle()
            case R.id.nav_trips:
                Intent tripsIntent = new Intent(HomeActivity.this, MyTripsActivity.class);
                startActivity(tripsIntent);
                break;

            case R.id.nav_friends:
                Intent friendsIntent = new Intent(HomeActivity.this, FriendsActivity.class);
                startActivity(friendsIntent);
                break;

            case R.id.nav_profile:
                Intent profileIntent = new Intent(getBaseContext(), ProfileActivity.class);
                startActivity(profileIntent);
                break;

            case R.id.nav_sign_out:
                signOut();
                break;
        }

    }


    @Override
    protected void onStop() {
        super.onStop();
        // remove listeners
        mDatabaseReference.child("Public Trips").removeEventListener(mSingleValueEventListener);
        mDatabaseReference.child("Users").child(uID).removeEventListener(mProfileEventListener);

    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        mDatabaseReference.child("Users").child(uID).removeEventListener(mProfileEventListener);
//    }

    private void signOut() {
        // sign out of firebase
        FirebaseAuth.getInstance().signOut();

        // sign out of google, need if statement here?
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(getBaseContext(), gso);
        mGoogleSignInClient.signOut().addOnCompleteListener(HomeActivity.this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(getBaseContext(), "Logged out", Toast.LENGTH_SHORT).show();
                sendUserToLoginActivity();
            }
        });

    }


    private void sendUserToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        //finish();
    }


}

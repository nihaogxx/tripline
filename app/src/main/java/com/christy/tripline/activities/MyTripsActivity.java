package com.christy.tripline.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.christy.tripline.R;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.christy.tripline.Tasks.GetCountryCodeTask;
import com.christy.tripline.adapters.TripsRecyclerAdapter;
import com.christy.tripline.model.TripModel;
import com.christy.tripline.fragments.CreateTripFragment;
import com.christy.tripline.fragments.SearchResultFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import cz.msebera.android.httpclient.Header;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class MyTripsActivity extends AppCompatActivity implements CreateTripFragment.CreateTripListener {

    private RecyclerView mRecyclerView;
    private FloatingActionButton addATripFab;
    private TripsRecyclerAdapter mAdapter;
    private ChildEventListener mChildEventListener;
    private LinkedList<String> titleList = new LinkedList<>();
    private LinkedList<String> mBackgroundList = new LinkedList<>();

    private FragmentManager fm;
    private DatabaseReference mUsersDatabaseRef;

    static String APP_API_KEY;
    static String SEARCH_ENGINE_ID;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 22;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_trips);

        APP_API_KEY = getString(R.string.app_api_key);
        SEARCH_ENGINE_ID = getString(R.string.search_engine_id);

        // References
        FirebaseAuth auth = FirebaseAuth.getInstance();
        final String uID = auth.getCurrentUser().getUid();
        mUsersDatabaseRef = FirebaseDatabase.getInstance().getReference().child("Users").child(uID);

        // UI
        addATripFab = findViewById(R.id.add_trip_fab);
        Toolbar toolbar = findViewById(R.id.toolbar);
        mRecyclerView = findViewById(R.id.list);

        // set up layout manager and adapter
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new TripsRecyclerAdapter(this, titleList, mBackgroundList);
        mRecyclerView.setAdapter(mAdapter);

        // set up action bar
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView appBarTitle = findViewById(R.id.app_bar_title);
        appBarTitle.setText(R.string.my_trips);

        // get FragmentManager
        fm = getSupportFragmentManager();

        // listen for add, remove, and edit trips
        mChildEventListener = new ChildEventListener() {
            @Override
            // will pass every trip as dataSnapshot at the initial time, and the single trip which is added
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {// the new child's data & the key of the previous child

                String title = dataSnapshot.getKey();
                titleList.addFirst(title);

                TripModel newTrip = dataSnapshot.getValue(TripModel.class);
                if (newTrip != null) {

                    String background = newTrip.getBackground();
                    mBackgroundList.addFirst(background);
                }
                mAdapter.notifyItemInserted(0); // !!!
                mRecyclerView.scrollToPosition(0);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                // triggered by setValue
                // called twice, why???

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                String title = dataSnapshot.getKey();
                int index = titleList.indexOf(title);
                titleList.remove(index); // or title, cuz title cannot be duplicated
                mBackgroundList.remove(index); // must be index cuz background could be duplicated
                mAdapter.notifyItemRemoved(index);
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };


        // swipe left to delete
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {

                return false;
            }

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {

                if (direction == ItemTouchHelper.LEFT) { // if swipe left
                    AlertDialog.Builder ab = new AlertDialog.Builder(MyTripsActivity.this);// if getBaseContext, theme not right
                    ab.setTitle("Deleting Trip")
                            .setMessage("Are you sure to delete this trip?")
                            .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                @Override // delete
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    TextView titleView = viewHolder.itemView.findViewById(R.id.trip_title);
                                    final String title = (String) titleView.getText();
                                    // delete under my trips
                                    mUsersDatabaseRef.child("Trips").child(title).removeValue();
                                    // delete under Public Trips.
                                    FirebaseDatabase.getInstance().getReference().child("Public Trips")
                                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    for (DataSnapshot tripData : dataSnapshot.getChildren()) {

                                                        if (tripData.child("title").getValue().equals(title)
                                                            && tripData.child("uId").getValue().equals(uID)) {

                                                            String pushId = tripData.getKey();
                                                            FirebaseDatabase.getInstance().getReference().
                                                                    child("Public Trips").child(pushId).removeValue();
                                                        }
                                                    }
                                                }
                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                                }
                                            });
                                }
                            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override // not delete
                        public void onClick(DialogInterface dialog, int which) {
                            mAdapter.notifyItemChanged(viewHolder.getAdapterPosition()); // revert swipe&restore view holder
                            dialog.dismiss();
                        }
                    }).show();
                }

            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        //Attach to RecyclerView
        itemTouchHelper.attachToRecyclerView(mRecyclerView);

        // initialize Places
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), APP_API_KEY);
        }

    }

    @Override
    protected void onRestart() { // OR instantiate and populate lists in onStart
        super.onRestart();
        // navigate back to this activity
        // reinitialize two linked lists or else trips will be added on top of the old lists

        int count = titleList.size();
        titleList.clear();
        mBackgroundList.clear();
        mAdapter.notifyItemRangeRemoved(0, count);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // sort trips in ascending order of time
        mUsersDatabaseRef.child("Trips").orderByChild("timeStamp").addChildEventListener(mChildEventListener);

        // set onClickListener
        addATripFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // hide fab when fragment is attached
                showCreateTripFragment();

            }
        });

    }


    @Override
    protected void onStop() {
        super.onStop();
        addATripFab.setOnClickListener(null);
        mUsersDatabaseRef.child("Trips").removeEventListener(mChildEventListener);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.trips_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            launchAutoComplete();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void launchAutoComplete() {
        List<Place.Field> fields = Arrays.asList(Place.Field.NAME, Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                .setTypeFilter(TypeFilter.CITIES)
                .build(MyTripsActivity.this);
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE && resultCode == RESULT_OK) {

            Place place = Autocomplete.getPlaceFromIntent(data);
            String placeName = place.getName();
            LatLng coordinates = place.getLatLng();

            // get country code and name in background
            new GetCountryCodeTask(MyTripsActivity.this, placeName).execute(coordinates);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    String destination = GetCountryCodeTask.getDestination();
                    // launch result fragment
                    Bundle bundle = new Bundle();
                    bundle.putString("des", destination);
                    SearchResultFragment searchResultFragment = new SearchResultFragment();
                    searchResultFragment.setArguments(bundle);
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.my_trips_container, searchResultFragment, "MySearchResult");
               //     fm.popBackStack();// pop any previous search fragment, used with ft.add()    same as ft.replace()
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }, 2000);

        } else { // from create trip fragment
            super.onActivityResult(requestCode,resultCode,data); // this will call onActivityResult() in CreateTripFragment
        }
    }

    private void showCreateTripFragment() {

        CreateTripFragment createTripFragment = new CreateTripFragment();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.my_trips_container, createTripFragment, "TripCreate");
        ft.addToBackStack(null); // press back button to get back to activity
        ft.commit();
    }


    @Override
    public void onFinishCreateTrip(String inputTitle, String inputDestination, String days, String code, String coordinate) {

        // get data
        String title = inputTitle;
        String destination = inputDestination;
        String totalDays = days;
        String countryCode = code;
        String coordinates = coordinate;

        // get time stamp
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.ENGLISH);
        //String timeStamp = sdf.format(new Date()); // cannot use Calendar.getInstance cuz SimpleDateFormat only handles Date value
        String timeStamp = sdf.format(Calendar.getInstance().getTime()); // getTime returns a Date value

        // search an image for destination
        imageSearch(destination, timeStamp, title, totalDays, countryCode, coordinates);

    }

    public void imageSearch(final String keyword, final String timeStamp, final String title,
                            final String totalDays, final String countryCode, final String coordinates) {
        String urlString = "https://www.googleapis.com/customsearch/v1?q=" + keyword +
                "&key=" + APP_API_KEY + "&cx=" + SEARCH_ENGINE_ID + "&num=1" + "&searchType=image"
                + "&imgSize=medium" + "&imgType=photo" + "&alt=json";
        // e.g https://www.googleapis.com/customsearch/v1?q=Miami,United%20States&key=AIzaSyBPI1mAld6Y_jwl-N2ukuOojNVrSYTslfQ&cx=012522926545414746588:-fk8djazcpc&num=1&searchType=image&imgSize=medium&imgType=photo&alt=json
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(urlString, new JsonHttpResponseHandler() { // can pass RequestParams as second parameter
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // called when response HTTP status is "200 OK"
                String imgUrl;
                try {
                    imgUrl = response.getJSONArray("items").getJSONObject(0).
                            getString("link");

                } catch (JSONException e) {
                    imgUrl = "null";
                    e.printStackTrace();
                }

                // set data as TripModel
                TripModel myTrip = new TripModel(keyword, imgUrl, timeStamp, totalDays,
                        "mm/dd/yyyy", countryCode, coordinates, "private");
                mUsersDatabaseRef.child("Trips").child(title).setValue(myTrip);//onChildAdded triggered, then onChildChanged

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
            }
        });

    }


}

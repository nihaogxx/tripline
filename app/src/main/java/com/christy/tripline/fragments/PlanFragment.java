package com.christy.tripline.fragments;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.christy.tripline.adapters.OnPhotoPickedListener;
import com.christy.tripline.adapters.PlanRecyclerAdapter;
import com.christy.tripline.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class PlanFragment extends Fragment {

    private RecyclerView planRecyclerView;
    private FloatingActionButton addDayFab;
    private PlanRecyclerAdapter mPlanRecyclerAdapter;
    private DatabaseReference tripsRef;
    private ValueEventListener durationValueEventListener;

    private String duration, title, startDate, countryCode;
    private OnPhotoPickedListener toPlanAdapterListener;


    public PlanFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_plan, container, false);
        addDayFab = view.findViewById(R.id.add_a_day_fab);


        // get duration
        Bundle args = getArguments();
        if (args != null) {

            duration = args.getString("duration");
            countryCode = args.getString("countryCode");
            startDate = args.getString("startDate"); // used when search for places
            title = args.getString("title");
            mPlanRecyclerAdapter = new PlanRecyclerAdapter(getContext(), duration, startDate, title, countryCode);
        }

        //set up adapter
        planRecyclerView = view.findViewById(R.id.list);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        planRecyclerView.setLayoutManager(linearLayoutManager);
        planRecyclerView.setAdapter(mPlanRecyclerAdapter);

        // for uploading photos
        toPlanAdapterListener = mPlanRecyclerAdapter;


        tripsRef = FirebaseDatabase.getInstance().getReference().child("Users").
                child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child("Trips");

        durationValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String newDuration = dataSnapshot.getValue().toString();

                mPlanRecyclerAdapter = new PlanRecyclerAdapter(getActivity(), newDuration, startDate, title, countryCode);
                planRecyclerView.setAdapter(null);
                // set a new adapter
                planRecyclerView.setAdapter(mPlanRecyclerAdapter);
                duration = newDuration;
//                    scrollTo wont scroll all the way down to the bottom???
//                    planRecyclerView.smoothScrollToPosition(mPlanRecyclerAdapter.getItemCount()-1);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        tripsRef.child(title).child("duration").addValueEventListener(durationValueEventListener);

        addDayFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                duration = "" + (Integer.parseInt(duration) +1);
                tripsRef.child(title).child("duration").setValue(duration);//setValue triggered durationValueEventListener which triggered adapter refresh onDataChange
            }
        });

    }
    @Override
    public void onResume() {
        super.onResume();
        //tripsRef.child(title).child("Days").addChildEventListener(mChildEventListener);
    }


    @Override
    public void onPause() {
        super.onPause();
        //tripsRef.child(title).child("Days").removeEventListener(mChildEventListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        addDayFab.setOnClickListener(null);
        tripsRef.child(title).child("duration").removeEventListener(durationValueEventListener);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        toPlanAdapterListener.onIntent(data, resultCode, requestCode);//  pass pick photo result to PlanRecyclerAdapter

    }

}

package com.christy.tripline.fragments;


import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import com.christy.tripline.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileDisplayFragment extends Fragment {

    private CircleImageView profileView;
    private TextView nameView;
    private TextView locationView;
    private TextView tripsView;
    private TextView introView;
    private ScrollView introScroller;
    private Button upperButton;
    private Button lowerButton;
    private DatabaseReference receiverRef, friendRequestRef, friendsRef;
    private String senderId, receiverId, CURRENT_STATE, timeStamp;


    public UserProfileDisplayFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_profie_display, container, false);
        // initialize fields
        profileView = view.findViewById(R.id.display_image);
        nameView = view.findViewById(R.id.display_name);
        locationView = view.findViewById(R.id.display_location);
        tripsView = view.findViewById(R.id.display_trips);
        introView = view.findViewById(R.id.display_intro);
        introScroller = view.findViewById(R.id.intro_scroller);
        upperButton = view.findViewById(R.id.upper_button);
        lowerButton = view.findViewById(R.id.lower_button);
        CURRENT_STATE = "not_friends";
        upperButton.requestFocus();

        // hide keyboard (different from the way used in edit trip, which not working here)
        View currentFocus = getActivity().getCurrentFocus();
        if (currentFocus != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(currentFocus.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }

        // get receiver ID and sender ID
        receiverId = getArguments().getString("id");
        senderId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        // get references
        receiverRef = FirebaseDatabase.getInstance().getReference().child("Users").child(receiverId);
        friendRequestRef = FirebaseDatabase.getInstance().getReference().child("FriendRequests");
        friendsRef = FirebaseDatabase.getInstance().getReference().child("Friends");

        // populate fields
        receiverRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                // name
                String name = dataSnapshot.child("name").getValue().toString();
                nameView.setText("Name: " + name);

                // profile
                if (dataSnapshot.hasChild("profileImage")) {
                    String profileUrl = dataSnapshot.child("profileImage").getValue().toString();
                    Picasso.with(getActivity()).load(profileUrl).placeholder(R.drawable.no_image_icon).into(profileView);
                }


                // location
                if (dataSnapshot.hasChild("location")) {
                    String location = dataSnapshot.child("location").getValue().toString();
                    locationView.setText("Location: " + location);
                }

                //trips
                Long count = dataSnapshot.child("Trips").getChildrenCount();
                tripsView.setText("Trips: " + count.toString());

                // intro
                if (dataSnapshot.hasChild("intro")) {

                    introView.setText(dataSnapshot.child("intro").getValue().toString());

                } else introScroller.setVisibility(View.GONE);

                // set button text
                maintenanceOfButtons();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        if (!senderId.equals(receiverId)) {// not myself

            upperButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    upperButton.setEnabled(false);

                    switch (CURRENT_STATE) {
                        case "not_friends":  // upper button text: sent request
                            sendFriendRequest();
                            break;
                        case "request_sent":  //upper button text: cancel request
                            cancelFriendRequest();
                            break;
                        case "request_received":
                            acceptFriendRequest();
                            break;
                        case "friends":
                            unfriendAPerson();
                            break;
                    }
                }
            });


        } else {

            upperButton.setVisibility(View.INVISIBLE);
            lowerButton.setVisibility(View.INVISIBLE);
        }

        return view;
    }


    private void unfriendAPerson() {

        friendsRef.child(senderId).child(receiverId).removeValue()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if (task.isSuccessful()) {
                            friendsRef.child(receiverId).child(senderId).removeValue().
                                    addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                            if (task.isSuccessful()) {

                                                upperButton.setEnabled(true);
                                                CURRENT_STATE = "not_friends";
                                                upperButton.setText("Send Friend Request");

                                                lowerButton.setVisibility(View.INVISIBLE);
                                                lowerButton.setEnabled(false);
                                            }
                                        }
                                    });
                        }
                    }
                });


    }

    private void acceptFriendRequest() {

        // get time stamp
        Calendar calendarDate = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
        String currentDate = dateFormat.format(calendarDate.getTime());
        Calendar calendarTime = Calendar.getInstance();
        final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        String currentTime = timeFormat.format(calendarTime.getTime());
        timeStamp = currentDate + "-" + currentTime;

        // save in Friends under both id
        friendsRef.child(senderId).child(receiverId).child("date").setValue(timeStamp).
                addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        friendsRef.child(receiverId).child(senderId).child("date").setValue(timeStamp)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        //remove from requestList
                                        friendRequestRef.child(senderId).child(receiverId).removeValue()
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {

                                                        if (task.isSuccessful()) {
                                                            friendRequestRef.child(receiverId).child(senderId).removeValue().
                                                                    addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                        @Override
                                                                        public void onComplete(@NonNull Task<Void> task) {

                                                                            if (task.isSuccessful()) {

                                                                                upperButton.setEnabled(true);
                                                                                CURRENT_STATE = "friends";
                                                                                upperButton.setText("Unfriend This Person");

                                                                                lowerButton.setVisibility(View.INVISIBLE);
                                                                                lowerButton.setEnabled(false);
                                                                            }
                                                                        }
                                                                    });
                                                        }
                                                    }
                                                });
                                    }
                                });
                    }
                });

    }

    private void cancelFriendRequest() {

        friendRequestRef.child(senderId).child(receiverId).removeValue()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if (task.isSuccessful()) {
                            friendRequestRef.child(receiverId).child(senderId).removeValue().
                                    addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                            if (task.isSuccessful()) {

                                                upperButton.setEnabled(true);
                                                CURRENT_STATE = "not_friends";
                                                upperButton.setText("Send Friend Request");

                                                lowerButton.setVisibility(View.INVISIBLE);
                                                lowerButton.setEnabled(false);
                                            }
                                        }
                                    });
                        }
                    }
                });


    }

    private void sendFriendRequest() {

        friendRequestRef.child(senderId).child(receiverId).child("requestType").setValue("sent")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if (task.isSuccessful()) {
                            friendRequestRef.child(receiverId).child(senderId).child("requestType")
                                    .setValue("received").addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    if (task.isSuccessful()) {

                                        upperButton.setEnabled(true);
                                        CURRENT_STATE = "request_sent";
                                        upperButton.setText("Cancel Friend Request");


                                        lowerButton.setVisibility(View.INVISIBLE);
                                        lowerButton.setEnabled(false);
                                    }
                                }
                            });
                        }
                    }
                });

    }

    private void maintenanceOfButtons() {

        friendRequestRef.child(senderId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.hasChild(receiverId)) {// receiver exists in friendRequests

                    String requestType = dataSnapshot.child(receiverId).child("requestType").getValue().toString();

                    if (requestType.equals("sent")) {

                        CURRENT_STATE = "request_sent";
                        upperButton.setText("Cancel Friend Request");

                        lowerButton.setVisibility(View.INVISIBLE);
                        lowerButton.setEnabled(false);
                    } else if (requestType.equals("received")) {
                        CURRENT_STATE = "request_received";
                        upperButton.setText("Accept Friend Request");

                        lowerButton.setVisibility(View.VISIBLE);
                        lowerButton.setEnabled(true);
                        lowerButton.setText("Decline Friend Request");

                        lowerButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                cancelFriendRequest();
                            }
                        });

                    }
                } else { // receiver not in friendRequests, check in Friends

                    friendsRef.child(senderId).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            if (dataSnapshot.hasChild(receiverId)) {

                                CURRENT_STATE = "friends";
                                upperButton.setText("Unfriend This Person");
                                lowerButton.setVisibility(View.INVISIBLE);
                                lowerButton.setEnabled(false);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }


}















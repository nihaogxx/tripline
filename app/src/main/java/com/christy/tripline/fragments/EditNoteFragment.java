package com.christy.tripline.fragments;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import com.christy.tripline.R;
import com.christy.tripline.model.NoteItemModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditNoteFragment extends Fragment {


    public EditNoteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // UI
        View view = inflater.inflate(R.layout.fragment_add_note, container, false);

        // hide action bar
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();

        TextView head = view.findViewById(R.id.note_head);
        final TextView title = view.findViewById(R.id.input_title);
        final TextView body = view.findViewById(R.id.input_body);
        Button submitButton = view.findViewById(R.id.submit_button);
        head.setText("Edit Note");

        // get info from bundle
        Bundle bundle = getArguments();
        final String tripTitle = bundle.getString("tripTitle");
        final String dayNumber = bundle.getString("dayNumber");
        final String pushId = bundle.getString("pushId");
        String noteTitle = bundle.getString("noteTitle");
        String text = bundle.getString("text");
        title.setText(noteTitle);
        body.setText(text);

        // show keyboard
        title.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newTitle = title.getText().toString();
                String newText = body.getText().toString();

                String uid = FirebaseAuth.getInstance().getUid();
                FirebaseDatabase.getInstance().getReference().child("Users").child(uid)
                        .child("Trips").child(tripTitle).child("Days").child(dayNumber).
                        child(pushId).setValue(new NoteItemModel("note", newTitle, newText));

                backToPlan();
            }
        });


        return view;
    }


    private void backToPlan() {

        // hide keyboard
        View currentFocus = getActivity().getCurrentFocus();
        if (currentFocus != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }

        // remove the fragment and return back to the parent activity
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction().remove(this).commit();
        //pop fragment out of back stack
        fm.popBackStack();
    }

    @Override
    public void onStop() {
        super.onStop();
        // show action bar
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
    }
}

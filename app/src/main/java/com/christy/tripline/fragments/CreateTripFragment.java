package com.christy.tripline.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import com.christy.tripline.Tasks.GetCountryCodeTask;
import com.christy.tripline.R;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.Arrays;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class CreateTripFragment extends Fragment implements TextView.OnEditorActionListener {

    private EditText titleEditText;
    private TextView destinationView;
    private EditText daysEditText;
    private DatabaseReference tripsReference;
    private boolean cancel;
    private String coordinate;
    private final static int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private FloatingActionButton fab;
    private static String APP_API_KEY;



    public CreateTripFragment() {
        // Required empty public constructor
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {

            cancel = false;
            validateDays();
            validateTitle();// validateDes() is inside it
            return true;
        }
        return false;
    }

    //  Defines the listener interface with a method passing back data result.
    public interface CreateTripListener {
        void onFinishCreateTrip(String inputTitle, String inputDestination, String days, String countryCode, String coordinates);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        APP_API_KEY = getString(R.string.app_api_key);

        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_create_trip, container, false);
        titleEditText = view.findViewById(R.id.input_title);
        destinationView = view.findViewById(R.id.input_destination);
        daysEditText = view.findViewById(R.id.input_days);

        // hide FAB
        fab = getActivity().findViewById(R.id.add_trip_fab);
        fab.hide();

        // hide action bar
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();

        FirebaseAuth auth = FirebaseAuth.getInstance();
        String uID = auth.getCurrentUser().getUid();
        tripsReference = FirebaseDatabase.getInstance().getReference().child("Users").child(uID).child("Trips");

        destinationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchAutoComplete();
            }
        });

        //Setup a callback when the "Done" button is pressed on keyboard
        daysEditText.setOnEditorActionListener(this);

        // show keyboard
        // getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        // doesn't work, cuz window is got from activity not fragment.[getDialog().getWindow() works]
        //InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        //imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

        // initialize Places
        if (!Places.isInitialized()) {
            Places.initialize(getContext(), APP_API_KEY);
        }
        // to always hide any soft input area when this window receives focus
        // getActivity().getWindow().setSoftInputMode(
        //    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        return view;
    }

    private void launchAutoComplete() {

        List<Place.Field> fields = Arrays.asList(Place.Field.NAME, Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                .setTypeFilter(TypeFilter.CITIES)
                .build(getContext());
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {

            if (resultCode == Activity.RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                String placeName = place.getName().toString();
                LatLng coordinates = place.getLatLng();
                String lat = String.valueOf(coordinates.latitude);
                String lng = String.valueOf(coordinates.longitude);
                coordinate = lat + " " + lng;

                // get country code and name in background
                new GetCountryCodeTask(getContext(), placeName).execute(coordinates);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String des  = GetCountryCodeTask.getDestination();
                        destinationView.setText(des);
                    }
                }, 2000);

            }
        }
    }

    private void validateDays() {

        try {
            int numOfDays = Integer.parseInt(daysEditText.getText().toString());
            if (numOfDays == 0) {
                daysEditText.requestFocus();
                daysEditText.setError("Need a number larger than 0");
                cancel = true;
            }
        } catch (NumberFormatException e) { // cannot parseInt(""), so empty or not a number
            daysEditText.requestFocus();
            daysEditText.setError("A number is required");
            cancel = true;
        }

    }

    private void validateDes() {
        String destination = destinationView.getText().toString();

        if (TextUtils.isEmpty(destination)) {
            destinationView.setError("Required");
            destinationView.requestFocus();
            cancel = true;
        }

    }

    private void validateTitle() {
        tripsReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String title = titleEditText.getText().toString();
                if (TextUtils.isEmpty(title)) {
                    titleEditText.setError("Required");
                    titleEditText.requestFocus();
                    cancel = true;
                }else {

                    for (DataSnapshot tripData : dataSnapshot.getChildren()) {
                        String currentTitle = tripData.getKey();
                        if (title.equals(currentTitle) ) {
                            titleEditText.setError("You already have a trip called " + title);
                            titleEditText.requestFocus();
                            cancel = true;
                            break;
                        }
                    }
                }
                validateDes();
                backToActivity();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

    }

    private void backToActivity() { // every view is populated, so background task is also finished
        if(!cancel) {

            // hide keyboard
            View currentFocus = getActivity().getCurrentFocus();
            if (currentFocus != null) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
            }

            // Return input text back to activity through the implemented listener
            String title = titleEditText.getText().toString();
            String destination = destinationView.getText().toString();
            String days = daysEditText.getText().toString();
            String countryCode = GetCountryCodeTask.getCountryCode(); // static method
            CreateTripFragment.CreateTripListener listener = (CreateTripFragment.CreateTripListener) getActivity();// ???
            listener.onFinishCreateTrip(title, destination, days, countryCode, coordinate);
            // remove the fragment and return back to the parent activity
            FragmentManager fm = getActivity().getSupportFragmentManager();
            fm.beginTransaction().remove(this).commit();
            //pop fragment out of back stack
            fm.popBackStack();


        }

    }

    @Override
    public void onStop() {
        super.onStop();
        // show action bar
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
        // show fab
        fab.show();
    }
}

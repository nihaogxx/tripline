package com.christy.tripline.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.christy.tripline.Tasks.GetCountryCodeTask;
import com.christy.tripline.R;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditTripFragment extends Fragment implements TextView.OnEditorActionListener {

    private LinearLayout mContainer;
    private EditText titleInput;
    private TextView desInput;
    private TextView dateTextView;
    private RadioGroup audioGroup;
    private String title, newTitle, destination, countryCode, coordinate, oldPrivacy, newPrivacy;
    private static final int REQUEST_CODE = 11;
    private static int PLACE_AUTOCOMPLETE_REQUEST_CODE = 2;

    private DatabaseReference tripsReference;
    private EditTripFragment.EditTripListener listener;
    private static String APP_API_KEY;


    public EditTripFragment() {
        // Required empty public constructor
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) { // title

            validateTitle();

            // hide keyboard
            View currentFocus = getActivity().getCurrentFocus();
            if (currentFocus != null) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }
            // container regain focus
            mContainer.requestFocus();

            return true;
        }

        return false;
    }


    public interface EditTripListener {
        void onFinishEditTrip(String title, String destination, String date, String countryCode,
                              String coordinate, String privacy);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        APP_API_KEY = getString(R.string.app_api_key);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_trip, container, false);

        // find views
        mContainer = view.findViewById(R.id.edit_trip_container);
        titleInput = view.findViewById(R.id.input_title);
        desInput = view.findViewById(R.id.input_destination);
        dateTextView = view.findViewById(R.id.start_date);
        Button submitButton = view.findViewById(R.id.submit_button);
        ImageView titleEditButton = view.findViewById(R.id.title_edit);
        ImageView dateEditButton = view.findViewById(R.id.date_edit);
        audioGroup = view.findViewById(R.id.privacy_group);

        // hide action bar
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();

        // set views
        Bundle bundle = getArguments();
        title = bundle.getString("title");
        newTitle = title; // must set value here, or else new title will be null if title is not changed
        destination = bundle.getString("destination");
        titleInput.setText(title);
        desInput.setText(destination);
        countryCode = bundle.getString("countryCode");
        coordinate = bundle.getString("coord");
        dateTextView.setText(bundle.getString("date")); // no need to compare old and new date so make it local
        oldPrivacy = bundle.getString("privacy");

        // check audio button
        if (oldPrivacy.equals("private")){
            audioGroup.check(R.id.private_button);
        } else audioGroup.check(R.id.public_button);

        // get database reference
        FirebaseAuth auth = FirebaseAuth.getInstance();
        final String uID = auth.getCurrentUser().getUid();
        tripsReference = FirebaseDatabase.getInstance().getReference().child("Users").child(uID).child("Trips");

        // set listeners
        titleEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                title = titleInput.getText().toString();
                // set cursor at the end of the text
                titleInput.setSelection(title.length());
                // show keyboard
                titleInput.requestFocus();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

            }
        });

        titleInput.setOnEditorActionListener(this);

        desInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchAutoComplete();
            }
        });


        dateEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                insertDatePicker();
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                backToActivity();

            }
        });

        if (!Places.isInitialized()){
            Places.initialize(getContext(), APP_API_KEY);
        }

        return view;
    }


    private void launchAutoComplete() {

        List<Place.Field> fields = Arrays.asList(Place.Field.NAME, Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                .setTypeFilter(TypeFilter.CITIES)
                .build(getContext());
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }


    private void validateTitle() {

        newTitle = titleInput.getText().toString();

        if (!TextUtils.isEmpty(newTitle) && !newTitle.equals(title)) {

            // check title duplication
            tripsReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    boolean cancel = false;

                    for (DataSnapshot tripData : dataSnapshot.getChildren()) {
                        String currentTitle = tripData.getKey();
                        if (newTitle.equals(currentTitle)) {
                            titleInput.setError("You already have a trip called " + newTitle);
                            cancel = true;
                            break;
                        }
                    }

                    if (!cancel) {
                        titleInput.setText(newTitle);

                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });
        }
    }




    private void insertDatePicker() {

        DialogFragment childFragment = new DatePicker();
//     1.
        childFragment.setTargetFragment(EditTripFragment.this, REQUEST_CODE);
        childFragment.show(getActivity().getSupportFragmentManager(), "datePicker");

//     2.   FragmentTransaction ft = getChildFragmentManager().beginTransaction();
//        ft.replace(R.id.child_fragment_container, childFragment).commit();

//     3.  childFragment.show(getChildFragmentManager(),"datePicker");

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) { // for date

            String startDate = data.getStringExtra("selectedDate");
            // update date UI
            dateTextView.setText(startDate);
            // update database
            tripsReference.child(title).child("startDate").setValue(startDate);

        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {  // for des

            if (resultCode == Activity.RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                String placeName = place.getName().toString();
                LatLng coordinates = place.getLatLng();
                String lat = String.valueOf(coordinates.latitude);
                String lng = String.valueOf(coordinates.longitude);
                coordinate = lat + " " + lng;

                // get country code and name in background
                new GetCountryCodeTask(getContext(), placeName).execute(coordinates);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String des  = GetCountryCodeTask.getDestination();
                        desInput.setText(des);
                    }
                }, 2000);
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;

    }

    private void backToActivity() {

        // Return new data to activity through the implemented listener;
        // title and des r instance variables
        String date = dateTextView.getText().toString();
        destination = desInput.getText().toString(); // updated in the background
        countryCode = GetCountryCodeTask.getCountryCode();

        // get latest checked radio button
        int id = audioGroup.getCheckedRadioButtonId();
        if (id == R.id.public_button) {
            newPrivacy = "public";
        } else {
            newPrivacy = "private";
        }

        listener = (EditTripFragment.EditTripListener) getActivity(); // CurrentTripActivity
        listener.onFinishEditTrip(newTitle, destination, date, countryCode, coordinate, newPrivacy);

        // remove the fragment and return back to the parent activity
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction().remove(this).commit();
        //pop fragment out of back stack
        fm.popBackStack();

    }

    @Override
    public void onStop() {
        super.onStop();
        // show action bar
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
    }
}

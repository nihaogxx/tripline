package com.christy.tripline.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import com.christy.tripline.activities.AddPlaceActivity;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimePicker extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    private final Calendar c = Calendar.getInstance();
    private String tag;

    public TimePicker() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        if (getArguments() != null) {
            tag = getArguments().getString("tag");
        }

        int hour = c.get(Calendar.HOUR);
        int minute = c.get(Calendar.MINUTE);
        return new TimePickerDialog(getActivity(), this, hour, minute, true);
    }

    @Override
    public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {

        c.set(Calendar.HOUR, hourOfDay);
        c.set(Calendar.MINUTE, minute);

        String selectedTime = new SimpleDateFormat("HH:mm", Locale.ENGLISH).format(c.getTime());

        if (tag != null && (tag.equals("departure") || tag.equals("arrival"))) {
            // going back to add place activity
            AddPlaceActivity activity = (AddPlaceActivity)getActivity();
            activity.getTimePickerResult(selectedTime, tag);
        }
        else {
            // going back to read more fragment
            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK,
                    new Intent().putExtra("selectedTime", selectedTime));

        }


    }
}

package com.christy.tripline.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.christy.tripline.R;
import com.christy.tripline.adapters.ExploreRecyclerAdapter;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import cz.msebera.android.httpclient.Header;


/**
 * A simple {@link Fragment} subclass.
 */
public class ExploreFragment extends Fragment {

    //UI
    private RecyclerView mRecyclerView;
    private ExploreRecyclerAdapter mExploreAdapter;
    // Data
    private ArrayList<String> nameList ;
    private ArrayList<Float> ratingList;
    private ArrayList<String> typeList ;
    private ArrayList<String> idList;
    private ArrayList<String> photoList;
    private ArrayList<String> openList ;
    private String destination, title;


    public ExploreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        String textSearchUrl = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=";
        String serverKey = getString(R.string.server_key);

        nameList = new ArrayList<>();
        ratingList = new ArrayList<>();
        typeList = new ArrayList<>();
        idList = new ArrayList<>();
        photoList = new ArrayList<>();
        openList = new ArrayList<>();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_explore, container, false);
        mRecyclerView = view.findViewById(R.id.list);
        // set layout manager
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(linearLayoutManager);

        if (getArguments() != null) {

            title = getArguments().getString("title");
            destination = getArguments().getString("destination");
            String[] desName = destination.split(" ");

            for (int i = 0; i < desName.length; i++) {
                textSearchUrl = textSearchUrl + desName[i] + "+";
            }

            // set adapter
            mExploreAdapter = new ExploreRecyclerAdapter(getActivity(), nameList,
                    ratingList, typeList, idList, openList, photoList, title);
            mRecyclerView.setAdapter(mExploreAdapter);

            AsyncHttpClient client = new AsyncHttpClient();
            textSearchUrl += "point+of+interest&language=en&key=" + serverKey;
            client.get(textSearchUrl, new JsonHttpResponseHandler(){

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    try {

                        for (int i = 0; i < response.getJSONArray("results").length(); i++) {

                            JSONObject currentObj = response.getJSONArray("results").getJSONObject(i);
                            String photoRef = "";
                            if (currentObj.has("photos")){
                                photoRef = currentObj.getJSONArray("photos").getJSONObject(0).getString("photo_reference");
                            }
                            if (!photoRef.equals("")) {

                                String name = currentObj.getString("name");
                                final String placeId = currentObj.getString("place_id");

                                String type = "";
                                if (currentObj.has("types")){
                                    type = currentObj.getJSONArray("types").get(0).toString();
                                }

                                float rating = -1;
                                if (currentObj.has("rating")){
                                    rating = (float)currentObj.getDouble("rating");
                                }

                                if (currentObj.has("opening_hours") && currentObj.getJSONObject("opening_hours").has("open_now") ) {
                                    boolean open = response.getJSONArray("results").getJSONObject(i)
                                            .getJSONObject("opening_hours").getBoolean("open_now");
                                    openList.add("" + open);
                                } else openList.add("null");

                                nameList.add(name);
                                ratingList.add(rating);
                                typeList.add(type);
                                idList.add(placeId);
                                photoList.add(photoRef);

                            }

                        }
                        mExploreAdapter.notifyItemRangeInserted(0, nameList.size() - 1);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                }

            });

        }

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
//        mRecyclerView.setAdapter(null);
    }
}

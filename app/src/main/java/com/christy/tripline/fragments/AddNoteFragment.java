package com.christy.tripline.fragments;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import com.christy.tripline.R;
import com.christy.tripline.model.NoteItemModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddNoteFragment extends Fragment {

    private String tripTitle;
    private String dayNumber;
    //private int index;


    public AddNoteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_note, container, false);

        // hide action bar
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();

        Button submitButton = view.findViewById(R.id.submit_button);
        final EditText inputBody = view.findViewById(R.id.input_body);
        final EditText noteTitle = view.findViewById(R.id.input_title);


        tripTitle = getArguments().getString("tripTitle");
        dayNumber = getArguments().getString("dayNumber");
        //index = getArguments().getInt("itemCount");

        // show keyboard
        noteTitle.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);


        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String title = noteTitle.getText().toString();
                String body = inputBody.getText().toString();

                NoteItemModel newNote = new NoteItemModel("note", title, body);

                String uid = FirebaseAuth.getInstance().getUid();
                DatabaseReference noteRef= FirebaseDatabase.getInstance().getReference().child("Users").child(uid)
                        .child("Trips").child(tripTitle).child("Days").child(dayNumber).push();

                noteRef.setValue(newNote);

                backToPlan();

            }
        });


        return view;
    }


    private void backToPlan() {

        // hide keyboard
        View currentFocus = getActivity().getCurrentFocus();
        if (currentFocus != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

        }

        // remove the fragment and return back to the parent activity
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction().remove(this).commit();
        //pop fragment out of back stack
        fm.popBackStack();
    }

    @Override
    public void onStop() {
        super.onStop();
        // show action bar
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
    }
}

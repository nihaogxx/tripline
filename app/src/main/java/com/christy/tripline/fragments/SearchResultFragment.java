package com.christy.tripline.fragments;


import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.christy.tripline.R;
import com.christy.tripline.adapters.HomeRecyclerAdapter;
import com.christy.tripline.adapters.TripsRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.LinkedList;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchResultFragment extends Fragment {

    private String destination;
    private String tag;
    private FloatingActionButton fab;
    private HomeRecyclerAdapter mHomeAdapter;
    private TripsRecyclerAdapter mTripsAdapter;
    private LinkedList<String> titleList = new LinkedList<>();
    private LinkedList<String> uIdList = new LinkedList<>();
    private LinkedList<String> desList = new LinkedList<>();
    private LinkedList<String> backgroundList = new LinkedList<>();
    private TextView desView;

    public SearchResultFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_search_result, container, false);
        desView = view.findViewById(R.id.desView);

        Bundle bundle = getArguments();
        destination = bundle.getString("des");
        desView.setText(destination);
        tag = getTag();

        // hide FAB
        if (tag.equals("MySearchResult")) { // HomeActivity does not have fab
            fab = getActivity().findViewById(R.id.add_trip_fab);
            fab.hide();
        }

        // get the Firebase references
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

        // set up layout manager
        RecyclerView desRecyclerView = view.findViewById(R.id.list);
        desRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        if (tag.equals("HomeSearchResult")) { // from home

            // instantiate and associate adapter
            mHomeAdapter = new HomeRecyclerAdapter(getActivity(), titleList, uIdList, desList);
            desRecyclerView.setAdapter(mHomeAdapter);

            databaseReference.child("Public Trips").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot tripData : dataSnapshot.getChildren()) {
                        String des = tripData.child("destination").getValue().toString();
                        if (des.equals(destination)) {
                            String uid = tripData.child("uId").getValue().toString();
                            String title = tripData.child("title").getValue().toString();
                            desList.addFirst(des);
                            uIdList.addFirst(uid);
                            titleList.addFirst(title);
                        }

                    }
                    if (titleList.size() == 0) {
                        String des = "No Result for " + destination;
                        desView.setText(des);
                    }
                    else mHomeAdapter.notifyItemRangeInserted(0, titleList.size());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        } else if (tag.equals("MySearchResult")) { // from trips

            mTripsAdapter = new TripsRecyclerAdapter(getActivity(), titleList, backgroundList);
            desRecyclerView.setAdapter(mTripsAdapter);

            FirebaseAuth auth = FirebaseAuth.getInstance();
            String uId = auth.getCurrentUser().getUid();
            databaseReference.child("Users").child(uId).child("Trips").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot tripData : dataSnapshot.getChildren()) {
                        String des = tripData.child("destination").getValue().toString();
                        if (des.equals(destination)) {
                            String background = tripData.child("background").getValue().toString();
                            String title = tripData.getKey();
                            titleList.addFirst(title);
                            backgroundList.addFirst(background);
                        }
                    }
                    if (titleList.size() == 0) {
                        String des = "No Result for " + destination;
                        desView.setText(des);
                    }
                    mTripsAdapter.notifyItemRangeInserted(0, titleList.size());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
        return view;
    }


    @Override
    public void onStop() {
        super.onStop();
        desView.setText(null);
        if (tag.equals("MySearchResult")) {
            fab.show();
        }


    }
}

package com.christy.tripline.fragments;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.christy.tripline.R;
import com.christy.tripline.adapters.BookmarkAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class BookmarkFragment extends Fragment {

    private String duration, title;
    private DatabaseReference tripRef;

    private RecyclerView mRecyclerView;
    private BookmarkAdapter mBookmarkAdapter;
    private ArrayList<String> idList = new ArrayList<>();
    private ArrayList<String> pushIdList = new ArrayList<>();
    private ChildEventListener childEventListener;


    public BookmarkFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bookmark, container, false);
        mRecyclerView = view.findViewById(R.id.list);
        // set layout manager
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(linearLayoutManager);

        if (getArguments() != null) {

            title = getArguments().getString("title");
            duration = getArguments().getString("duration");

            mBookmarkAdapter = new BookmarkAdapter(getActivity(), title, duration, idList, pushIdList);
            mRecyclerView.setAdapter(mBookmarkAdapter);

            tripRef = FirebaseDatabase.getInstance().getReference().child("Users").child(FirebaseAuth
                    .getInstance().getCurrentUser().getUid()).child("Trips").child(title);

            // clear old data so that we when get back here from other fragments, the data displayed are updated.
            int count = idList.size();
            idList.clear();
            pushIdList.clear();
            mBookmarkAdapter.notifyItemRangeRemoved(0, count);

            childEventListener = new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    String placeId = dataSnapshot.getValue().toString();
                    idList.add(placeId);
                    String pushId = dataSnapshot.getKey();
                    pushIdList.add(pushId);
                    mBookmarkAdapter.notifyItemInserted(idList.size()-1);
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                    String placeId = dataSnapshot.getValue().toString();
                    String pushId = dataSnapshot.getKey();
                    int index = pushIdList.indexOf(pushId);
                    idList.remove(placeId);
                    pushIdList.remove(pushId);
                    mBookmarkAdapter.notifyItemRemoved(index);

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            };

            tripRef.child("Bookmarks").addChildEventListener(childEventListener);
        }

        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        tripRef.child("Bookmarks").removeEventListener(childEventListener);
        // other wise every time onCreateView called there will be another listener added and data duplicated in bookmark recycler view
    }
}

package com.christy.tripline.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.christy.tripline.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.json.JSONException;
import org.json.JSONObject;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import cz.msebera.android.httpclient.Header;


/**
 * A simple {@link Fragment} subclass.
 */
public class WeatherFragment extends Fragment {

    // Constants:
    private final String WEATHER_URL_CURRENT = "http://api.openweathermap.org/data/2.5/weather";
    private final String WEATHER_URL_TODAY = "http://api.openweathermap.org/data/2.5/forecast";

    //UI
    private TextView currentTempView;
    private ImageView currentIcon;
    private TextView dayView;
    private TextView minTempView;
    private TextView maxTempView;
    private LinearLayout todayContainer;
    private TextView changeUnitButton;
    private TextView currentUnit;
    private TextView descriptionView;
    private TextView windView;
    private TextView visibilityView;
    private TextView pressureView;
    private TextView humidityView;
    private TextView sunriseView;
    private TextView sunsetView;

    static String APP_ID;
    private String city;
    private String countryCode;


    public WeatherFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        APP_ID = getString(R.string.weather_key);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_weather, container, false);
        TextView cityView = view.findViewById(R.id.city_name);
        currentTempView = view.findViewById(R.id.temperature_current);
        currentIcon = view.findViewById(R.id.condition_current);
        todayContainer = view.findViewById(R.id.weather_today_container);
        dayView = view.findViewById(R.id.today);
        minTempView = view.findViewById(R.id.temperature_min);
        maxTempView = view.findViewById(R.id.temperature_max);
        changeUnitButton = view.findViewById(R.id.change_unit);
        currentUnit = view.findViewById(R.id.current_unit);
        windView = view.findViewById(R.id.speed);
        descriptionView = view.findViewById(R.id.description);
        visibilityView = view.findViewById(R.id.visibility);
        pressureView = view.findViewById(R.id.pressure);
        humidityView = view.findViewById(R.id.humidity);
        sunriseView = view.findViewById(R.id.sunrise);
        sunsetView = view.findViewById(R.id.sunset);

        if (getArguments() != null) {

            city = getArguments().getString("destination");
            cityView.setText(city);
            countryCode = getArguments().getString("countryCode");

        }

        getCurrentWeather();
        getWeatherForToday();

        // convert btw °F and °C
        changeUnitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String temp = currentTempView.getText().toString();
                String minT = minTempView.getText().toString();
                String maxT = maxTempView.getText().toString();

                if (changeUnitButton.getText().toString().equals("C")) {
                    // change current temp
                    currentTempView.setText("" + (int) Math.rint((fahrenToCel(Double.parseDouble(temp)))));
                    changeUnitButton.setText("F");
                    currentUnit.setText("°C");

                    // change min and max temp
                    minTempView.setText("" + (int) Math.rint((fahrenToCel(Double.parseDouble(minT)))));
                    maxTempView.setText("" + (int) Math.rint((fahrenToCel(Double.parseDouble(maxT)))));

                    // change temp for every 3 hours
                    for (int i = 0; i < todayContainer.getChildCount(); i++) {
                        View childView = todayContainer.getChildAt(i);
                        TextView tempView = childView.findViewById(R.id.temperature);
                        String string = tempView.getText().toString();
                        String currentTemp = string.substring(0, (string.length() - 2));
                        tempView.setText((int) Math.rint(fahrenToCel(Double.parseDouble(currentTemp))) + "°C");

                    }

                } else {
                    currentTempView.setText("" + (int) Math.rint(celsiusToFahren(Double.parseDouble(temp))));
                    changeUnitButton.setText("C");
                    currentUnit.setText("°F");


                    // change min and max temp
                    minTempView.setText("" + (int) Math.rint((celsiusToFahren(Double.parseDouble(minT)))));
                    maxTempView.setText("" + (int) Math.rint((celsiusToFahren(Double.parseDouble(maxT)))));

                    for (int i = 0; i < todayContainer.getChildCount(); i++) {
                        View childView = todayContainer.getChildAt(i);
                        TextView tempView = childView.findViewById(R.id.temperature);
                        String string = tempView.getText().toString();
                        String currentTemp = string.substring(0, (string.length() - 2));
                        tempView.setText((int) Math.rint(celsiusToFahren(Double.parseDouble(currentTemp))) + "°F");

                    }
                }
            }
        });

        return view;
    }

    private void getCurrentWeather() {
        // Do without loopJ :  write request on output stream and read response on input stream. https://www.baeldung.com/java-http-request
        RequestParams params = new RequestParams();
        params.put("q", city + "," + countryCode);
        params.put("appid", APP_ID);
        // api.openweathermap.org/data/2.5/weather?q=Kuta,Indonesia,ID&appid=AIzaSyAI9H36cuR_M3ZM1RBGe9qnJto-3lJbmXg
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(WEATHER_URL_CURRENT, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    int condition = response.getJSONArray("weather").getJSONObject(0).getInt("id");

                    // get icon
                    int icon = getIconName(condition);

                    // get current temp
                    double tempResult = response.getJSONObject("main").getDouble("temp") - 273.15;
                    int roundValue = (int) Math.rint(celsiusToFahren(tempResult));
                    String temperature = Integer.toString(roundValue);

                    //get min and max temp
                    double minTemp = response.getJSONObject("main").getDouble("temp_min") - 273.15;
                    double maxTemp = response.getJSONObject("main").getDouble("temp_max") - 273.15;

                    // convert to fahrenheit
                    String minT = Integer.toString((int) Math.rint(celsiusToFahren(minTemp)));
                    String maxT = Integer.toString((int) Math.rint(celsiusToFahren(maxTemp)));

                    //get Day of Week
                    Calendar calendar = Calendar.getInstance();
                    int resultDay = calendar.get(Calendar.DAY_OF_WEEK);

                    // get description
                    String description = response.getJSONArray("weather").getJSONObject(0).getString("description");
                    // get wind speed
                    int wind = (int) Math.rint(response.getJSONObject("wind").getDouble("speed"));
                    // get humidity
                    int humidity = response.getJSONObject("main").getInt("humidity");
                    // get pressure
                    int pressure = response.getJSONObject("main").getInt("pressure");
                    // get visibility
                    int visibility = response.getInt("visibility") / 1000;

                    // get sunrise
                    Long sunriseStamp = response.getJSONObject("sys").getLong("sunrise");
                    Timestamp riseStamp = new Timestamp(sunriseStamp * 1000);// can skip this line and pass sunriseStamp*1000 directly to date
                    Date riseDate = new Date(riseStamp.getTime());
                       // solution 1: get hour and minute via calendar
                    Calendar riseCalendar = Calendar.getInstance();
                    riseCalendar.setTime(riseDate);
                    int riseHour = riseCalendar.get(Calendar.HOUR);// 0-12    HOUR_OF_DAY: 0-24
                    int riseMin = riseCalendar.get(Calendar.MINUTE);
                    String sunrise = "";
                    if (riseCalendar.get(Calendar.AM_PM) == Calendar.AM) {

                        sunrise = riseHour + ":" + riseMin + "AM";
                    } else if (riseCalendar.get(Calendar.AM_PM) == Calendar.PM) {

                        sunrise = riseHour + ":" + riseMin + "PM";
                    }

                    // get sunset
                    Long sunsetStamp = response.getJSONObject("sys").getLong("sunset");
                    Date setDate = new Date(sunsetStamp * 1000);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a", Locale.ENGLISH);
                        // solution 2: format date (easier way)
                    String sunset = dateFormat.format(setDate);
                    updateCurrentUI(icon, temperature, resultDay, minT, maxT);
                    updateDetailInfo(description, wind, humidity, pressure, visibility, sunrise, sunset);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject errorResponse) {
                // if failed, check here https://openweathermap.org/faq#error401%20for%20more%20info
                // check personal account to see if it's right api key and if it is active
                Toast.makeText(getActivity(), "" + statusCode, Toast.LENGTH_SHORT).show();
                // This will show when we navigate to BookMark Fragment
                // since WeatherFragment is started at the same time
            }
        });

    }

    private void updateDetailInfo(String description, int wind, int humidity, int pressure, int visibility, String sunrise, String sunset) {

        descriptionView.setText(description);
        windView.setText(wind + " mph");
        humidityView.setText(humidity + "%");
        pressureView.setText("" + pressure); // need to convert to inHg
        visibilityView.setText(visibility + " mi");
        sunriseView.setText(sunrise);
        sunsetView.setText(sunset);
    }

    private double celsiusToFahren(double tempCel) {

        return tempCel * 9 / 5 + 32;

    }

    private double fahrenToCel(double tempFahren) {

        return (tempFahren - 32) * 5 / 9;
    }

    private void updateCurrentUI(int icon, String temperature, int resultDay, String minT, String maxT) {
        //int rscId = getResources().getIdentifier(icon, "drawable", getActivity().getPackageName());
        currentIcon.setImageResource(icon);
        currentTempView.setText(temperature);
        minTempView.setText(minT);
        maxTempView.setText(maxT);
        String dayOfWeek = "SUNDAY";
        switch (resultDay) {

            case 1:
                dayOfWeek = "SUNDAY";
                break;
            case 2:
                dayOfWeek = "MONDAY";
                break;
            case 3:
                dayOfWeek = "TUESDAY";
                break;
            case 4:
                dayOfWeek = "WEDNESDAY";
                break;
            case 5:
                dayOfWeek = "THURSDAY";
                break;
            case 6:
                dayOfWeek = "FRIDAY";
                break;

            case 7:
                dayOfWeek = "SATURDAY";
                break;

        }
        dayView.setText(dayOfWeek);

    }


    private void getWeatherForToday() {
        RequestParams params = new RequestParams();
        params.put("q", city + "," + countryCode);
        params.put("appid", APP_ID);

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(WEATHER_URL_TODAY, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                double temperature;
                int icon;
                String hour;

                try {
                    for (int i = 0; i < 8; i++) {
                        // get temp
                        temperature = response.getJSONArray("list").getJSONObject(i)
                                .getJSONObject("main").getDouble("temp") - 273.15;

                        String temp = Integer.toString((int) Math.rint(celsiusToFahren(temperature)));

                        // get icon
                        int condition = response.getJSONArray("list").getJSONObject(i)
                                .getJSONArray("weather").getJSONObject(0).getInt("id");
                        icon = getIconName(condition);
                        // get hour
                        int _24HourTime = Integer.parseInt(response.getJSONArray("list").getJSONObject(i)
                                .getString("dt_txt").substring(11, 13));
                        hour = _24HourTime % 12 + ((_24HourTime >= 12) ? "PM" : "AM");

                        updateTodayScroller(temp, icon, hour);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject errorResponse) {
                Toast.makeText(getActivity(), "Request Failed", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void updateTodayScroller(String temperature, int icon, String hour) {

        // inflate single hour view
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.single_time_weather, todayContainer, false);
        // set hour
        TextView hourView = view.findViewById(R.id.hour);
        hourView.setText(hour);
        // set icon
        ImageView iconView = view.findViewById(R.id.weather_icon);
        //int rscId = getResources().getIdentifier(icon, "drawable", getActivity().getPackageName());
        iconView.setImageResource(icon);
        // set temp
        TextView tempView = view.findViewById(R.id.temperature);
        tempView.setText(temperature + "°F");

        todayContainer.addView(view);
    }


    private int getIconName(int condition) {
        if (condition >= 0 && condition < 300) {
            return R.drawable.tstorm1;
        } else if (condition >= 300 && condition < 500) {
            return R.drawable.light_rain;
        } else if (condition >= 500 && condition < 600) {
            return R.drawable.shower3;
        } else if (condition >= 600 && condition <= 700) {
            return R.drawable.snow4;
        } else if (condition >= 701 && condition <= 771) {
            return R.drawable.fog;
        } else if (condition >= 772 && condition < 800) {
            return R.drawable.tstorm3;
        } else if (condition == 800) {
            return R.drawable.sunny;
        } else if (condition >= 801 && condition <= 804) {
            return R.drawable.cloudy2;
        } else if (condition >= 900 && condition <= 902) {
            return R.drawable.tstorm3;
        } else if (condition == 903) {
            return R.drawable.snow5;
        } else if (condition == 904) {
            return R.drawable.sunny;
        } else if (condition >= 905 && condition <= 1000) {
            return R.drawable.tstorm3;
        }

        return R.drawable.dunno;
    }


}

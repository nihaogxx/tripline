package com.christy.tripline.fragments;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.christy.tripline.R;
import com.christy.tripline.model.LodgingItemModel;
import com.christy.tripline.model.PlaceItemModel;
import com.christy.tripline.model.TransitItemModel;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.PhotoMetadata;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPhotoRequest;
import com.google.android.libraries.places.api.net.FetchPhotoResponse;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReadMoreAndEditFragment extends Fragment implements OnMapReadyCallback {
    private GoogleMap mMap;
    private MapView mMapView;
    private LatLngBounds viewport;
    private TextView checkInView;
    private TextView checkOutView;
    private TextView numView;
    private TextView dateView;
    private TextView departureView;
    private TextView arrivalView;
    private TextView placeAttrView;
    private LinearLayout transitInfo, lodgingDates;
    private static final int REQUEST_CODE_IN = 12;
    private static final int REQUEST_CODE_OUT = 13;
    private static final int REQUEST_CODE_DATE = 15;
    private static final int REQUEST_CODE_DEPARTURE = 16;
    private static final int REQUEST_CODE_ARRIVAL = 17;
    private static final String URL_DETAIL_REQUEST = "https://maps.googleapis.com/maps/api/place/details/json?";
    private static String APP_API_KEY;
    private String selectedDay, selectedType;
    private String placeType;
    private LinearLayout reviews;
    private DatabaseReference tripRef = null;
    private LatLng location;
    private PlacesClient mPlacesClient;


    public ReadMoreAndEditFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_read_more_and_edit, container, false);

        APP_API_KEY = getString(R.string.app_api_key);

        // hide action bar to show full photos
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        // UI
        final TextView nameView = view.findViewById(R.id.place_name);
        final TextView typeView = view.findViewById(R.id.place_type);
        final TextView openView = view.findViewById(R.id.open_now);
        final TextView hoursView = view.findViewById(R.id.open_hours);
        final TextView ratingView = view.findViewById(R.id.place_rating);
        final TextView addressView = view.findViewById(R.id.place_address);
        final RatingBar ratingBar = view.findViewById(R.id.rating_bar);
        final LinearLayout gallery = view.findViewById(R.id.place_gallery);
        final TextView optional = view.findViewById(R.id.optional);
        final ImageView bookmark = view.findViewById(R.id.bookmark);
        final ImageView callView = view.findViewById(R.id.call);
        final ImageView websiteView = view.findViewById(R.id.website);
        View addDivider = view.findViewById(R.id.add_divider);
        final View hoursDivider = view.findViewById(R.id.hours_divider);
        Button updateButton = view.findViewById(R.id.update_button);
        LinearLayout addLayout = view.findViewById(R.id.add_layout);
        final Spinner daySpinner = view.findViewById(R.id.day_spinner);
        Spinner typeSpinner = view.findViewById(R.id.type_spinner);
        final LinearLayout hoursLayout = view.findViewById(R.id.hours_layout);
        LinearLayout bookmarkLayout = view.findViewById(R.id.bookmark_layout);
        placeAttrView = view.findViewById(R.id.place_attri);

        lodgingDates = view.findViewById(R.id.lodging_dates);
        checkInView = view.findViewById(R.id.check_in_date);
        checkOutView = view.findViewById(R.id.check_out_date);
        transitInfo = view.findViewById(R.id.transit_info);
        numView = view.findViewById(R.id.transit_num);
        dateView = view.findViewById(R.id.input_date);
        departureView = view.findViewById(R.id.input_departure_time);
        arrivalView = view.findViewById(R.id.input_arrival_time);
        reviews = view.findViewById(R.id.reviews);

        // set up map view
        mMapView = view.findViewById(R.id.viewport_map);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);

        // initialize Places
        if (!Places.isInitialized()) {
            Places.initialize(getContext(), APP_API_KEY);
        }
        mPlacesClient = Places.createClient(getContext());

        // set time picker
        final View.OnClickListener timePickerListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogFragment childFragment = new TimePicker();

                switch (v.getId()) {

                    case R.id.input_departure_time:

                        childFragment.setTargetFragment(ReadMoreAndEditFragment.this, REQUEST_CODE_DEPARTURE);
                        break;

                    case R.id.input_arrival_time:

                        childFragment.setTargetFragment(ReadMoreAndEditFragment.this, REQUEST_CODE_ARRIVAL);
                        break;
                }

                childFragment.show(getActivity().getSupportFragmentManager(), "timePicker");
            }
        };

        // set date picker
        final View.OnClickListener datePickerListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePicker childFragment = new DatePicker();

                switch (v.getId()) {

                    case R.id.check_in_date:
                        childFragment.setTargetFragment(ReadMoreAndEditFragment.this, REQUEST_CODE_IN);
                        break;

                    case R.id.check_out_date:
                        childFragment.setTargetFragment(ReadMoreAndEditFragment.this, REQUEST_CODE_OUT);
                        break;

                    case R.id.input_date:
                        childFragment.setTargetFragment(ReadMoreAndEditFragment.this, REQUEST_CODE_DATE);
                        break;
                }
                childFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
            }
        };

        Bundle bundle = getArguments();
        final String tag = bundle.getString("tag");
        final String placeId = bundle.getString("placeId");
        final String placeName = bundle.getString("placeName");
        final String title = bundle.getString("title");

        // set name
        nameView.setText(placeName);

        // set bookmark
        if (title != null) { // not from home
            final String[] bookmarkId = {""};

            tripRef = FirebaseDatabase.getInstance().getReference().child("Users").child(FirebaseAuth
                    .getInstance().getCurrentUser().getUid()).child("Trips").child(title);

            tripRef.child("Bookmarks").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot data : dataSnapshot.getChildren()) {

                        // it is bookmarked
                        if (data.getValue().toString().equals(placeId)) {
                            bookmarkId[0] = data.getKey();
                            bookmark.setImageResource(R.drawable.ic_bookmark);
                            bookmark.setSelected(true);
                            break;
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            bookmark.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    v.setSelected(!v.isSelected());

                    if (v.isSelected()) {

                        bookmark.setImageResource(R.drawable.ic_bookmark);
                        // save in database
                        DatabaseReference bookmarkRef = tripRef.child("Bookmarks").push();
                        bookmarkRef.setValue(placeId);
                        bookmarkId[0] = bookmarkRef.getKey();

                    } else if (!v.isSelected()) {

                        bookmark.setImageResource(R.drawable.ic_bookmark_add);
                        // remove from database
                        tripRef.child("Bookmarks").child(bookmarkId[0]).removeValue();

                    }
                }
            });
        } else {

            bookmarkLayout.setVisibility(View.GONE);
        }


        if (tag.equals("fromPoi") || tag.equals("fromExplore")) { //from map info window or explore fragment

            if (tag.equals("fromExplore")) {

                // set open now
                String open = bundle.getString("open");
                if (open.equals("true")) {

                    openView.setText("open now");

                } else if (open.equals("false")) {
                    openView.setText("closed now");
                }
                // set type
                placeType = bundle.getString("type");
                typeView.setText(placeType);

                // set rating
                float rating = bundle.getFloat("rating");
                if (rating > 0) {
                    ratingBar.setRating(rating);
                    ratingView.setText(String.valueOf(rating));
                } else {
                    ratingBar.setVisibility(View.INVISIBLE);
                    ratingView.setVisibility(View.INVISIBLE);
                }

            }

            // show add layout
            addLayout.setVisibility(View.VISIBLE);
            updateButton.setVisibility(View.VISIBLE);
            updateButton.setText("ADD");
            addDivider.setVisibility(View.VISIBLE);

            // set up day spinner
            tripRef.child("duration").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    String duration = dataSnapshot.getValue().toString();

                    String[] days = new String[Integer.parseInt(duration)];
                    for (int i = 0; i < Integer.parseInt(duration); i++) {
                        days[i] = "Day" + (i + 1);
                    }
                    ArrayAdapter<String> dayAdapter = new ArrayAdapter<>(getActivity(), R.layout.single_spinner_item, days);
                    dayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                    daySpinner.setAdapter(dayAdapter);

                    daySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            selectedDay = "DAY " + (position + 1);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });


            // set up type spinner
            String[] types = {"Place", "Transit", "Lodging"};
            ArrayAdapter<String> typeAdapter = new ArrayAdapter<>(getActivity(), R.layout.single_spinner_item, types);
            typeAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            typeSpinner.setAdapter(typeAdapter);

            typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    switch (position) {

                        case 0:
                            selectedType = "place";
                            transitInfo.setVisibility(View.GONE);
                            lodgingDates.setVisibility(View.GONE);
                            optional.setVisibility(View.GONE);
                            break;
                        case 1:
                            selectedType = "transit";
                            transitInfo.setVisibility(View.VISIBLE);
                            optional.setVisibility(View.VISIBLE);
                            lodgingDates.setVisibility(View.GONE);
                            dateView.setOnClickListener(datePickerListener);
                            departureView.setOnClickListener(timePickerListener);
                            arrivalView.setOnClickListener(timePickerListener);
                            break;
                        case 2:
                            selectedType = "lodging";
                            lodgingDates.setVisibility(View.VISIBLE);
                            optional.setVisibility(View.VISIBLE);
                            transitInfo.setVisibility(View.GONE);
                            checkInView.setOnClickListener(datePickerListener);
                            checkOutView.setOnClickListener(datePickerListener);
                            break;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final DatabaseReference dayRef = tripRef.child("Days").child(selectedDay);

                    // check up database for duplicate item
                    dayRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            //itemCount = (int)dataSnapshot.getChildrenCount();
                            boolean cancel = false;
                            for (DataSnapshot itemData : dataSnapshot.getChildren()) {
                                if (itemData.child("placeId").exists()) {
                                    String id = itemData.child("placeId").getValue().toString();
                                    if (placeId.equals(id)) {
                                        Toast.makeText(getActivity(), "Item already exists in " + selectedDay, Toast.LENGTH_SHORT).show();
                                        cancel = true;
                                    }
                                }
                            }
                            if (!cancel) {
                                saveItem(dayRef);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                }

                private void saveItem(DatabaseReference dayRef) {

                    final DatabaseReference itemRef = dayRef.push();

                    switch (selectedType) {

                        case "place":
                            PlaceItemModel place = new PlaceItemModel("place", placeId, placeName, placeType.toLowerCase());
                            itemRef.setValue(place);
                            break;

                        case "transit":
                            String transitNum = numView.getText().toString();
                            String date = dateView.getText().toString();
                            String departureTime = departureView.getText().toString();
                            String arrivalTime = arrivalView.getText().toString();
                            String time = departureTime + " - " + arrivalTime;

                            TransitItemModel transit = new TransitItemModel("transit", placeId, placeName,
                                    transitNum, date, time);
                            itemRef.setValue(transit);
                            break;
                        case "lodging":

                            String checkInDate = checkInView.getText().toString();
                            String checkOutDate = checkOutView.getText().toString();
                            String dates = checkInDate + " - " + checkOutDate;

                            LodgingItemModel lodging = new LodgingItemModel("lodging", placeId, placeName, dates);
                            itemRef.setValue(lodging);
                            break;
                    }

                    Toast.makeText(getActivity(), "Adding to " + selectedDay, Toast.LENGTH_SHORT).show();
                }
            });

        } else if (tag.equals("fromPlace")) {

            String placeType = bundle.getString("placeType");
            typeView.setText(placeType);

        } else if (tag.equals("fromLodging") || tag.equals("fromTransit")) {

            optional.setVisibility(View.VISIBLE);
            addDivider.setVisibility(View.VISIBLE);

            // get bundle info
            final String pushId = bundle.getString("pushId");
            final String dayNumber = bundle.getString("dayNumber");
            String info = bundle.getString("info");

            if (tag.equals("fromLodging")) {
                typeView.setText("lodging");
                String checkIn = info.substring(0, 10);
                String checkOut = info.substring(13);
                checkInView.setText(checkIn);
                checkOutView.setText(checkOut);
                lodgingDates.setVisibility(View.VISIBLE);
                checkInView.setOnClickListener(datePickerListener);
                checkOutView.setOnClickListener(datePickerListener);

            } else { // from transit

                typeView.setText("transit");
                transitInfo.setVisibility(View.VISIBLE);

                String[] lines = info.split("\r\n|\r|\n");
                String transitNum = lines[0];
                String date = lines[1];
                String time = lines[2];
                String departure = time.substring(0, 5);
                String arrival = time.substring(8);

                numView.setText(transitNum);
                dateView.setText(date);
                departureView.setText(departure);
                arrivalView.setText(arrival);

                dateView.setOnClickListener(datePickerListener);
                departureView.setOnClickListener(timePickerListener);
                arrivalView.setOnClickListener(timePickerListener);

            }

            updateButton.setVisibility(View.VISIBLE);

            updateButton.setOnClickListener(new View.OnClickListener() {

                DatabaseReference itemRef = tripRef.child("Days").child(dayNumber).child(pushId);

                @Override
                public void onClick(View v) {

                    if (tag.equals("fromLodging")) {

                        String dates = checkInView.getText().toString() + " - " + checkOutView.getText().toString();
                        itemRef.child("dates").setValue(dates);
                        Toast.makeText(getContext(), "Dates are updated", Toast.LENGTH_SHORT).show();

                    } else {

                        String number = numView.getText().toString();
                        String date = dateView.getText().toString();
                        String departure = departureView.getText().toString();
                        String arrival = arrivalView.getText().toString();
                        itemRef.child("date").setValue(date);
                        itemRef.child("transitNum").setValue(number);
                        itemRef.child("time").setValue(departure + " - " + arrival);

                        Toast.makeText(getContext(), "Transit info are updated", Toast.LENGTH_SHORT).show();
                    }

                }
            });

        }

        // get place by ID
        List<Place.Field> fields = Arrays.asList(Place.Field.PHOTO_METADATAS,
                Place.Field.TYPES, Place.Field.ADDRESS, Place.Field.OPENING_HOURS, Place.Field.PHONE_NUMBER,
                Place.Field.LAT_LNG, Place.Field.RATING, Place.Field.VIEWPORT, Place.Field.WEBSITE_URI);
        FetchPlaceRequest placeRequest = FetchPlaceRequest.builder(placeId, fields).build();

        mPlacesClient.fetchPlace(placeRequest).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
            @Override
            public void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
                final Place place = fetchPlaceResponse.getPlace();
                // get place attributions
                List<String> list = place.getAttributions();
                if (list.size() != 0){
                    placeAttrView.setVisibility(View.VISIBLE);
                    placeAttrView.setText(place.getAttributions().toString());
                } else placeAttrView.setVisibility(View.GONE);

                // get photo
                List<PhotoMetadata> photos = place.getPhotoMetadatas();
                if (photos != null) {

                    for (int i = 0; i < photos.size(); i++) {
                        final View photoView = LayoutInflater.from(getActivity()).inflate(R.layout.single_photo_horizental,
                                gallery, false);
                        final ImageView imageView = photoView.findViewById(R.id.place_photo);
                        final TextView attriView = photoView.findViewById(R.id.photo_attributions);

                        // get photo data
                        PhotoMetadata photo = photos.get(i);
                        // get photo attributions
                        String attrOri = photo.getAttributions();
                        attrOri = attrOri.substring(3, attrOri.length()-4);
                        final String link = attrOri.substring(6, attrOri.indexOf('>')-1);
                        final String author = attrOri.substring(attrOri.indexOf('>')+1);
                        // Create a FetchPhotoRequest.
                        FetchPhotoRequest photoRequest = FetchPhotoRequest.builder(photo).build();
                        mPlacesClient.fetchPhoto(photoRequest).addOnSuccessListener(new OnSuccessListener<FetchPhotoResponse>() {
                            @Override
                            public void onSuccess(FetchPhotoResponse fetchPhotoResponse) {
                                Bitmap bitmap = fetchPhotoResponse.getBitmap();
                                imageView.setImageBitmap(bitmap);
                                attriView.setText("by " + author);
                                attriView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                                        startActivity(intent);
                                    }
                                });
                                gallery.addView(photoView);

                            }
                        });
                    }
                }

                // get location
                location = place.getLatLng();
                // get viewport
                viewport = place.getViewport();

                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(viewport, 500, 400, 0);
                mMap.moveCamera(cu);
                mMap.addMarker(new MarkerOptions().position(location));

                // get Address
                String address = (String) place.getAddress();
                addressView.setText(address);

                // get phone#
                final String phoneNumber = (String) place.getPhoneNumber();
                callView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (phoneNumber != null && !phoneNumber.isEmpty()) {

                            startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber)));

                        } else {

                            Toast.makeText(getActivity(), "Call is not available for this place",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                // get website link
                websiteView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (place.getWebsiteUri() != null) {
                            String link = place.getWebsiteUri().toString();

                            if (!link.startsWith("http://") && !link.startsWith("https://")) {

                                link = "https://" + link;
                            }
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                            startActivity(intent);

                        } else {

                            Toast.makeText(getActivity(), "Website is not available for this place",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                });

                // get opening hours
                if(place.getOpeningHours() != null){
                    hoursLayout.setVisibility(View.VISIBLE);
                    hoursDivider.setVisibility(View.VISIBLE);
                    List<String> hours = place.getOpeningHours().getWeekdayText();

                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < hours.size(); i++) {

                        sb.append(hours.get(i)).append("\n").append("\n");

                    }
                    hoursView.setText(sb.toString());
                }

                // get rating if not from explore
                if (!tag.equals("fromExplore")) {

                    if (place.getRating() != null) {
                        float rating = place.getRating().floatValue();
                        DecimalFormat df = new DecimalFormat("#.#");
                        String placeRating = df.format(rating);
                        ratingView.setText(placeRating);
                        ratingBar.setRating(rating);
                    } else ratingView.setVisibility(View.GONE);
                }

                // get place type
                if (tag.equals("fromPoi") || tag.equals("fromPlan") || tag.equals("fromReadOnly")) {
                    if (place.getTypes() != null) {

                        Place.Type type = place.getTypes().get(0); // Emnu
                        placeType = type.toString().toLowerCase();
                        typeView.setText(placeType);
                    } else typeView.setVisibility(View.GONE);
                }
            }
        });

        // open now?
        String url = URL_DETAIL_REQUEST + "placeid=" + placeId + "&fields=opening_hours&key=" + APP_API_KEY;
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    JSONObject result = response.getJSONObject("result");
                    if (result.has("opening_hours")) {

                        if (result.getJSONObject("opening_hours").has("open_now")) {

                            String open = "" + result.getJSONObject("opening_hours").getBoolean("open_now");
                            if (open.equals("true")) {

                                openView.setText("open now");

                            } else if (open.equals("false")) {
                                openView.setText("closed now");
                            }

                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
            }
        });


        // get reviews via Place Details Requests

        String reviewUrl = URL_DETAIL_REQUEST + "placeid=" + placeId + "&fields=review&key=" + APP_API_KEY;

        AsyncHttpClient reviewClient = new AsyncHttpClient();
        reviewClient.get(reviewUrl, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {

                    if (response.getJSONObject("result").has("reviews")) {

                        for (int i = 0; i < response.getJSONObject("result").getJSONArray("reviews").length(); i++) {

                            View view = LayoutInflater.from(getActivity()).inflate(R.layout.single_review,
                                    reviews, false);

                            ImageView profileView = view.findViewById(R.id.photo);
                            TextView nameView = view.findViewById(R.id.author);
                            RatingBar ratingBar = view.findViewById(R.id.rating_bar);
                            TextView timeView = view.findViewById(R.id.time);
                            TextView textView = view.findViewById(R.id.text);

                            JSONObject review = response.getJSONObject("result").getJSONArray("reviews").getJSONObject(i);
                            String author = review.getString("author_name");
                            double rating = review.getDouble("rating");
                            String time = review.getString("relative_time_description");
                            String text = review.getString("text");
                            String photoUrl = review.getString("profile_photo_url");
                            Picasso.with(getActivity()).load(photoUrl).placeholder(R.drawable.no_image_icon).into(profileView);
                            nameView.setText(author);
                            ratingBar.setRating((float) rating);
                            timeView.setText(time);
                            textView.setText(text);
                            reviews.addView(view);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
            }
        });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {

            switch (requestCode) {

                case REQUEST_CODE_IN:  // date

                    String checkInDate = data.getStringExtra("selectedDate");
                    // update date UI
                    checkInView.setText(checkInDate);

                    break;
                case REQUEST_CODE_OUT:  // date

                    String checkOutDate = data.getStringExtra("selectedDate");
                    // update date UI
                    checkOutView.setText(checkOutDate);

                    break;
                case REQUEST_CODE_DATE:  // date

                    String date = data.getStringExtra("selectedDate");
                    dateView.setText(date);

                    break;
                case REQUEST_CODE_DEPARTURE:  // time

                    String departureTime = data.getStringExtra("selectedTime");
                    departureView.setText(departureTime);

                    break;
                case REQUEST_CODE_ARRIVAL:  // time

                    String arrivalTime = data.getStringExtra("selectedTime");
                    arrivalView.setText(arrivalTime);
                    break;
            }

        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        mMapView.onStop();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }


    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}



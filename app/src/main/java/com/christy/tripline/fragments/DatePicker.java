package com.christy.tripline.fragments;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import com.christy.tripline.activities.AddPlaceActivity;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class DatePicker extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private final Calendar c = Calendar.getInstance();

    private String tag;


    public DatePicker() {
        // Required empty public constructor
    }



    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {// not needing xml

        if (getArguments() != null) {  // not from edit trip fragment

            tag = getArguments().getString("tag");
        }

        // Use the current date as the default date in the picker.
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(android.widget.DatePicker view, int year, int month, int dayOfMonth) {

        c.set(year, month, dayOfMonth);
//        c.set(Calendar.YEAR, year);
//        c.set(Calendar.MONTH, month);
//        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        String selectedDate = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).format(c.getTime()); //c.getTime returns a Date

        if (tag !=null && (tag.equals("CheckIn") || tag.equals("CheckOut") || tag.equals("transit"))) {

            // going back to add place activity
            AddPlaceActivity activity = (AddPlaceActivity)getActivity();
            activity.getDatePickerResult(selectedDate, tag);

        }
        else {  // going back to trip edit fragment or see more fragment (depend on getTargetRequestCode())

            // send back to the target fragment
            getTargetFragment().onActivityResult(
                    getTargetRequestCode(),
                    Activity.RESULT_OK,
                    new Intent().putExtra("selectedDate", selectedDate));

//        getParentFragment().getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
//        getChildFragmentManager().beginTransaction().remove(this).commit();
        }


    }
}

package com.christy.tripline.Tasks;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Locale;

public class GetCountryCodeTask extends AsyncTask<LatLng, Void, Void> {

    private WeakReference<Context> weakContext;
    private  static String placeName;
    private  static String countryCode;
    private  static String countryName;


    public GetCountryCodeTask(Context context, String placeName) {
        this.weakContext = new WeakReference<>(context);
        this.placeName = placeName;
    }

    @Override
    protected Void doInBackground(LatLng... latLngs) {

        LatLng coordinates = latLngs[0];

        Geocoder geocoder = new Geocoder(weakContext.get(), Locale.getDefault());

        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(
                    coordinates.latitude,
                    coordinates.longitude,
                    1);  // Only retrieve 1 address
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null){
            Address address = addresses.get(0);

            countryCode = address.getCountryCode();
            countryName = address.getCountryName();
        }

        return null;
    }


    public static String getCountryCode() {
        return countryCode;
    }

    public static String getDestination() {
        return  placeName + ", " + countryName;
    }
}

package com.christy.tripline.Tasks;

import android.os.AsyncTask;
import com.google.firebase.storage.StorageReference;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class UploadImageToStorageTask extends AsyncTask<String, Void, Void> {

    private StorageReference filePath;

    public UploadImageToStorageTask(StorageReference filePath) {
        this.filePath = filePath;
    }

    @Override
    protected Void doInBackground(String... strings) {
        try {
            URL newUrl = new URL(strings[0]);
            InputStream in = newUrl.openConnection().getInputStream(); // download a http file
            filePath.putStream(in); // upload the file to storage

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
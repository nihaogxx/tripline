package com.christy.tripline.adapters;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.christy.tripline.activities.AddPlaceActivity;
import com.christy.tripline.R;
import com.christy.tripline.model.LodgingItemModel;
import com.christy.tripline.model.NoteItemModel;
import com.christy.tripline.model.PlaceItemModel;
import com.christy.tripline.model.TransitItemModel;
import com.christy.tripline.fragments.AddNoteFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class PlanRecyclerAdapter extends RecyclerView.Adapter<PlanRecyclerAdapter.DayViewHolder> implements OnPhotoPickedListener {

    private Context mContext;
    private String count;
    private String startDate;
    private String title;
    private String countryCode;
    private DatabaseReference tripRef;
    private OnPhotoPickedListener toItemsAdapterListener;


    public PlanRecyclerAdapter(Context context, String count, String startDate, String title, String countryCode) {
        this.mContext = context;
        this.count = count;
        this.startDate = startDate;
        this.title = title;
        this.countryCode = countryCode;

        tripRef = FirebaseDatabase.getInstance().getReference().child("Users").child(FirebaseAuth
                .getInstance().getCurrentUser().getUid()).child("Trips").child(title);

    }

    @NonNull
    @Override
    public PlanRecyclerAdapter.DayViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.plan_item, parent, false);

        return new DayViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PlanRecyclerAdapter.DayViewHolder holder, final int position) {

        //set day#
        final int number = position + 1;
        holder.dayNumView.setText("DAY " + number);

        // set dates
        if (!startDate.equals("mm/dd/yyyy")) {

            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);

            // set start date to calender
            try {
                c.setTime(sdf.parse(startDate)); // Calendar.setTime only handles Date value, so parse String to Date
            } catch (ParseException e) {
                e.printStackTrace();
            }

            // add days to calender
            c.add(Calendar.DATE, position); // Adds or subtracts the specified amount of time to the given calendar field. e.g. c.add(Calendar.DATE, -2)

            // get new date
            String resultDate = sdf.format(c.getTime());

            // set dateView visible
            holder.dateView.setVisibility(View.VISIBLE);

            // update date view
            holder.dateView.setText(resultDate);
        }

        // set inner recycler view
        // each view must have their own lists.
        final ArrayList<String> itemTypeList = new ArrayList<>();
        final ArrayList<String> valueOneList = new ArrayList<>();
        final ArrayList<String> valueTwoList = new ArrayList<>();
        final ArrayList<String> pushIds = new ArrayList<>();

        final ItemsRecyclerAdapter itemsRecyclerAdapter = new ItemsRecyclerAdapter(mContext,
                itemTypeList, valueOneList, valueTwoList, pushIds, title, holder.dayNumView.getText().toString());


        // listener at DayNumber node (cannot set inside view holder, no views r populated then)
        tripRef.child("Days").child("DAY " + number).addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                if (dataSnapshot.exists() && dataSnapshot.hasChild("itemType")) {

                    pushIds.add(dataSnapshot.getKey());

                    String itemType = dataSnapshot.child("itemType").getValue().toString();

                    switch (itemType) {

                        case "place":

                            // add type value
                            itemTypeList.add("place");

                            // get place name and type
                            PlaceItemModel place = dataSnapshot.getValue(PlaceItemModel.class);
                            String placeName = place.getPlaceName();
                            valueOneList.add(placeName);
                            String placeType = place.getPlaceType();
                            valueTwoList.add(placeType);

                            break;

                        case "lodging":

                            // add type value
                            itemTypeList.add("lodging");

                            // get lodging name and dates
                            LodgingItemModel lodging = dataSnapshot.getValue(LodgingItemModel.class);
                            String hotelName = lodging.getPlaceName();
                            String dates = lodging.getDates();
                            valueOneList.add(hotelName);
                            valueTwoList.add(dates);

                            break;


                        case "note":

                            itemTypeList.add("note");
                            String noteTitle = dataSnapshot.getValue(NoteItemModel.class).getNoteTitle();
                            valueOneList.add(noteTitle);
                            String body = dataSnapshot.getValue(NoteItemModel.class).getNoteBody();
                            String[] lines = body.split("\r\n|\r|\n");

                            if (lines.length > 1) {
                                body = lines[0] + "\n...";
                            }

                            valueTwoList.add(body);

                            break;

                        case "transit":

                            itemTypeList.add("transit");
                            TransitItemModel transit = dataSnapshot.getValue(TransitItemModel.class);
                            String transitName = transit.getPlaceName();
                            valueOneList.add(transitName);
                            String date = transit.getDate();
                            String time = transit.getTime();
                            String number = transit.getTransitNum();
                            String info = number + "\n" + date + "\n" + time;
                            valueTwoList.add(info);
                            break;
                    }

                    // set up adapter here, notify child added not working, why
                    holder.items.setLayoutManager(new LinearLayoutManager(mContext));
                    holder.items.setAdapter(itemsRecyclerAdapter);
                }

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                String id = dataSnapshot.getKey();
                int oldIndex = pushIds.indexOf(id);
                String type = dataSnapshot.child("itemType").getValue().toString();
                String valueOne = "", valueTwo = "";
                switch (type) {
                    case "place":
                        valueOne = dataSnapshot.getValue(PlaceItemModel.class).getPlaceName();
                        valueTwo = dataSnapshot.getValue(PlaceItemModel.class).getPlaceType();
                        break;

                    case "note":
                        valueOne = dataSnapshot.getValue(NoteItemModel.class).getNoteTitle();
                        valueTwo = dataSnapshot.getValue(NoteItemModel.class).getNoteBody();
                        String[] lines = valueTwo.split("\r\n|\r|\n");

                        if (lines.length > 1) {
                            valueTwo = lines[0] + "\n...";
                        }

                        break;

                    case "lodging":
                        valueOne = dataSnapshot.getValue(LodgingItemModel.class).getPlaceName();
                        valueTwo = dataSnapshot.getValue(LodgingItemModel.class).getDates();
                        break;

                    case "transit":
                        TransitItemModel transit = dataSnapshot.getValue(TransitItemModel.class);
                        valueOne = transit.getPlaceName();
                        String date = transit.getDate();
                        String time = transit.getTime();
                        String number = transit.getTransitNum();
                        valueTwo = number + "\n" + date + "\n" + time;
                        break;
                }
                valueOneList.set(oldIndex, valueOne);
                valueTwoList.set(oldIndex, valueTwo);
                itemsRecyclerAdapter.notifyItemChanged(oldIndex);

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                String id = dataSnapshot.getKey();
                int index = pushIds.indexOf(id);
                pushIds.remove(index);
                itemTypeList.remove(index);
                valueOneList.remove(index);
                valueTwoList.remove(index);
                itemsRecyclerAdapter.notifyItemRemoved(index);
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                // child don't move in DB
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        // swipe left to delete, drag and drop to move
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT) {// ItemTouchHelper.UP | ItemTouchHelper.DOWN,
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {

                return false;
            }

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {

                if (direction == ItemTouchHelper.LEFT) { // if swipe left
                    AlertDialog.Builder ab = new AlertDialog.Builder(mContext);// if getBaseContext, theme not right
                    ab.setTitle("Deleting An Item")
                            .setMessage("Are you sure to delete this item?")
                            .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                @Override // delete
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    String dayNumber = holder.dayNumView.getText().toString();
                                    // remove value from this pushId
                                    int itemPosition = viewHolder.getBindingAdapterPosition();// same as pushId index
                                    tripRef.child("Days").child(dayNumber).child(pushIds.
                                            get(itemPosition)).removeValue();

                                }
                            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override // not delete
                        public void onClick(DialogInterface dialog, int which) {
                            itemsRecyclerAdapter.notifyItemChanged(viewHolder.getBindingAdapterPosition());
                            dialog.dismiss();
                        }
                    }).show();
                }

            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        //Attach to RecyclerView
        itemTouchHelper.attachToRecyclerView(holder.items);

        // delete-day button
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder ab = new AlertDialog.Builder(mContext);// if getBaseContext, theme not right
                ab.setTitle("Deleting A Day")
                        .setMessage("Are you sure to delete this day?")
                        .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            @Override // delete
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                                final String dayNumber = holder.dayNumView.getText().toString();
                                tripRef.child("Days").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                        // if this day has value
                                        if (dataSnapshot.hasChild(dayNumber)) {

                                            // remove this day from Days
                                            tripRef.child("Days").child(dayNumber).removeValue();
                                        }

                                        for (DataSnapshot dayData : dataSnapshot.getChildren()) {
                                            String key = dayData.getKey();

                                            if (key.compareTo(dayNumber) > 0) {

                                                // reduce key (day#)
                                                int number = Integer.parseInt(key.substring(4)) - 1;
                                                // save original value to new key
                                                tripRef.child("Days").child("DAY " + number)
                                                        .setValue(dayData.getValue());
                                                // remove old node
                                                tripRef.child("Days").child(key).removeValue();
                                            }
                                        }

                                        // reduce duration
                                        int duration = Integer.parseInt(count) - 1;
                                        count = "" + duration;
                                        tripRef.child("duration").setValue(count);

                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override // not delete
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                }).show();
            }
        });

        // set listener to pass photo pick result
        toItemsAdapterListener = itemsRecyclerAdapter;

    }

    @Override
    // indispensable!!!!!!!!!! or else new added view will be populated by old view's data
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        return Integer.parseInt(count);
    }

    @Override
    public void onIntent(Intent i, int resultCode, int requestCode) {

        toItemsAdapterListener.onIntent(i, resultCode, requestCode); // pass pick photo result to ItemsRecyclerAdapter

    }


    public class DayViewHolder extends RecyclerView.ViewHolder {

        RecyclerView items;
        TextView dayNumView;
        TextView dateView;
        CircleImageView addItemButton;
        CircleImageView addPlaceButton;
        CircleImageView addTransitButton;
        CircleImageView addNoteButton;
        CircleImageView addLodgingButton;
        RelativeLayout buttonsLayout;
        ImageView deleteButton;

        public DayViewHolder(final View itemView) {
            super(itemView);

            items = itemView.findViewById(R.id.list);
            dayNumView = itemView.findViewById(R.id.day_number);
            dateView = itemView.findViewById(R.id.date);
            addItemButton = itemView.findViewById(R.id.add_item_button);
            addPlaceButton = itemView.findViewById(R.id.place);
            addTransitButton = itemView.findViewById(R.id.transit);
            addLodgingButton = itemView.findViewById(R.id.lodging);
            addNoteButton = itemView.findViewById(R.id.note);
            buttonsLayout = itemView.findViewById(R.id.buttons_layout);
            deleteButton = itemView.findViewById(R.id.delete_day_button);


            // add item listener
            View.OnClickListener addItemListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    v.setSelected(!v.isSelected());

                    float startPosition = itemView.findViewById(R.id.place_layout).getX();

                    if (v.isSelected()) {

                        buttonsLayout.setVisibility(View.VISIBLE);
                        ObjectAnimator animation = ObjectAnimator.ofFloat(itemView.findViewById(R.id.transit_layout),
                                "translationX", 150f);
                        animation.start();

                        animation = ObjectAnimator.ofFloat(itemView.findViewById(R.id.lodging_layout),
                                "translationX", 300f);
                        animation.start();

                        animation = ObjectAnimator.ofFloat(itemView.findViewById(R.id.note_layout),
                                "translationX", 490f);
                        animation.start();

                        addItemButton.setImageResource(R.drawable.ic_action_cancel);//selector not working with ImageView

                    } else {

                        ObjectAnimator animation = ObjectAnimator.ofFloat(buttonsLayout.findViewById(R.id.transit_layout),
                                "translationX", startPosition);
                        animation.start();

                        animation = ObjectAnimator.ofFloat(buttonsLayout.findViewById(R.id.lodging_layout),
                                "translationX", startPosition);
                        animation.start();

                        animation = ObjectAnimator.ofFloat(buttonsLayout.findViewById(R.id.note_layout),
                                "translationX", startPosition);
                        animation.start();

                        buttonsLayout.setVisibility(View.INVISIBLE);
                        addItemButton.setImageResource(R.drawable.ic_action_add);

                    }
                }
            };

            addItemButton.setOnClickListener(addItemListener);

            // add place/lodging/transit listener
            View.OnClickListener addPlaceListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    buttonsLayout.setVisibility(View.INVISIBLE);
                    addItemButton.setImageResource(R.drawable.ic_action_add);
                    Intent intent = new Intent(mContext, AddPlaceActivity.class);
                    intent.putExtra("title", title);
                    intent.putExtra("countryCode", countryCode);
                    intent.putExtra("dayNumber", dayNumView.getText().toString());
                    if (v.getId() == R.id.lodging) {
                        intent.putExtra("tag", "lodging");
                    } else if (v.getId() == R.id.place) {
                        intent.putExtra("tag", "place");
                    } else if (v.getId() == R.id.transit) {
                        intent.putExtra("tag", "transit");
                    }
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }
            };

            addPlaceButton.setOnClickListener(addPlaceListener);
            addLodgingButton.setOnClickListener(addPlaceListener);
            addTransitButton.setOnClickListener(addPlaceListener);


            // add note listener
            addNoteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    buttonsLayout.setVisibility(View.INVISIBLE);
                    addItemButton.setImageResource(R.drawable.ic_action_add);
                    Bundle args = new Bundle();
                    args.putString("tripTitle", title);
                    args.putString("dayNumber", dayNumView.getText().toString());
                    AddNoteFragment addNoteFragment = new AddNoteFragment();
                    addNoteFragment.setArguments(args);

                    FragmentTransaction ft = ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction();
                    ft.add(R.id.current_trip_container, addNoteFragment, "AddNote");
                    ft.addToBackStack(null); // press back button to get back to activity
                    ft.commit();
                }
            });
        }
    }
}

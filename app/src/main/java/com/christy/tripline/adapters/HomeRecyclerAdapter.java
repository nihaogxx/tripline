package com.christy.tripline.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.christy.tripline.R;
import com.christy.tripline.activities.ReadTripActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import java.util.LinkedList;

public class HomeRecyclerAdapter extends RecyclerView.Adapter<HomeRecyclerAdapter.TripViewHolder> {

    private final Context mContext;
    private LinkedList<String> titleList;
    private LinkedList<String> uIdList;
    private LinkedList<String> desList;


    public HomeRecyclerAdapter(Context context, LinkedList<String> titleList, LinkedList<String> uIdList, LinkedList<String> desList) {
        mContext = context;
        this.titleList = titleList;
        this.uIdList = uIdList;
        this.desList = desList;
    }

    @NonNull
    @Override
    public HomeRecyclerAdapter.TripViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater mInflater = LayoutInflater.from(mContext);
        View mItemView = mInflater.inflate(R.layout.public_trips_item, parent, false);

        return new TripViewHolder(mItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final HomeRecyclerAdapter.TripViewHolder holder, int position) {

        final String uId = uIdList.get(position);
        final String destination = desList.get(position);
        final String title = titleList.get(position);

        final String[] duration = new String[1];
        final String[] startDate = new String[1];

        FirebaseDatabase.getInstance().getReference().child("Users").child(uId)
                .child("Trips").child(title).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String background = dataSnapshot.child("background").getValue().toString();
                Picasso.with(mContext).load(background).into(holder.backgroundView);

                holder.desView.setText(destination);
                duration[0] = dataSnapshot.child("duration").getValue().toString();
                String durText = duration[0] + " days";
                holder.durationView.setText(durText);
                startDate[0] = dataSnapshot.child("startDate").getValue().toString();
                holder.dateView.setText(startDate[0]);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // view only
                Intent intent = new Intent(mContext, ReadTripActivity.class);
                intent.putExtra("title", title);
                intent.putExtra("duration", duration[0]);
                intent.putExtra("startDate", startDate[0]);
                intent.putExtra("uId", uId);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//??
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return titleList.size();
    }

    public class TripViewHolder extends RecyclerView.ViewHolder{

        RelativeLayout container;
        TextView desView;
        TextView dateView;
        TextView durationView;
        ImageView backgroundView;

        public TripViewHolder(View itemView) {
            super(itemView);
            desView = itemView.findViewById(R.id.trip_title);
            backgroundView = itemView.findViewById(R.id.trip_backgroud);
            dateView = itemView.findViewById(R.id.date);
            durationView = itemView.findViewById(R.id.duration);
            container = itemView.findViewById(R.id.trip_item);
        }
    }
}

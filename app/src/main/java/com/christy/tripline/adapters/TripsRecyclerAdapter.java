package com.christy.tripline.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.christy.tripline.activities.CurrentTripActivity;
import com.christy.tripline.R;
import com.squareup.picasso.Picasso;

import java.util.LinkedList;

public class TripsRecyclerAdapter extends RecyclerView.Adapter<TripsRecyclerAdapter.TripViewHolder> {

    private Context mContext;
    private LinkedList<String> mTitleList;
    private LinkedList<String> mBackgroundURLList;


    public TripsRecyclerAdapter(Context context, LinkedList<String> titleList, LinkedList<String> backgroundURLList) {
        this.mContext = context;
        this.mTitleList = titleList;
        this.mBackgroundURLList = backgroundURLList;
    }


    @NonNull
    @Override
    public TripsRecyclerAdapter.TripViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //creates a View and returns it.
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        View mItemView = mInflater.inflate(R.layout.my_trips_item, parent, false);
        return new TripViewHolder(mItemView);
    }

    //associates the data with the ViewHolder for a given position in the RecyclerView.
    @Override
    public void onBindViewHolder(@NonNull TripsRecyclerAdapter.TripViewHolder holder, int position) {
        // Retrieve the data for that position
        String currentTitle = mTitleList.get(position);
        holder.titleView.setText(currentTitle);
        // set backgroundView to current image here
        String currentBackground = mBackgroundURLList.get(position);
        Picasso.with(mContext).load(currentBackground).into(holder.backgroundView);
    }

    @Override
    public int getItemCount() {
        return mTitleList.size();
    }


    public class TripViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView titleView;
        ImageView backgroundView;

        public TripViewHolder(View itemView) {
            super(itemView);

            titleView = itemView.findViewById(R.id.trip_title);
            backgroundView = itemView.findViewById(R.id.trip_backgroud);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            final String title = titleView.getText().toString();
            // go to selected trip
            Intent intent = new Intent(mContext, CurrentTripActivity.class);
            intent.putExtra("title", title);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
        }

    }

}


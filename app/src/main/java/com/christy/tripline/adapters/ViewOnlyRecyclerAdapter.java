package com.christy.tripline.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.christy.tripline.R;
import com.christy.tripline.model.LodgingItemModel;
import com.christy.tripline.model.NoteItemModel;
import com.christy.tripline.model.PlaceItemModel;
import com.christy.tripline.model.TransitItemModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class ViewOnlyRecyclerAdapter extends RecyclerView.Adapter<ViewOnlyRecyclerAdapter.TripViewHolder> {
    private Context mContext;
    private String mTitle, mTripDuration, mStartDate, uId;
    private DatabaseReference tripRef;


    public ViewOnlyRecyclerAdapter(Context context, String title, String tripDuration, String startDate, String uId) {
        mContext = context;
        mTitle = title;
        mTripDuration = tripDuration;
        mStartDate = startDate;
        this.uId = uId;
        tripRef = FirebaseDatabase.getInstance().getReference().child("Users").child(uId).child("Trips").child(mTitle);
    }

    @NonNull
    @Override
    public TripViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.view_only_single_day, parent, false);

        return new TripViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final TripViewHolder holder, int position) {

        //set day#
        int number = position + 1;
        holder.dayNumView.setText("DAY " + number);

        // set dates
        if (!mStartDate.equals("mm/dd/yyyy")) {

            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);

            // set start date to calender
            try {
                c.setTime(sdf.parse(mStartDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            // add days to calender
            c.add(Calendar.DATE, position);

            // get new date
            String resultDate = sdf.format(c.getTime());

            // update date view
            holder.dateView.setText(resultDate);
        }

        // set inner recycler view
        // each view must have their own lists.
        final ArrayList<String> itemTypeList = new ArrayList<>();
        final ArrayList<String> valueOneList = new ArrayList<>();
        final ArrayList<String> valueTwoList = new ArrayList<>();
        final ArrayList<String> pushIdList = new ArrayList<>();

        final ViewOnlyItemsAdapter itemsAdapter = new ViewOnlyItemsAdapter(mContext, itemTypeList,
                valueOneList, valueTwoList, pushIdList, mTitle, uId, holder.dayNumView.getText().toString());

        tripRef.child("Days").child("DAY " + number).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot itemData : dataSnapshot.getChildren()) {

                    String itemType = itemData.child("itemType").getValue().toString();
                    pushIdList.add(itemData.getKey());

                    switch (itemType) {

                        case "place":

                            // add type value
                            itemTypeList.add("place");

                            // get place name and type
                            PlaceItemModel place = itemData.getValue(PlaceItemModel.class);
                            String placeName = place.getPlaceName();
                            valueOneList.add(placeName);

                            String placeType = place.getPlaceType();
                            valueTwoList.add(placeType);
                            break;

                        case "lodging":

                            // add type value
                            itemTypeList.add("lodging");

                            // get lodging name and skip dates
                            LodgingItemModel lodging = itemData.getValue(LodgingItemModel.class);
                            String hotelName = lodging.getPlaceName();
                            valueOneList.add(hotelName);
                            valueTwoList.add(""); // for privacy
                            break;


                        case "note":

                            itemTypeList.add("note");
                            String noteTitle = itemData.getValue(NoteItemModel.class).getNoteTitle();
                            valueOneList.add(noteTitle);
                            String body = itemData.getValue(NoteItemModel.class).getNoteBody();
                            String[] lines = body.split("\r\n|\r|\n");

                            if (lines.length > 1) {
                                body = lines[0] + "\n...";
                            }
                            valueTwoList.add(body);

                            break;

                        case "transit":

                            itemTypeList.add("transit");
                            TransitItemModel transit = itemData.getValue(TransitItemModel.class);
                            String transitName = transit.getPlaceName();
                            valueOneList.add(transitName);
                            valueTwoList.add(""); // for privacy
                            break;

                    }

                    // create an adapter
                    holder.items.setLayoutManager(new LinearLayoutManager(mContext));
                    holder.items.setAdapter(itemsAdapter);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    @Override
    public int getItemCount() {
        return Integer.parseInt(mTripDuration);
    }

    public class TripViewHolder extends RecyclerView.ViewHolder{

        RecyclerView items;
        TextView dayNumView;
        TextView dateView;

        public TripViewHolder(View itemView) {
            super(itemView);

            items = itemView.findViewById(R.id.list);
            dayNumView = itemView.findViewById(R.id.day_number);
            dateView = itemView.findViewById(R.id.date);
        }
    }
}

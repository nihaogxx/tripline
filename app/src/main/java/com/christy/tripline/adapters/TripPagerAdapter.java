package com.christy.tripline.adapters;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import com.christy.tripline.fragments.BookmarkFragment;
import com.christy.tripline.fragments.ExploreFragment;
import com.christy.tripline.fragments.PlanFragment;
import com.christy.tripline.fragments.WeatherFragment;
import com.google.firebase.database.annotations.Nullable;

public class TripPagerAdapter extends FragmentPagerAdapter {

    private Bundle bundle;

    public TripPagerAdapter(FragmentManager fm, Bundle bundle) {
        super(fm);
        this.bundle = bundle;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                PlanFragment planFragment = new PlanFragment();
                // pass bundle to fragment
                planFragment.setArguments(bundle);
                return planFragment;
            case 1:
                ExploreFragment exploreFragment = new ExploreFragment();
                exploreFragment.setArguments(bundle);
                return exploreFragment;
            case 2:

                BookmarkFragment bookmarkFragment = new BookmarkFragment();
                bookmarkFragment.setArguments(bundle);
                return bookmarkFragment;
            case 3:
                WeatherFragment weatherFragment = new WeatherFragment();
                weatherFragment.setArguments(bundle);

                return weatherFragment;


        }

        return null;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){

            case 0: return "Plan";
            case 1: return "Explore";
            case 2: return "Bookmark";
            case 3: return "Weather";
        }

        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public int getItemPosition(Object object) { // workaround to notify viewpager whether to refresh an item
        return POSITION_NONE;
    }
}

package com.christy.tripline.adapters;

import android.content.Intent;

public interface OnPhotoPickedListener { // we can also create this interface in PlanFragment

    void onIntent(Intent i, int resultCode, int requestCode);
}

package com.christy.tripline.adapters;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.christy.tripline.R;
import com.christy.tripline.fragments.UserProfileDisplayFragment;
import com.squareup.picasso.Picasso;
import java.util.LinkedList;
import de.hdodenhof.circleimageview.CircleImageView;

public class PeopleRecyclerAdapter  extends RecyclerView.Adapter<PeopleRecyclerAdapter.PeopleHolder>{

    private Context mContext;
    private LinkedList<String> profileUrlList;
    private LinkedList<String> nameList;
    private LinkedList<String> locationList;
    private LinkedList<String> userIdList;

    public PeopleRecyclerAdapter(Context context, LinkedList<String> profileUrlList,
                                 LinkedList<String> nameList, LinkedList<String> locationList,
                                 LinkedList<String> userIdList) {
        mContext = context;
        this.profileUrlList = profileUrlList;
        this.nameList = nameList;
        this.locationList = locationList;
        this.userIdList = userIdList;
    }

    @NonNull
    @Override
    public PeopleHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.single_user_display, parent, false);

        return new PeopleHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PeopleHolder holder, int position) {

        String profileUrl = profileUrlList.get(position);
        String name = nameList.get(position);
        String location = locationList.get(position);
        final String id = userIdList.get(position);

        holder.nameView.setText(name);
        holder.locationView.setText(location);
        if (!profileUrl.equals("")) {
            Picasso.with(mContext).load(profileUrl).placeholder(R.drawable.no_image_icon).into(holder.profileView);
        } else holder.profileView.setImageResource(R.drawable.no_image_icon);

        holder.userCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // go to friend's profile display
                UserProfileDisplayFragment userProfileDisplayFragment = new UserProfileDisplayFragment();
                Bundle bundle = new Bundle();
                bundle.putString("id", id);
                userProfileDisplayFragment.setArguments(bundle);
                FragmentTransaction ft = ((FragmentActivity)mContext).getSupportFragmentManager().beginTransaction();
                // since RelativeLayout allows views to be on top of one another. When clicking on
                // the fragment, not fully handling it, so it goes through to the base view
                ft.add(R.id.friends_activity_container, userProfileDisplayFragment, "Display Profile");
                ft.addToBackStack(null);
                ft.commit();

            }
        });

    }

    @Override
    public int getItemCount() {
        return nameList.size();
    }

    @Override
    // indispensable!!!!!!!!!! or else new added view will be populated by old view's data
    public int getItemViewType(int position) {
        return position;
    }

    public class PeopleHolder extends RecyclerView.ViewHolder{

        CircleImageView profileView;
        TextView nameView;
        TextView locationView;
        RelativeLayout userCard;

        public PeopleHolder(View itemView) {
            super(itemView);

            profileView = itemView.findViewById(R.id.friends_profile);
            nameView = itemView.findViewById(R.id.friends_user_name);
            locationView = itemView.findViewById(R.id.friends_location);
            userCard = itemView.findViewById(R.id.friend_card);
        }
    }
}

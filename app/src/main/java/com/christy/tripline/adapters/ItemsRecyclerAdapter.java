package com.christy.tripline.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import com.christy.tripline.R;
import com.christy.tripline.model.NoteItemModel;
import com.christy.tripline.fragments.EditNoteFragment;
import com.christy.tripline.fragments.ReadMoreAndEditFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class ItemsRecyclerAdapter extends RecyclerView.Adapter<ItemsRecyclerAdapter.ItemViewHolder>
        implements OnPhotoPickedListener {

    private Context mContext;
    private ArrayList<String> itemTypeList;
    private ArrayList<String> valueOneList;
    private ArrayList<String> valueTwoList;
    private ArrayList<String> pushIds;
    private String title;
    private String dayNumber;
    private DatabaseReference dayRef;
    private static final int RC_GALLERY_PICK = 5;
    private static final String ID_PREFS = "IdPrefs";

    public ItemsRecyclerAdapter(Context context, ArrayList<String> itemType, ArrayList<String> valueOneList,
                                ArrayList<String> valueTwoList, ArrayList<String> pushIds, String title, String dayNumber) {
        mContext = context;
        this.itemTypeList = itemType;
        this.valueOneList = valueOneList;
        this.valueTwoList = valueTwoList;
        this.pushIds = pushIds;
        this.title = title;
        this.dayNumber = dayNumber;
        this.dayRef = FirebaseDatabase.getInstance().getReference().child("Users").child(FirebaseAuth
                .getInstance().getCurrentUser().getUid()).child("Trips").child(title).child("Days").child(dayNumber);

    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.single_item, parent, false);

        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ItemViewHolder holder, int position) {

        final String[] noteText = new String[1];
        final String[] placeId = new String[1];

        // get icon
        final String icon = itemTypeList.get(position);
        // get push id
        final String pushId = pushIds.get(position);

        // set value One
        String valueOne = valueOneList.get(position);
        holder.valueOneView.setText(valueOne);
        // set value Two
        String valueTwo = valueTwoList.get(position);
        holder.valueTwoView.setText(valueTwo);

        // set menu listener for lodging, note, &transit
        View.OnClickListener menuEditListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popupMenu = new PopupMenu(mContext, holder.actionButton);// pass context and UI
                // inflate menu
                popupMenu.getMenuInflater().inflate(R.menu.item_menu_edit, popupMenu.getMenu());

                //Register the popup with OnMenuItemClickListener
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.action_edit:

                                if ("lodging".equals(icon) || "transit".equals(icon)) {
                                    ReadMoreAndEditFragment readMoreAndEditFragment = new ReadMoreAndEditFragment();
                                    Bundle args = new Bundle();
                                    args.putString("placeId", placeId[0]);
                                    args.putString("placeName", holder.valueOneView.getText().toString());
                                    args.putString("info", holder.valueTwoView.getText().toString());
                                    args.putString("pushId", pushId);
                                    args.putString("title", title);
                                    args.putString("dayNumber", dayNumber);
                                    if (icon.equals("lodging")) {
                                        args.putString("tag", "fromLodging");
                                    } else {
                                        args.putString("tag", "fromTransit");
                                    }
                                    readMoreAndEditFragment.setArguments(args);
                                    FragmentTransaction ft = ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction();
                                    ft.add(R.id.current_trip_container, readMoreAndEditFragment, "ReadMore");
                                    ft.addToBackStack(null);
                                    ft.commit();

                                } else if ("note".equals(icon)) {
                                    EditNoteFragment editNoteFragment = new EditNoteFragment();
                                    String text = noteText[0];
                                    String noteTitle = valueOneList.get(holder.getBindingAdapterPosition());
                                    Bundle args = new Bundle();
                                    args.putString("text", text);
                                    args.putString("noteTitle", noteTitle);
                                    args.putString("pushId", pushId);
                                    args.putString("tripTitle", title);
                                    args.putString("dayNumber", dayNumber);
                                    editNoteFragment.setArguments(args);
                                    FragmentTransaction ft = ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction();
                                    // fab will automatically hide if parent is current trip not plan fragment
                                    ft.add(R.id.current_trip_container, editNoteFragment, "EditNote");
                                    ft.addToBackStack(null); // press back button to get back to activity
                                    ft.commit();
                                }
                                break;

                            case R.id.action_upload_photo:

                                // send user to gallery
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_GET_CONTENT)
                                        .setType("image/*");
                                // start pick photo activity from PlanFragment -> PlanFragment receives result and pass result to PlanRecyclerAdapter
                                // -> PlanRecyclerAdapter pass result back to here
                                Fragment planFragment = ((FragmentActivity) mContext).getSupportFragmentManager().
                                        findFragmentByTag("android:switcher:" + R.id.view_pager + ":" + 0);
                                planFragment.startActivityForResult(intent, RC_GALLERY_PICK);

                                // save pushId and day#
                                SharedPreferences idPref = mContext.getSharedPreferences(ID_PREFS, 0);
                                SharedPreferences.Editor editor = idPref.edit();
                                editor.clear();
                                editor.putString("pushId", pushId);
                                editor.putString("dayNumber", dayNumber);
                                editor.apply(); // in background
                                break;

                        }

                        return true;
                    }
                });
                popupMenu.show();
            }
        };


        // set read more listener for Place
        View.OnClickListener menuReadMoreListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popupMenu = new PopupMenu(mContext, holder.actionButton);
                // inflate menu
                popupMenu.getMenuInflater().inflate(R.menu.item_menu_see_more, popupMenu.getMenu());
                //Register the popup with OnMenuItemClickListener
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {

                            case R.id.action_see_more:
                                ReadMoreAndEditFragment readMoreAndEditFragment = new ReadMoreAndEditFragment();
                                Bundle args = new Bundle();

                                args.putString("placeId", placeId[0]);
                                args.putString("placeName", holder.valueOneView.getText().toString());
                                args.putString("placeType", holder.valueTwoView.getText().toString());
                                args.putString("tag", "fromPlace");
                                args.putString("title", title);
                                readMoreAndEditFragment.setArguments(args);

                                FragmentTransaction ft = ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction();
                                ft.add(R.id.current_trip_container, readMoreAndEditFragment, "ReadMore");
                                ft.addToBackStack(null);
                                ft.commit();
                                break;


                            case R.id.action_upload_photo:

                                // send user to gallery
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_GET_CONTENT);
                                intent.setType("image/*");
                                // start pick photo activity from PlanFragment -> PlanFragment receives result and pass result to PlanRecyclerAdapter
                                // -> PlanRecyclerAdapter pass result back to here
                                Fragment planFragment = ((FragmentActivity) mContext).getSupportFragmentManager().
                                        findFragmentByTag("android:switcher:" + R.id.view_pager + ":" + 0);
                                planFragment.startActivityForResult(intent, RC_GALLERY_PICK);

                                // save pushId and day for after getting result from activity
                                SharedPreferences idPref = mContext.getSharedPreferences(ID_PREFS, 0);
                                SharedPreferences.Editor editor = idPref.edit();
                                editor.clear();
                                editor.putString("pushId", pushId);
                                editor.putString("dayNumber", dayNumber);
                                editor.apply(); // in background

                                break;

                        }
                        return true;
                    }

                });

                popupMenu.show();
            }

        };

        // set icon view
        switch (icon) {
            case "place":
                holder.iconView.setImageResource(R.drawable.ic_place);

                // onClickListener to see more info
                holder.actionButton.setOnClickListener(menuReadMoreListener);
                break;

            case "lodging":
                holder.iconView.setImageResource(R.drawable.ic_hotel);

                // onClickListener to edit check in/check out dates
                holder.actionButton.setOnClickListener(menuEditListener);
                break;

            case "transit":
                holder.iconView.setImageResource(R.drawable.ic_transit);
                // onClickListener to edit transit info
                holder.actionButton.setOnClickListener(menuEditListener);
                break;

            case "note":
                holder.iconView.setImageResource(R.drawable.ic_note);
                // onClickListener to edit note
                holder.actionButton.setOnClickListener(menuEditListener);
                break;
        }

        //load images if any，set clicker if it's a note, get placeId if it's not a note
        dayRef.child(pushId).addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if ("note".equals(icon)) {
                    noteText[0] = dataSnapshot.getValue(NoteItemModel.class).getNoteBody();
                    setNoteClicker();
                } else {
                    placeId[0] = dataSnapshot.child("placeId").getValue().toString();
                }

                holder.gallery.removeAllViews();

                // get photo if any
                if (dataSnapshot.hasChild("photos")) {

                    for (DataSnapshot photoData : dataSnapshot.child("photos").getChildren()) {

                        final String photoId = photoData.getKey();

                        final String url = photoData.getValue().toString();

                        final View photoView = LayoutInflater.from(mContext).inflate(R.layout.single_photo_vertical,
                                holder.gallery, false);

                        ImageView imageView = photoView.findViewById(R.id.place_photo);
                        Picasso.with(mContext).load(url).resize(imageView.getWidth(), 250).into(imageView);
                        holder.gallery.addView(photoView);

                        // click to delete
                        photoView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(final View v) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(mContext)
                                        .setTitle("Delete Image")
                                        .setMessage("Are you sure to delete this image?")
                                        .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                // remove from storage
                                                StorageReference photoRef = FirebaseStorage.getInstance().getReferenceFromUrl(url);
                                                photoRef.delete();

                                                // remove photo from database
                                                dayRef.child(pushId).child("photos").
                                                        child(photoId).removeValue();

                                            }
                                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                builder.show();
                            }
                        });
                    }
                }
            }

            private void setNoteClicker() {

                if (noteText[0] != null) {

                    String[] lines = noteText[0].split("\r\n|\r|\n"); // split by line break

                    if (lines.length > 1) {

                        // set on click
                        holder.valueTwoView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                v.setSelected(!v.isSelected());

                                if (v.isSelected()) {
                                    holder.valueTwoView.setText(noteText[0]);

                                } else {
                                    holder.valueTwoView.setText(valueTwoList.get(holder.getBindingAdapterPosition()));
                                }

                            }
                        });

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);

    }

    @Override
    public int getItemCount() {
        return itemTypeList.size();
    }

    @Override
    public void onIntent(Intent i, int resultCode, int requestCode) {
        SharedPreferences idPref = mContext.getSharedPreferences(ID_PREFS, 0);
        String pushId = idPref.getString("pushId", null);

        if (resultCode == Activity.RESULT_OK
                && i != null) {// user picked a pic

            Uri imgUri = i.getData(); // data is an imgUri

            storeImageToStorage(imgUri, pushId);

            Toast.makeText(mContext, "Uploading the image...", Toast.LENGTH_SHORT).show();
        }

    }


    void storeImageToStorage(Uri resultUri, final String pushId) {

        StorageReference mUserImageRef = FirebaseStorage.getInstance().getReference().child("User Uploaded Images");

        // get time stamp for individual photo path
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss", Locale.ENGLISH);
        String timeStamp = dateFormat.format(Calendar.getInstance().getTime());

        final StorageReference filePath = mUserImageRef.child(pushId + timeStamp + ".jpg");
        filePath.putFile(resultUri) // upload a local file (any content uri) to storage
                .addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        if (task.isSuccessful()) {
                            // get download url
                            filePath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {

                                    String photoUrl = uri.toString();

                                    // store the download url to firebase database
                                    storeImageToDatabase(photoUrl, pushId);
                                }
                            });

                        } else {
                            Toast.makeText(mContext,
                                    "image failed to be uploaded", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    private void storeImageToDatabase(String downloadUrl, String pushId) {

        SharedPreferences idPref = mContext.getSharedPreferences(ID_PREFS, 0);
        String dayNum = idPref.getString("dayNumber", null);

        DatabaseReference photoRef = FirebaseDatabase.getInstance().getReference().child("Users").child(FirebaseAuth
                .getInstance().getCurrentUser().getUid()).child("Trips").child(title).child("Days").
                child(dayNum).child(pushId).child("photos").push();
        photoRef.setValue(downloadUrl).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {

                } else {
                    Toast.makeText(mContext,
                            "image failed to be uploaded", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder {

        CircleImageView iconView;
        TextView valueOneView;
        TextView valueTwoView;
        LinearLayout gallery;
        ImageView actionButton;

        public ItemViewHolder(View itemView) {
            super(itemView);

            iconView = itemView.findViewById(R.id.item_icon);
            valueOneView = itemView.findViewById(R.id.value_1);
            valueTwoView = itemView.findViewById(R.id.value_2);
            actionButton = itemView.findViewById(R.id.item_action_button);
            gallery = itemView.findViewById(R.id.user_photo_gallery);

        }
    }
}

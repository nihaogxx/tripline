package com.christy.tripline.adapters;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.christy.tripline.R;
import com.christy.tripline.model.NoteItemModel;
import com.christy.tripline.fragments.ReadMoreAndEditFragment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import de.hdodenhof.circleimageview.CircleImageView;

public class ViewOnlyItemsAdapter extends RecyclerView.Adapter<ViewOnlyItemsAdapter.ItemViewHolder> {

    private Context mContext;
    private ArrayList<String> itemTypeList;
    private ArrayList<String> valueOneList;
    private ArrayList<String> valueTwoList;
    private ArrayList<String> pushIdList;
    private String title, uId, dayNumber;
    private DatabaseReference dayRef;

    public ViewOnlyItemsAdapter(Context context, ArrayList<String> itemTypeList, ArrayList<String> valueOneList,
                                ArrayList<String> valueTwoList, ArrayList<String> pushIdList, String title, String uId, String dayNumber) {
        mContext = context;
        this.itemTypeList = itemTypeList;
        this.valueOneList = valueOneList;
        this.valueTwoList = valueTwoList;
        this.pushIdList = pushIdList;
        this.title = title;
        this.uId = uId;
        this.dayNumber = dayNumber;

    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.view_only_single_item, parent, false);

        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ItemViewHolder holder, final int position) {

        final String[] noteText = new String[1];
        final String[] placeId = new String[1];

        // get push id
        final String pushId = pushIdList.get(position);

        // get icon
        final String icon = itemTypeList.get(position);

        View.OnClickListener readMoreListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ReadMoreAndEditFragment readMoreAndEditFragment = new ReadMoreAndEditFragment();
                Bundle args = new Bundle();
                args.putString("placeId", placeId[0]);
                args.putString("placeName", holder.valueOneView.getText().toString());
                args.putString("tag", "fromReadOnly");
                readMoreAndEditFragment.setArguments(args);

                FragmentTransaction ft = ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction();
                ft.add(R.id.read_trip_container, readMoreAndEditFragment, "ReadMore");
                ft.addToBackStack(null);
                ft.commit();
            }
        };

        // set value One
        String valueOne = valueOneList.get(position);
        holder.valueOneView.setText(valueOne);
        // set value Two
        String valueTwo = valueTwoList.get(position);
        holder.valueTwoView.setText(valueTwo);

        // set icon view
        switch (icon) {
            case "place":
                holder.iconView.setImageResource(R.drawable.ic_place);
                holder.container.setOnClickListener(readMoreListener);
                break;

            case "lodging":
                holder.iconView.setImageResource(R.drawable.ic_hotel);
                holder.container.setOnClickListener(readMoreListener);
                holder.valueTwoView.setVisibility(View.GONE);
                break;

            case "transit":
                holder.iconView.setImageResource(R.drawable.ic_transit);
                holder.container.setOnClickListener(readMoreListener);
                holder.valueTwoView.setVisibility(View.GONE);
                break;

            case "note":
                holder.iconView.setImageResource(R.drawable.ic_note);
                break;

        }

        dayRef = FirebaseDatabase.getInstance().getReference().child("Users").child(uId).child("Trips").child(title).child("Days").child(dayNumber);


        // load images if any，set clicker if it's a note, get placeId if it's not a note
        dayRef.child(pushId).addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    if (icon.equals("note")) {

                        noteText[0] = dataSnapshot.getValue(NoteItemModel.class).getNoteBody();
                        setNoteClicker();
                    } else {
                        placeId[0] = dataSnapshot.child("placeId").getValue().toString();
                    }

                    // get photo if any
                    if (dataSnapshot.hasChild("photos")) {

                        for (DataSnapshot photoData : dataSnapshot.child("photos").getChildren()) {

                            final String url = photoData.getValue().toString();

                            View photoView = LayoutInflater.from(mContext).inflate(R.layout.single_photo_vertical,
                                    holder.gallery, false);

                            ImageView imageView = photoView.findViewById(R.id.place_photo);
                            Picasso.with(mContext).load(url).into(imageView);
                            holder.gallery.addView(photoView);
                        }
                }
            }

            private void setNoteClicker() {

                if (noteText[0] != null) {

                    String[] lines = noteText[0].split("\r\n|\r|\n");

                    if (lines.length > 1) {

                        // set on click
                        holder.container.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                v.setSelected(!v.isSelected());

                                if (v.isSelected()) {
                                    holder.valueTwoView.setText(noteText[0]);

                                } else {

                                    holder.valueTwoView.setText(valueTwoList.get(position));

                                }

                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

    }

    @Override
    public int getItemCount() {
        return itemTypeList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        CircleImageView iconView;
        TextView valueOneView;
        TextView valueTwoView;
        LinearLayout gallery;
        CardView container;

        public ItemViewHolder(View itemView) {
            super(itemView);

            iconView = itemView.findViewById(R.id.item_icon);
            valueOneView = itemView.findViewById(R.id.value_1);
            valueTwoView = itemView.findViewById(R.id.value_2);
            gallery = itemView.findViewById(R.id.user_photo_gallery);
            container = itemView.findViewById(R.id.card_container);
        }
    }
}

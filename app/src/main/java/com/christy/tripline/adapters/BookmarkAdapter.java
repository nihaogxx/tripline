package com.christy.tripline.adapters;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.christy.tripline.R;
import com.christy.tripline.fragments.ReadMoreAndEditFragment;
import com.google.android.libraries.places.api.model.PhotoMetadata;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPhotoRequest;
import com.google.android.libraries.places.api.net.FetchPhotoResponse;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BookmarkAdapter extends RecyclerView.Adapter<BookmarkAdapter.BookmarkHolder> {

    private Context mContext;
    private ArrayList<String> placeIdList;
    private ArrayList<String> pushIdList;
    private DatabaseReference tripRef;
    private String title;
    private String duration;
    private PlacesClient placesClient;
    private static String APP_API_KEY;

    public BookmarkAdapter(Context context, String title, String duration, ArrayList<String> idList,
                           ArrayList<String> pushIdList) {
        mContext = context;
        this.placeIdList = idList;
        this.title = title;
        this.duration = duration;
        this.pushIdList = pushIdList;
        APP_API_KEY = mContext.getString(R.string.app_api_key);
        tripRef = FirebaseDatabase.getInstance().getReference().child("Users").child(FirebaseAuth
                .getInstance().getCurrentUser().getUid()).child("Trips").child(title);

    }

    @NonNull
    @Override
    public BookmarkAdapter.BookmarkHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.explore_item, parent, false);

        //Initialize the places SDK client
        if (!Places.isInitialized()) {
            Places.initialize(mContext, APP_API_KEY);
        }
        placesClient = Places.createClient(mContext);

        return new BookmarkHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final BookmarkAdapter.BookmarkHolder holder, int position) {

        final String id = placeIdList.get(position);
        final String pushId = pushIdList.get(position);
        final String[] placeName = new String[1];
        final String[] placeRating = new String[1];
        final String[] placeType = new String[1];

        List<Place.Field> fields = Arrays.asList(Place.Field.NAME, Place.Field.RATING, Place.Field.TYPES,
                Place.Field.PHOTO_METADATAS);
        FetchPlaceRequest placeRequest = FetchPlaceRequest.builder(id, fields).build();
        placesClient.fetchPlace(placeRequest).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
            @Override
            public void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
                Place place = fetchPlaceResponse.getPlace();

                // get name
                placeName[0] = place.getName();
                holder.nameView.setText(placeName[0]);

                // get rating
                if (place.getRating() != null) {

                    float rating = place.getRating().floatValue();
                    DecimalFormat df = new DecimalFormat("#.#");
                    placeRating[0] = df.format(rating);
                    holder.ratingBar.setRating(rating);

                } else holder.ratingBar.setVisibility(View.GONE);

                // get photo
                List<PhotoMetadata> photos = place.getPhotoMetadatas();
                if (photos != null) {
                    PhotoMetadata photo = photos.get(0);
                    FetchPhotoRequest photoRequest = FetchPhotoRequest.builder(photo).build();
                    placesClient.fetchPhoto(photoRequest).addOnSuccessListener(new OnSuccessListener<FetchPhotoResponse>() {
                        @Override
                        public void onSuccess(FetchPhotoResponse fetchPhotoResponse) {
                            Bitmap bitmap = fetchPhotoResponse.getBitmap();
                            holder.photoView.setImageBitmap(bitmap);
                        }
                    });

                } else {
                    holder.photoView.setImageResource(R.drawable.ic_no_photo);
                }

                // get type
                if (place.getTypes() != null) {
                    Place.Type type = place.getTypes().get(0);
                    placeType[0] = type.toString();
                    holder.typeView.setText(placeType[0]);
                } else holder.typeView.setVisibility(View.GONE);
            }

        });


        // set open now
        holder.openNow.setText("open now");

        // set bookmark
        holder.bookmark.setImageResource(R.drawable.ic_bookmark);
        holder.bookmark.setSelected(true);


        holder.bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // remove from database
                tripRef.child("Bookmarks").child(pushId).removeValue();

            }
        });

        holder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // see attraction details
                ReadMoreAndEditFragment newFragment = new ReadMoreAndEditFragment();
                Bundle bundle = new Bundle();
                bundle.putString("placeName", placeName[0]);
                bundle.putString("placeId", id);
                bundle.putString("type", placeType[0]);

                if (placeRating[0] == null) {
                    bundle.putFloat("rating", 0);
                } else {
                    bundle.putFloat("rating", Float.parseFloat(placeRating[0]));
                }
                bundle.putString("open", "open now");
                bundle.putString("title", title);
                bundle.putString("duration", duration);
                bundle.putString("tag", "fromExplore");
                newFragment.setArguments(bundle);
                FragmentTransaction ft = ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction();
                ft.add(R.id.current_trip_container, newFragment, "BookmarkDetails");
                ft.addToBackStack(null);
                ft.commit();

            }
        });


    }

    @Override
    // indispensable!!!!!!!!!! or else new added view will be populated by old view's data
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return placeIdList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class BookmarkHolder extends RecyclerView.ViewHolder {

        ImageView photoView;
        TextView nameView;
        TextView typeView;
        TextView openNow;
        RatingBar ratingBar;
        ImageView bookmark;
        CardView mCardView;

        public BookmarkHolder(View itemView) {
            super(itemView);

            photoView = itemView.findViewById(R.id.attraction_photo);
            nameView = itemView.findViewById(R.id.attraction_name);
            typeView = itemView.findViewById(R.id.attraction_type);
            ratingBar = itemView.findViewById(R.id.attraction_rating);
            openNow = itemView.findViewById(R.id.open_now);
            bookmark = itemView.findViewById(R.id.bookmark);
            mCardView = itemView.findViewById(R.id.attraction_card);
        }
    }
}

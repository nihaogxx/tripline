package com.christy.tripline.adapters;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.christy.tripline.R;
import com.christy.tripline.fragments.ReadMoreAndEditFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ExploreRecyclerAdapter extends RecyclerView.Adapter<ExploreRecyclerAdapter.AttractionHolder>{


    private Context mContext;
    private ArrayList<String> nameList;
    private ArrayList<Float> ratingList;
    private ArrayList<String> typeList;
    private ArrayList<String> idList;
    private ArrayList<String> photoList;
    private ArrayList<String> openList;
    private static String APP_API_KEY;
    private String title;


    public ExploreRecyclerAdapter(Context context, ArrayList<String> nameList, ArrayList<Float> ratingList
            , ArrayList<String> typeList, ArrayList<String> idList, ArrayList<String> openList,
                                  ArrayList<String> photoList, String title) {
        mContext = context;
        this.nameList = nameList;
        this.ratingList = ratingList;
        this.typeList = typeList;
        this.idList = idList;
        this.openList = openList;
        this.photoList = photoList;
        this.title = title;
        APP_API_KEY = mContext.getString(R.string.app_api_key);
    }


    @NonNull
    @Override
    public AttractionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.explore_item, parent, false);

        return new AttractionHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AttractionHolder holder, final int position) {
        // get place id
        final String id = idList.get(position);
        // set name
        final String name = nameList.get(position);
        holder.nameView.setText(name);
        // get type
        final String type = typeList.get(position);
        holder.typeView.setText(type);
        // set rating
        final float rating = ratingList.get(position);
        if (rating >= 0) {
            holder.ratingBar.setRating(rating);
        }

        // get open now
        final String open = openList.get(position);
        if (open.equals("true")) {

            holder.openNow.setText("open now");

        } else if (open.equals("false")) {
            holder.openNow.setText("closed now");
        }

        // build url to get photo
        StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/photo?maxheight=500&photoreference=");
        sb.append(photoList.get(position));
        sb.append("&key=" + APP_API_KEY);
        String url = sb.toString();
        Picasso.with(mContext).load(url).into(holder.photoView);

        holder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // see attraction details
                ReadMoreAndEditFragment newFragment = new ReadMoreAndEditFragment();
                Bundle bundle = new Bundle();
                bundle.putString("placeName", name);
                bundle.putString("placeId", id);
                bundle.putString("type", type);
                bundle.putFloat("rating", rating);
                bundle.putString("open", open);
                bundle.putString("title", title);
                bundle.putString("tag", "fromExplore");
                newFragment.setArguments(bundle);
                FragmentTransaction ft = ((FragmentActivity)mContext).getSupportFragmentManager().beginTransaction();
                ft.add(R.id.current_trip_container, newFragment, "AttractionDetails");
                ft.addToBackStack(null);
                ft.commit();

            }
        });


    }

    @Override
    public int getItemCount() {
        return nameList.size();
    }

    @Override
    // indispensable!!!!!!!!!! or else new added view will be populated by old view's data
    public int getItemViewType(int position) {
        return position;
    }

    public class AttractionHolder extends RecyclerView.ViewHolder {

        ImageView photoView;
        TextView nameView;
        TextView typeView;
        TextView openNow;
        RatingBar ratingBar;
        CardView mCardView;
        ImageView bookmark;


        public AttractionHolder(View itemView) {
            super(itemView);

            photoView = itemView.findViewById(R.id.attraction_photo);
            nameView = itemView.findViewById(R.id.attraction_name);
            typeView = itemView.findViewById(R.id.attraction_type);
            ratingBar = itemView.findViewById(R.id.attraction_rating);
            openNow = itemView.findViewById(R.id.open_now);
            mCardView = itemView.findViewById(R.id.attraction_card);
            bookmark = itemView.findViewById(R.id.bookmark);

            bookmark.setVisibility(View.GONE);

        }
    }
}

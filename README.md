An android tirp planning application where users can make their trip plans as detailed and complete as they need and share their trips with other users. 


![Home](https://bitbucket.org/nihaogxx/tripline/downloads/Screen_Shot_2019-02-24_at_7.10.28_PM.png)

![Navigation](https://bitbucket.org/nihaogxx/tripline/downloads/Screen_Shot_2019-02-24_at_7.10.53_PM.png)

![My Trips](https://bitbucket.org/nihaogxx/tripline/downloads/Screen_Shot_2019-11-30_at_10.03.54_PM.png)

![Plan](https://bitbucket.org/nihaogxx/tripline/downloads/Screen_Shot_2019-02-24_at_7.12.54_PM.png)

![Place Details](https://bitbucket.org/nihaogxx/tripline/downloads/Screen_Shot_2019-02-24_at_7.13.27_PM.png)

![Bookmarks](https://bitbucket.org/nihaogxx/tripline/downloads/Screen_Shot_2019-02-24_at_7.13.44_PM.png)

![Map](https://bitbucket.org/nihaogxx/tripline/downloads/Screen_Shot_2019-02-24_at_7.14.43_PM.png)

![Add a Place from Map](https://bitbucket.org/nihaogxx/tripline/downloads/Screen_Shot_2019-02-24_at_7.16.27_PM.png)

![Friends](https://bitbucket.org/nihaogxx/tripline/downloads/Screen_Shot_2019-11-30_at_10.31.23_PM.png)